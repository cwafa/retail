<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

/**
 * Description of MY_Controller
 *
 * @author pudyasto
 */
class MY_Controller extends CI_Controller{
    var $menu_app="";
    var $msg_active="";
    var $msg_main="";
    var $msg_detail="";
    var $csrf="";
    public $mm="";
    public $yyyymm="";
    public $yyyymmdd="";
    public $ddmmyyyy="";
    public $last_ddmmyyyy="";

    public $perusahaan="";
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('enc'));
        if(isset($_GET['token'])){
            $tmptoken = $this->enc->decode($_GET['token']);
            $userdata = json_decode($tmptoken);
            if($userdata->expd < time()){
                redirect('access/logout','refresh');
            }else{
                foreach ($userdata as $key => $object) {
                    $this->session->set_userdata($key, $object);
                }
            }
        }
        $logged_in = $this->session->userdata('logged_in');
        if(!$logged_in){
            redirect($this->apps->ssoapp . '/access?url=' . $this->apps->curPageURL(),'refresh');
            exit;
        }



        $kelas = $this->uri->segment(1);
        if($kelas=="dashboard" || empty($kelas)){
            $this->msg_main = "Selamat Datang ". ucwords(strtolower($this->session->userdata('nmuser')));
            $this->msg_detail = ' <i class="fa fa-map-marker text-success"></i> '.$this->apps->logintag;
        }else{
            foreach($this->rbac->ceksubmenu_app($kelas) as $val){
                $this->msg_main = $val->nmmenu;
                $this->msg_detail = $val->description;
            }
        }

        $show = $this->rbac->module_access('show');
        if(empty($show)){
            redirect('debug/err_505','refresh');
            exit;
        }

        $q_2 = $this->db->query("select * from trx.perusahaan");
        if($q_2->num_rows()>0){
            $this->perusahaan = $q_2->result_array();
        }

        //echo "<script> console.log('PHP: ". json_encode($this->perusahaan) ."');</script>"; 

        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

    }

    public function num_format($int) {
        if($int<=9){
            $int = "0".$int;
        }else{
            $int = $int;
        }
        return $int;
    }

    function get_month_name($param){
        $date = explode("-", $param);
        $mm = strtoupper(month($date[1]));
        $yy = $date[0];
        $dm = $param."-01";
        $dd = '';//date("t", strtotime($dm));
        return $dd." ".$mm." ".$yy;
    }
}
