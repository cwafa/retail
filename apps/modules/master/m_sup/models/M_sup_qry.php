<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_sup_qry
 *
 * @author adi
 */
class M_sup_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kdsup) {
        $this->db->select("kdsup, nmsup, pic, nohp, alamat, kota, npwp, noktp, notes,case when fstatus = true then 't' else 'f' end as fstatus");
        $this->db->where('kdsup',$kdsup);
        $query = $this->db->get('trx.m_sup');
                // echo $this->db->last_query();
        return $query->result_array();
    } 

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdsup']) ){
            $id = $_GET['kdsup'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'nmsup',
                            'pic',
                            'nohp',
                            'alamat',
                            'kota',
                            'npwp',
                            'notes',
                            'fstatusx',
                            'kdsup');
	$sIndexColumn = "kdsup";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT '' as no, kdsup, nmsup, pic, nohp, alamat, kota, npwp, notes, CASE WHEN fstatus = TRUE THEN 'ACTIVE' ELSE 'NON-ACTIVE' END AS fstatusx FROM trx.m_sup ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[9] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('m_sup/edit/'.$aRow['kdsup'])."\">Edit</a>";
                $row[10] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdsup']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
        try {
            $array = $this->input->post();  
            if(empty($array['kdsup'])){
                unset($array['kdsup']); 
                $array['nmsup'] = strtoupper($array['nmsup']);
                $array['pic'] = strtoupper($array['pic']);
                $array['nohp'] = strtoupper($array['nohp']);
                $array['alamat'] = strtoupper($array['alamat']);
                $array['kota'] = strtoupper($array['kota']);
                $array['npwp'] = strtoupper($array['npwp']);
                $array['notes'] = strtoupper($array['notes']);
                $array['noktp'] = strtoupper($array['noktp']);
                $array['fstatus'] = 'TRUE';

                $resl = $this->db->insert('trx.m_sup',$array);
                echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdsup'])&& empty($array['stat'])){ 
                $array['nmsup'] = strtoupper($array['nmsup']);
                $array['pic'] = strtoupper($array['pic']);
                $array['nohp'] = strtoupper($array['nohp']);
                $array['alamat'] = strtoupper($array['alamat']);
                $array['kota'] = strtoupper($array['kota']);
                $array['npwp'] = strtoupper($array['npwp']);
                $array['notes'] = strtoupper($array['notes']);
                $array['noktp'] = strtoupper($array['noktp']);
                if($array['fstatus']==='t'){
                    $array['fstatus'] = 'TRUE'; 
                } else {
                    $array['fstatus'] = 'FALSE'; 
                }

                $this->db->where('kdsup', $array['kdsup']);
                $resl = $this->db->update('trx.m_sup', $array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdsup']) && !empty($array['stat'])){                
                $this->db->where('kdsup', $array['kdsup']);
                //$this->db->set("faktif","false");
                //$this->db->set("tgl_out",date('Y-m-d'));
                $resl = $this->db->delete('trx.m_sup');
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            } else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }


}
