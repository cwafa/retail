<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row"> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                                      echo form_input($form['kdcust']);
                                      echo form_error('kdcust','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div> 
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['nmcust']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['nmcust']);
                              echo form_error('nmcust','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div> 

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['pic']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['pic']);
                              echo form_error('pic','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['nohp']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['nohp']);
                              echo form_error('nohp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['alamat']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['alamat']);
                              echo form_error('alamat','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>   
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['kota']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['kota']);
                              echo form_error('kota','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['npwp']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['npwp']);
                              echo form_error('npwp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>   
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['noktp']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['noktp']);
                              echo form_error('noktp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['notes']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['notes']);
                              echo form_error('notes','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div> 

              <div class="col-xs-3">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                <?=form_label($form['fstatus']['placeholder']);?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                          
                                  <div class="checkbox">
                                      <?php
                                          echo form_checkbox($form['fstatus']);
                                      ?>
                                  </div>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdcust').val()){ 
            $('#fstatus').hide();
            // $('.btn-submit').click(function(){
            //     submit();
            // });
        } else if($('#kdcust').val()){ 
            $('#fstatus').hide();
            $('#fstatus').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            // $('.btn-submit').click(function(){
            // update();
            // });
        }
    });



    // function submit(){
    //     var kditemjns = $("#kditemjns").val();
    //     var kditemsat = $("#kditemsat").val();
    //     var kditemgr = $("#kditemgr").val();
    //     var kditem = $("#kditem").val();
    //     var nmitem = $("#nmitem").val().toUpperCase();
    //     //alert(nmitem);
    //   	$.ajax({
    //   		type: "POST",
    //   		url: "<?=site_url("m_customer/submit");?>",
    //   		data: {"kditem":kditem,"nmitem":nmitem,"kditemgr":kditemgr,"kditemjns":kditemjns,"kditemsat":kditemsat},
    //   		success: function(resp){
    //   			var obj = jQuery.parseJSON(resp);
    //   			$.each(obj, function(key, data){
    //           if (data.tipe==="success"){
    //               swal({
    //                   title: data.title,
    //                   text: data.msg,
    //                   type: data.tipe
    //               }, function(){
    //                   window.location.href = '<?=site_url('m_customer');?>';
    //               });
    //           }else{
    //               refresh();
    //           }
    //   			});
    //       },
    //       error:function(event, textStatus, errorThrown) {
    //       	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //       }
    //     });
    // }

    // function update(){
    //     var kditemjns = $("#kditemjns").val();
    //     var kditemsat = $("#kditemsat").val();
    //     var kditemgr = $("#kditemgr").val();
    //     var kditem = $("#kditem").val();
    //     var nmitem = $("#nmitem").val().toUpperCase();
    //     if ($("#faktif").prop("checked")){
    //       var faktif = 't';
    //     } else {
    //       var faktif = 'f';
    //     }
    //   	$.ajax({
    //   		type: "POST",
    //   		url: "<?=site_url("m_customer/update");?>",
    //   		data: {"kditem":kditem,"nmitem":nmitem,"kditemgr":kditemgr,"kditemjns":kditemjns,"kditemsat":kditemsat,"faktif":faktif},
    //   		success: function(resp){
    //   			var obj = jQuery.parseJSON(resp);
    //   			$.each(obj, function(key, data){
    //           if (data.tipe==="success"){
    //               swal({
    //                   title: data.title,
    //                   text: data.msg,
    //                   type: data.tipe
    //               }, function(){
    //                   window.location.href = '<?=site_url('m_customer');?>';
    //               });
    //           }else{
    //               refresh();
    //           }
    //   			});
    //       },
    //       error:function(event, textStatus, errorThrown) {
    //       	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //       }
    //     });
    // }

    function refresh(){
      window.location.reload();
    }
</script>
