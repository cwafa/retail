<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row"> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                                      echo form_input($form['kdprod']);
                                      echo form_error('kdprod','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div> 
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['sku']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['sku']);
                              echo form_error('sku','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div> 

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['nmprod']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['nmprod']);
                              echo form_error('nmprod','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['tipeprod']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_dropdown( $form['tipeprod']['name'],
                                                  $form['tipeprod']['data'] ,
                                                  $form['tipeprod']['value'] ,
                                                  $form['tipeprod']['attr']);
                              echo form_error('tipeprod','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['kdktgprod']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_dropdown( $form['kdktgprod']['name'],
                                                  $form['kdktgprod']['data'] ,
                                                  $form['kdktgprod']['value'] ,
                                                  $form['kdktgprod']['attr']);
                              echo form_error('kdktgprod','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['kdstnprod']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_dropdown( $form['kdstnprod']['name'],
                                                  $form['kdstnprod']['data'] ,
                                                  $form['kdstnprod']['value'] ,
                                                  $form['kdstnprod']['attr']);
                              echo form_error('kdstnprod','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_label($form['notes']['placeholder']);
                                  ?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                                  <?php  
                              echo form_input($form['notes']);
                              echo form_error('notes','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>   
                  </div>
              </div> 

              <div class="col-xs-3">
                  <div class="row">

                      <div class="col-xs-2">
                          <div class="row">
                              <div class="form-group">
                                <?=form_label($form['fstatus']['placeholder']);?>
                              </div>
                          </div>
                      </div> 

                      <div class="col-xs-10">
                          <div class="row">
                              <div class="form-group">
                          
                                  <div class="checkbox">
                                      <?php
                                          echo form_checkbox($form['fstatus']);
                                      ?>
                                  </div>
                              </div>
                          </div>
                      </div>  
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdprod').val()){ 
            $('#fstatus').hide();
            // $('.btn-submit').click(function(){
            //     submit();
            // });
        } else if($('#kdprod').val()){ 
            $('#fstatus').hide();
            $('#fstatus').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            // $('.btn-submit').click(function(){
            // update();
            // });
        }
    }); 

    function refresh(){
      window.location.reload();
    }
</script>
