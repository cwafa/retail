<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_prod_qry
 *
 * @author adi
 */
class M_prod_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kdprod) {
        $this->db->select("kdprod, nmprod, pic, nohp, alamat, kota, npwp, noktp, notes,case when fstatus = true then 't' else 'f' end as fstatus");
        $this->db->where('kdprod',$kdprod);
        $query = $this->db->get('trx.m_prodplier');
                // echo $this->db->last_query();
        return $query->result_array();
    } 

    public function getKategori() {
        $this->db->select("kdktgprod,nmktgprod");
        $this->db->order_by('nmktgprod');
        $query = $this->db->get('trx.m_ktgprod');
                // echo $this->db->last_query();
        return $query->result_array();
    }

    public function getSatuan() {
        $this->db->select("*");
        $this->db->order_by('nmstnprod');
        $query = $this->db->get('trx.m_stnprod');
        return $query->result_array();
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdprod']) ){
            $id = $_GET['kdprod'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'sku',
                            'nmprod',
                            'tipeprod',
                            'nmktgprod',
                            'nmstnprod',
                            'notes', 
                            'fstatusx',
                            'kdprod');
	$sIndexColumn = "kdprod";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT '' as no, a.kdprod, a.nmprod, a.sku, a.tipeprod, a.kdktgprod,b.nmktgprod,a.kdstnprod,c.nmstnprod,a.notes, CASE WHEN a.fstatus = TRUE THEN 'ACTIVE' ELSE 'NON-ACTIVE' END AS fstatusx FROM trx.m_prod a LEFT JOIN trx.m_ktgprod b on a.kdktgprod = b.kdktgprod LEFT JOIN trx.m_stnprod c on a.kdstnprod = c.kdstnprod ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[8] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('m_prod/edit/'.$aRow['kdprod'])."\">Edit</a>";
                $row[9] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdprod']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
        try {
            $array = $this->input->post();  
            if(empty($array['kdprod'])){
                unset($array['kdprod']); 
                $array['nmprod'] = strtoupper($array['nmprod']);
                $array['sku'] = strtoupper($array['sku']);
                $array['tipeprod'] = strtoupper($array['tipeprod']);
                $array['kdktgprod'] = strtoupper($array['kdktgprod']);
                $array['kdstnprod'] = strtoupper($array['kdstnprod']); 
                $array['notes'] = strtoupper($array['notes']); 
                $array['fstatus'] = 'TRUE';

                $resl = $this->db->insert('trx.m_prod',$array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdprod'])&& empty($array['stat'])){ 
                $array['nmprod'] = strtoupper($array['nmprod']);
                $array['sku'] = strtoupper($array['sku']);
                $array['tipeprod'] = strtoupper($array['tipeprod']);
                $array['kdktgprod'] = strtoupper($array['kdktgprod']);
                $array['kdstnprod'] = strtoupper($array['kdstnprod']); 
                $array['notes'] = strtoupper($array['notes']); 
                if($array['fstatus']==='t'){
                    $array['fstatus'] = 'TRUE'; 
                } else {
                    $array['fstatus'] = 'FALSE'; 
                }

                $this->db->where('kdprod', $array['kdprod']);
                $resl = $this->db->update('trx.m_prod', $array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdprod']) && !empty($array['stat'])){                
                $this->db->where('kdprod', $array['kdprod']);
                //$this->db->set("faktif","false");
                //$this->db->set("tgl_out",date('Y-m-d'));
                $resl = $this->db->delete('trx.m_prod');
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            } else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }


}
