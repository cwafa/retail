<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php  
                              echo form_input($form['kdktgprod']);
                              echo form_error('kdktgprod','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['nmktgprod']['placeholder']);
                              echo form_input($form['nmktgprod']);
                              echo form_error('nmktgprod','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div> 

              <div class="col-xs-3">
                  <div class="row">
                      <div class="form-group">
                          <?=form_label($form['fstatus']['placeholder']);?>
                          <div class="checkbox">
            							    <?php
                                  echo form_checkbox($form['fstatus']);
            							    ?>
            					    </div>
                      </div>
                  </div>
              </div> 
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdktgprod').val()){ 
            $('#fstatus').hide();
            // $('.btn-submit').click(function(){
            //     submit();
            // });
        } else if($('#kdktgprod').val()){ 
            $('#fstatus').hide();
            $('#fstatus').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            // $('.btn-submit').click(function(){
            // update();
            // });
        }

        $('#fstatus').change(function(){
        
          if ($("#fstatus").prop("checked")){
              $("#fstatus").val('t');
          } else {
              $("#fstatus").val('f');
          } 
        });
    });



    // function submit(){
    //     var kditemjns = $("#kditemjns").val();
    //     var kditemsat = $("#kditemsat").val();
    //     var kditemgr = $("#kditemgr").val();
    //     var kditem = $("#kditem").val();
    //     var nmitem = $("#nmitem").val().toUpperCase();
    //     //alert(nmitem);
    //   	$.ajax({
    //   		type: "POST",
    //   		url: "<?=site_url("m_ktgprod/submit");?>",
    //   		data: {"kditem":kditem,"nmitem":nmitem,"kditemgr":kditemgr,"kditemjns":kditemjns,"kditemsat":kditemsat},
    //   		success: function(resp){
    //   			var obj = jQuery.parseJSON(resp);
    //   			$.each(obj, function(key, data){
    //           if (data.tipe==="success"){
    //               swal({
    //                   title: data.title,
    //                   text: data.msg,
    //                   type: data.tipe
    //               }, function(){
    //                   window.location.href = '<?=site_url('m_ktgprod');?>';
    //               });
    //           }else{
    //               refresh();
    //           }
    //   			});
    //       },
    //       error:function(event, textStatus, errorThrown) {
    //       	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //       }
    //     });
    // }

    // function update(){
    //     var kditemjns = $("#kditemjns").val();
    //     var kditemsat = $("#kditemsat").val();
    //     var kditemgr = $("#kditemgr").val();
    //     var kditem = $("#kditem").val();
    //     var nmitem = $("#nmitem").val().toUpperCase();
    //     if ($("#faktif").prop("checked")){
    //       var faktif = 't';
    //     } else {
    //       var faktif = 'f';
    //     }
    //   	$.ajax({
    //   		type: "POST",
    //   		url: "<?=site_url("m_ktgprod/update");?>",
    //   		data: {"kditem":kditem,"nmitem":nmitem,"kditemgr":kditemgr,"kditemjns":kditemjns,"kditemsat":kditemsat,"faktif":faktif},
    //   		success: function(resp){
    //   			var obj = jQuery.parseJSON(resp);
    //   			$.each(obj, function(key, data){
    //           if (data.tipe==="success"){
    //               swal({
    //                   title: data.title,
    //                   text: data.msg,
    //                   type: data.tipe
    //               }, function(){
    //                   window.location.href = '<?=site_url('m_ktgprod');?>';
    //               });
    //           }else{
    //               refresh();
    //           }
    //   			});
    //       },
    //       error:function(event, textStatus, errorThrown) {
    //       	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //       }
    //     });
    // }

    function refresh(){
      window.location.reload();
    }
</script>
