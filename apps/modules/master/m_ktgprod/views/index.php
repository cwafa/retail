<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-header">
            <a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
            <a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="<?php echo $add;?>" >Tambah Data Baru</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="btn-refersh">Refresh</a>
                    </li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
                <div class="table-responsive">
                <table class="dataTable table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Nama Produk</th> 
                            <th style="text-align: center;">Status</th> 
                            <th style="width: 10px;text-align: center;">Edit</th>
                            <th style="width: 10px;text-align: center;">Hapus</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Nama Produk</th> 
                            <th style="text-align: center;">Status</th> 
                            <th style="width: 10px;text-align: center;">Edit</th>
                            <th style="width: 10px;text-align: center;">Hapus</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-refersh").click(function(){
            table.ajax.reload();
        });

        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "columnDefs": [
                { "targets": 0 , "data": null,"sortable": false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "orderable": false, "targets": 3 },
                { "orderable": false, "targets": 4 }
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('m_ktgprod/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            }
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });

    function refresh(){
        table.ajax.reload();
    }

    function deleted(kdktgprod){
        swal({
            title: "Konfirmasi Hapus!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var submit = "<?php echo $submit;?>"; 
            $.ajax({
                type: "POST",
                url: submit,
                data: {"kdktgprod":kdktgprod,"stat":"delete" },
                success: function(resp){ 
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            table.ajax.reload();
                            swal({
                                title: "Terhapus",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                location.reload();
                            });
                        }else{
                            swal("Terhapus", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
            });
        });
    }
</script>
