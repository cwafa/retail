<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_ktgprod
 *
 * @author adi
 */
class M_ktgprod extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('m_ktgprod/submit'),
            'add' => site_url('m_ktgprod/add'),
            'edit' => site_url('m_ktgprod/edit'),
            'reload' => site_url('m_ktgprod'),
        );
        $this->load->model('m_ktgprod_qry'); 

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->m_ktgprod_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->m_ktgprod_qry->getKategori();
    }*/ 

    public function submit() {  
        $kdktgprod = $this->input->post('kdktgprod'); 
        // echo $this->input->post('fstatus'); 
        $stat = $this->input->post('stat');
        if($this->validate($kdktgprod,$stat) == TRUE){
            $res = $this->m_ktgprod_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdktgprod)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdktgprod);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdktgprod)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdktgprod);
                $this->template->build('form', $this->data);
            }
        }
    } 

    private function _init_add(){

        if(isset($_POST['fstatus']) && strtoupper($_POST['fstatus']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdktgprod'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Kategori Produk',
                    'id'          => 'kdktgprod',
                    'name'        => 'kdktgprod',
                    'value'       => set_value('kdktgprod'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmktgprod'=> array(
                    'placeholder' => 'Nama Kategori Produk',
                    'id'      => 'nmktgprod',
                    'name'        => 'nmktgprod',
                    'value'       => set_value('nmktgprod'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => '',
      				'id'          => 'fstatus',
      				// 'value'       => 't',
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdktgprod = $this->uri->segment(3);
        }
        $this->_check_id($kdktgprod);

        if($this->val[0]['fstatus'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdktgprod'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kategori Produk',
                    'id'          => 'kdktgprod',
                    'name'        => 'kdktgprod',
                    'value'       => $this->val[0]['kdktgprod'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmktgprod'=> array(
                    'placeholder' => 'Nama Kategori Produk',
                    'id'          => 'nmktgprod',
                    'name'        => 'nmktgprod',
                    'value'       => $this->val[0]['nmktgprod'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),  
      		'fstatus'=> array(
                    'placeholder' => 'Status Item',
      				'id'          => 'fstatus',
      				// 'value'       => $faktifx,
      				'checked'     => $faktifx,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdktgprod){
        if(empty($kdktgprod)){
            redirect($this->data['add']);
        }

        $this->val= $this->m_ktgprod_qry->select_data($kdktgprod);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdktgprod,$stat) {
        if(!empty($stat)){
            return true;
        }
        $config = array( 
            array(
                    'field' => 'nmktgprod',
                    'label' => 'Nama Kategori Produk',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
