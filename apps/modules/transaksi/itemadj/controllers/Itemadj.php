<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Itemadj
 *
 * @author
 */
class Itemadj extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('itemadj/submit'),
            'add' => site_url('itemadj/add'),
            'edit' => site_url('itemadj/edit'),
            'reload' => site_url('itemadj'),
        );
        $this->load->model('itemadj_qry');
        $supplier = $this->itemadj_qry->getDataSupplier();
        foreach ($supplier as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function getKodeitemadj() {
        echo $this->itemadj_qry->getKodeitemadj();
    }

    public function json_dgview() {
        echo $this->itemadj_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->itemadj_qry->json_dgview_detail();
    }
    public function addDetail() {
        echo $this->itemadj_qry->addDetail();
    }

    public function detaildeleted() {
        echo $this->itemadj_qry->detaildeleted();
    }

    public function delete() {
        echo $this->itemadj_qry->delete();
    }

    public function submit() {
        echo $this->itemadj_qry->submit();
    }
    private function _init_add(){


    		if(isset($_POST['hrgblmada']) && strtoupper($_POST['hrgblmada']) == 'OK'){
    			$chk_hrg = TRUE;
    		} else{
    			$chk_hrg = FALSE;
    		}

        $this->data['form'] = array(
           'noitemadj'=> array(
                    'placeholder' => 'No. Transaksi',
                    //'type'        => 'hidden',
                    'id'          => 'noitemadj',
                    'name'        => 'noitemadj',
                    'value'       => set_value('noitemadj'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglitemadj'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tglitemadj',
                    'name'        => 'tglitemadj',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kdaks'=> array(
                    'attr'        => array(
                        'id'    => 'kdaks',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdaks'),
                    'name'     => 'kdaks',
                    'placeholder' => 'Cari Barang',
                    'required' => ''
            ),
            'nmaks'=> array(
                    'attr'        => array(
                        'id'    => 'nmaks',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmaks'),
                    'name'     => 'nmaks',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
                    'required'    => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $noitemadj = $this->uri->segment(3);
        }
        $this->_check_id($noitemadj);
        $this->data['form'] = array(
           'noitemadj'=> array(
                    'placeholder' => 'No. Transaksi',
                    //'type'        => 'hidden',
                    'id'          => 'noitemadj',
                    'name'        => 'noitemadj',
                    'value'       => $this->val[0]['noitemadj'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglitemadj'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tglitemadj',
                    'name'        => 'tglitemadj',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglitemadj']),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['ket'],
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kdaks'=> array(
                    'attr'        => array(
                        'id'    => 'kdaks',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdaks'),
                    'name'     => 'kdaks',
                    'placeholder' => 'Cari Barang',
                    'required' => ''
            ),
            'nmaks'=> array(
                    'attr'        => array(
                        'id'    => 'nmaks',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmaks'),
                    'name'     => 'nmaks',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
                    'required'    => '',
            ),
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->itemadj_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
