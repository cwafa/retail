<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 17, 2017 10:32:23 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php
$file = $this->uri->segment(4);
if($file==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Laporan Barang " .date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$this->load->library('table');
$caption = "<b>Laporan Pengeluaran Barang Per Transaksi</b>"
        . "<br>"
        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->session->userdata('data')['cabang'])."</b>"
        . "<br><br>";
// Caption text
$this->table->set_caption($caption);
$no = 1;
$total_all = '';
$tot_akhir = array();
foreach ($data as $value) {
    //var_dump($value['detail']);
    $namaheader = array( 
        array('data' => '<h5><b>&nbsp;Nomor Transaksi</h5>'
                            , 'colspan' => 4
                            , 'style' => 'text-align: left; width: 40%; font-size: 12px; background-color: #C9DFDE;'),
        array('data' => '<h5><b>Tanggal</h5>'
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; width: 30%; font-size: 12px; background-color: #C9DFDE;'),
        array('data' => '<h5><b>Keterangan</h5>'
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; width: 30%; font-size: 12px; background-color: #C9DFDE;'),
    );
    $this->table->add_row($namaheader);
    $header_data = array( 
        array('data' => "&nbsp;".$value['noitem_out']
                            , 'colspan' => 4
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => date_format(date_create($value['tglitem_out']),"d-m-Y")
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['ket']
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; font-size: 12px;'),
    );  
    $this->table->add_row($header_data);
    if(count($value['detail'])>0){
        $detail_header = array(
            array('data' => '&nbsp'
                            , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            array('data' => 'No.'
                                , 'style' => 'text-align: center; width: 5%; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Kode Item'
                                , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            array('data' => '&nbsp;Nama Item' 
                                , 'style' => 'text-align: left; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Jumlah&nbsp;' 
                                , 'style' => 'text-align: right; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Satuan'
                                , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Harga Jual&nbsp;' 
                                , 'style' => 'text-align: right; font-size: 12px;background-color: #ddd;'), 
            array('data' => 'Sub Total&nbsp;'
                                , 'style' => 'text-align: right; font-size: 12px;background-color: #ddd;'),
        );  
        $this->table->add_row($detail_header);
        $dno = 1;
        foreach ($value['detail'] as $v_detail) {
            $detail_data = array(
                array('data' => '&nbsp'
                                , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $dno
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $v_detail['kditem']
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => '&nbsp;'.$v_detail['nmitem'] 
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $v_detail['qty'].'&nbsp;'
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => $v_detail['nmitemsat']
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => number_format($v_detail['harga'],2,",",".").'&nbsp;'
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($v_detail['total'],2,",",".").'&nbsp;'
                                    , 'style' => 'text-align: right; font-size: 12px;'),
            );  
            $this->table->add_row($detail_data);
            $dno++;
        }
        $total = '';
        foreach ($value['detail2'] as $v_detail2) {
            $total = $v_detail2['tot_all'];
            $detail_footer = array(
                array('data' => '&nbsp'
                               , 'colspan' =>3
                               , 'style' => 'text-align: center; font-size: 12px;background-color: #fff;'),
                array('data' => '<h6><b>&nbsp;TOTAL ITEM :'
                               , 'style' => 'text-align: center; font-size: 12px;background-color: #fff;'),
                array('data' => '<h6><b>'.$v_detail2['jml_all'] 
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '&nbsp'
                               , 'style' => 'text-align: center; font-size: 12px;background-color: #fff;'),
                array('data' => '<h6><b>&nbsp;TOTAL HARGA :'
                               , 'style' => 'text-align: center; font-size: 12px;background-color: #fff;'),
                array('data' => '<h6><b>'.number_format($v_detail2['tot_all'],2,",",".")
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
            );  
            $this->table->add_row($detail_footer);
        }
        $total_last = $total - $value['pot'] + $value['byongkos'];
        $tot_akhir[] = $total - $value['pot'] + $value['byongkos'];
        $footer = array(
                array('data' => '<h7><b>&nbsp;Potongan :' 
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>'.number_format($value['pot'],2,",",".").'&nbsp;'
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>&nbsp;Pajak :' 
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>'.number_format('0',2,",",".").'&nbsp;'
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>&nbsp;Biaya Ongkos :' 
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>'.number_format($value['byongkos'],2,",",".").'&nbsp;'
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>&nbsp;Total Akhir :' 
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
                array('data' => '<h7><b>'.number_format($total_last,2,",",".").'&nbsp;'
                               , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'), 
            );  
        $total_all = array_sum($tot_akhir);
        $this->table->add_row($footer);
        $footer = array(
                array('data' => '<hr>' 
                               , 'colspan' => 8
                               , 'style' => 'text-align: left; font-size: 12px;background-color: #fff;'), 
            );  
        $this->table->add_row($footer);
      
    }
    $no++;
}
    $tot_footer = array(
            array('data' => '<h7><b>&nbsp;'
                            , 'colspan' => '6'
                            , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
            array('data' => '<h7><b>&nbsp;Total Semua Transaksi :' 
                            , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'),
            array('data' => '<h7><b>'.number_format($total_all,2,",",".").'&nbsp;'
                            , 'style' => 'text-align: right; font-size: 12px;background-color: #fff;'), 
        );   
    $this->table->add_row($tot_footer);
    $hr_footer = array(
        array('data' => '<hr>' 
                       , 'colspan' => 8
                       , 'style' => 'text-align: left; font-size: 12px;background-color: #fff;'), 
        );  
    $this->table->add_row($hr_footer);


$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
$this->table->set_template($template);
echo $this->table->generate();    
 