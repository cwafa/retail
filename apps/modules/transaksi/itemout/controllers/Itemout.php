<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Itemout
 *
 * @author
 */
class Itemout extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('itemout/submit'),
            'add' => site_url('itemout/add'),
            'edit' => site_url('itemout/edit'),
            'reload' => site_url('itemout'),
        );
        $this->load->model('itemout_qry'); 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function printAll() {
        $this->data['data'] = $this->itemout_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function submit() {
        $this->data['data'] = $this->itemout_qry->submit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data); 
    }

    public function getKodeitemout() {
        echo $this->itemout_qry->getKodeitemout();
    }  

    public function json_dgview() {
        echo $this->itemout_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->itemout_qry->json_dgview_detail();
    }

    public function addDetail() {
        echo $this->itemout_qry->addDetail();
    }

    public function detaildeleted() {
        echo $this->itemout_qry->detaildeleted();
    }
    public function delete() {
        echo $this->itemout_qry->delete();
    }

    public function save() {
        echo $this->itemout_qry->save();
    }

    public function set_harga() {
        echo $this->itemout_qry->set_harga();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'noitem_out'=> array(
                    'placeholder' => 'No. Transaksi',
                    //'type'        => 'hidden',
                    'id'          => 'noitem_out',
                    'name'        => 'noitem_out',
                    'value'       => set_value('noitem_out'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglitem_out'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tglitem_out',
                    'name'        => 'tglitem_out',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ), 
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),  
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
            //        'required'    => ''
            ),
            'kditem'=> array(
                    'attr'        => array(
                        'id'    => 'kditem',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kditem'),
                    'name'     => 'kditem',
                    'placeholder' => 'Kode Barang'
            ),
            'nmitem'=> array(
                    'attr'        => array(
                        'id'    => 'nmitem',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmitem'),
                    'name'     => 'nmitem',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'placeholder' => 'Harga',
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
                    'required'    => '',
                    'readonly'    => '',
            ),

            //lain lain 
            'tot1'=> array(
                    'placeholder' => 'Total',
                    'id'    => 'tot1',
                    'class' => 'form-control',
                    'value'    => set_value('tot1'),
                    'name'     => 'tot1',
                    'required'    => '',
                    'readonly'    => '',
            ),
            'pot'=> array(
                    'placeholder' => 'Potongan',
                    'id'    => 'pot',
                    'class' => 'form-control',
                    'value'    => set_value('pot'),
                    'name'     => 'pot',
                    'required'    => '',
                        'onkeyup'     => 'sum()',
            ),
            'byongkos'=> array(
                    'placeholder' => 'Biaya Ongkos',
                    'id'    => 'byongkos',
                    'class' => 'form-control',
                    'value'    => set_value('byongkos'),
                    'name'     => 'byongkos',
                    'required'    => '',
                        'onkeyup'     => 'sum()',
            ),
            'tot2'=> array(
                    'placeholder' => 'Total dengan potongan & biaya ongkos',
                    'id'    => 'tot2',
                    'class' => 'form-control',
                    'value'    => set_value('tot2'),
                    'name'     => 'tot2',
                    'required'    => '',
                    'readonly'    => '',
            ),
            'byr'=> array(
                    'placeholder' => 'Bayar',
                    'id'    => 'byr',
                    'class' => 'form-control',
                    'value'    => set_value('byr'),
                    'name'     => 'byr',
                    'required'    => '',
                        'onkeyup'     => 'sum()',
            ),
            'tot_akhir'=> array(
                    'placeholder' => 'Total',
                    'id'    => 'tot_akhir',
                    'class' => 'form-control',
                    'value'    => set_value('tot_akhir'),
                    'name'     => 'tot_akhir',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'tgltrans'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tgltrans',
                    'name'        => 'tgltrans',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nojurnal = $this->uri->segment(3);
        }
        $this->_check_id($nojurnal);
        $this->data['form'] = array(
               'noitem_out'=> array(
                        'placeholder' => 'No. Transaksi',
                        //'type'        => 'hidden',
                        'id'          => 'noitem_out',
                        'name'        => 'noitem_out',
                        'value'       => $this->val[0]['noitem_out'],
                        'class'       => 'form-control',
                        'style'       => '',
                        'readonly'    => '',
                ),
               'tglitem_out'=> array(
                        'placeholder' => 'Tanggal Transaksi',
                        'id'          => 'tglitem_out',
                        'name'        => 'tglitem_out',
                        'value'       => $this->apps->dateConvert($this->val[0]['tglitem_out']),
                        'class'       => 'form-control calendar',
                        'style'       => '',
                ),
                'ket'=> array(
                        'placeholder' => 'Keterangan',
                        'id'      => 'ket',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => $this->val[0]['ket'],
                        'name'     => 'ket',
                //        'required'    => '',
                //        'onkeyup'     => 'myFunction()',
                //        'style'       => 'text-transform: uppercase;',
                ), 
                'nourut'=> array(
                        'id'    => 'nourut',
                        'type'    => 'hidden',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nourut'),
                        'name'     => 'nourut',
                //        'required'    => ''
                ),
                'kditem'=> array(
                        'attr'        => array(
                            'id'    => 'kditem',
                            'class' => 'form-control',
                        ),
                        'data'     =>  '',
                        'value'    => set_value('kditem'),
                        'name'     => 'kditem',
                        'placeholder' => 'Kode Barang'
                ),
                'nmitem'=> array(
                        'attr'        => array(
                            'id'    => 'nmitem',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                        'class' => 'form-control',
                        'value'    => set_value('nmitem'),
                        'name'     => 'nmitem',
                        'readonly' => '',
                ),
                'qty'=> array(
                        'placeholder' => 'Quantity',
                        'id'    => 'qty',
                        'class' => 'form-control',
                        'value'    => set_value('qty'),
                        'name'     => 'qty',
                        'required'    => '',
                ),
                'harga'=> array(
                        'placeholder' => 'Harga',
                        'id'    => 'harga',
                        'class' => 'form-control',
                        'value'    => set_value('harga'),
                        'name'     => 'harga',
                        'required'    => '',
                        'readonly'    => '',
                ),

                //lain lain 
                'tot1'=> array(
                        'placeholder' => 'Total',
                        'id'    => 'tot1',
                        'class' => 'form-control',
                        'value'    => set_value('tot1'),
                        'name'     => 'tot1',
                        'required'    => '',
                        'readonly'      => '',
                ),
                'pot'=> array(
                        'placeholder' => 'Potongan',
                        'id'    => 'pot',
                        'class' => 'form-control',
                        'value'    => $this->val[0]['pot'],
                        'name'     => 'pot',
                        'required'    => '',
                        'onkeyup'     => 'sum()',
                ),
                'byongkos'=> array(
                        'placeholder' => 'Biaya Ongkos',
                        'id'    => 'byongkos',
                        'class' => 'form-control',
                        'value'    => $this->val[0]['byongkos'],
                        'name'     => 'byongkos',
                        'required'    => '',
                        'onkeyup'     => 'sum()',
                ),
                'tot2'=> array(
                        'placeholder' => 'Total',
                        'id'    => 'tot2',
                        'class' => 'form-control',
                        'value'    => set_value('tot2'),
                        'name'     => 'tot2',
                        'required'    => '',
                        'readonly'      => '',
                ),
                'byr'=> array(
                        'placeholder' => 'Bayar',
                        'id'    => 'byr',
                        'class' => 'form-control',
                        'value'    => $this->val[0]['byr'],
                        'name'     => 'byr',
                        'required'    => '',
                        'onkeyup'     => 'sum()',
                ),
                'tot_akhir'=> array(
                        'placeholder' => 'Total Akhir',
                        'id'    => 'tot_akhir',
                        'class' => 'form-control',
                        'value'    => set_value('tot_akhir'),
                        'name'     => 'tot_akhir',
                        'required'    => '',
                        'readonly'      => '',
                ),
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->itemout_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'noref',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
