<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptitem_io_bln_qry
 *
 * @author adi
 */
class Rptitem_io_bln_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function json_dgview() {
        error_reporting(-1); 
        if( isset($_GET['periode']) ){
            if($_GET['periode']){
                $tgl2 = explode('-',$_GET['periode']);
                $periode = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode = '';
            }
        }else{
            $periode = '';
        }

        $aColumns = array('kditem', 'nmitem', 'nmitemjns', 'harga', 'saw_qty', 'saw_total', 'in_qty', 'in_total', 'out_qty', 'out_total', 'sak_qty', 'sak_total');
        $sIndexColumn = "nmitem";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 
        $sTable=" ( SELECT kditem, nmitem, nmitemjns, harga, saw_qty, saw_total, in_qty, in_total, out_qty, out_total, sak_qty, sak_total
                                FROM apotek.vl_item_io_bln('".$this->session->userdata('data')['kddiv']."','".$periode."')) AS a"; 
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
  			{
  					if($_GET['iDisplayStart']>0){
  							$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
  											intval( $_GET['iDisplayStart'] );
  					}
  			}

  			$sOrder = "";
  			if ( isset( $_GET['iSortCol_0'] ) )
  			{
  							$sOrder = " ORDER BY  ";
  							for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
  							{
  											if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
  											{
  															$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
  																			($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
  											}
  							}

  							$sOrder = substr_replace( $sOrder, "", -2 );
  							if ( $sOrder == " ORDER BY" )
  							{
  											$sOrder = "";
  							}
  			}
  			$sWhere = "";

  			if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
  			{
  	$sWhere = " WHERE (";
  	for ( $i=0 ; $i<count($aColumns) ; $i++ )
  	{
  		$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  	}
  	$sWhere = substr_replace( $sWhere, "", -3 );
  	$sWhere .= ')';
  			}

  			for ( $i=0 ; $i<count($aColumns) ; $i++ )
  			{

  					if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
  					{
  							if ( $sWhere == "" )
  							{
  									$sWhere = " WHERE ";
  							}
  							else
  							{
  									$sWhere .= " AND ";
  							}
  							//echo $sWhere."<br>";
  							$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
  					}
  			}


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "order by nmitem";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }
}
