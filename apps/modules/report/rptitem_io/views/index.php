<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => '_blank'
                        , 'class' => "form-inline");
				echo form_open($submit,$attributes);
			?>
			<div class="box-body"> 
                <div class="form-group">
                 	<?php 
                        echo form_label('Pilih Tanggal Transaksi&nbsp;');
                        echo form_input($form['periode_awal']);
                        echo form_error('periode_awal','<div class="note">','</div>');  
                    ?>
                </div>
                <button type="button" class="btn btn-primary btn-tampil">Tampil</button> 

                <div class="form-group">
                 	<?php  
                        echo form_input($form['potongan']);
                        echo form_error('potongan','<div class="note">','</div>'); 
                        echo form_input($form['byongkos']);
                        echo form_error('byongkos','<div class="note">','</div>'); 
                    ?>
                </div>
                <div class="row">                	
                	<div class="col-md-12">
                		<br>
                    	<div style="border-top: 1px solid #ddd; height: 10px;"></div>
                	</div> 
                </div>
                <?php echo form_close(); ?>  

				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 20%; text-align: center;" rowspan="2">Kode Item</th>
								<th style="width: 14%; text-align: center;" rowspan="2">Nama Item</th>
								<th style="width: 14%; text-align: center;" rowspan="2">Jenis</th>
								<th style="width: 13%; text-align: center;" rowspan="2">Harga</th>
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Awal</th>
								<th style="width: 10%; text-align: center;" colspan="2">Masuk</th>
								<th style="width: 10%; text-align: center;" colspan="2">Keluar</th> 
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Akhir</th>
							</tr>
							<tr>
								<td style="text-align: center;">Sisa Kemarin</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Item Baru</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Item Terjual</td>
								<td style="text-align: center;">Total</td> 

								<td style="text-align: center;">Sisa</td>
								<td style="text-align: center;">Total</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<td style="text-align: center;" colspan="5"></td>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th> 
							</tr>
						</tfoot>
					</table>
				</div> 
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		getdata();
		set_biaya();
		
		$('.btn-tampil').click(function () {
			getdata();
		set_biaya();
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		}); 
	}); 

	function set_biaya(){  
		var periode_awal = $("#periode_awal").val();  
		$.ajax({
			type: "POST",
			url: "<?=site_url("rptitem_io/set_biaya");?>",
			data: {"periode_awal":periode_awal },
			success: function(resp){
				var obj = JSON.parse(resp);
				$.each(obj, function(key, data){
					$('#potongan').val(data.potongan);
					$('#byongkos').val(data.byongkos);
				});
			},
			error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		}); 
	}

	function getdata(){
		var periode_awal = $("#periode_awal").val();  
		var column = [];

		column.push({
			"aTargets": [ 3,5,7,9,11 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right"
		});

		column.push({
			"aTargets": [ 4,6,8,10 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "kditem" },
				{ "data": "nmitem" },
				{ "data": "nmitemjns" },
				{ "data": "harga" },
				{ "data": "saw_qty" },
				{ "data": "saw_total" },
				{ "data": "in_qty" },
				{ "data": "in_total" },
				{ "data": "out_qty" },
				{ "data": "out_total" }, 
				{ "data": "sak_qty" },
				{ "data": "sak_total" }
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push({ "name": "periode_awal", "value": periode_awal });
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptitem_io/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var saw_tot = api
					.column( 5 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var in_tot = api
					.column( 7 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var out_tot = api
					.column( 9 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 ); 
					
				var potongan = $('#potongan').val();
				var byongkos = $('#byongkos').val();
				var out_tot_real = parseFloat(out_tot) - parseFloat(potongan) + parseFloat(byongkos);
				var sak_tot =  parseInt(saw_tot+in_tot-out_tot_real);

				// Update footer by showing the total with the reference of the column index
				$( api.column( 1 ).footer() ).html('<b>Total</b>');
				$( api.column( 5 ).footer() ).html(numeral(saw_tot).format('0,0.00'));
				$( api.column( 7 ).footer() ).html(numeral(in_tot).format('0,0.00'));
				$( api.column( 9 ).footer() ).html(numeral(out_tot_real).format('0,0.00')); 
				$( api.column( 11 ).footer() ).html(numeral(sak_tot).format('0,0.00'));
			},
			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true, header: true,
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '12px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		}); 
	}
</script>
