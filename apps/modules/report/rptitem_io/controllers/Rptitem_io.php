<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptitem_io
 *
 * @author adi
 */
class Rptitem_io extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptitem_io/submit'),
            'add' => site_url('rptitem_io/add'),
            'edit' => site_url('rptitem_io/edit'),
            'reload' => site_url('rptitem_io'),
        );
        $this->load->model('rptitem_io_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->rptitem_io_qry->json_dgview();
    }

    public function set_biaya() {
        echo $this->rptitem_io_qry->set_biaya();
    }

    private function _init_add(){
        $this->data['form'] = array( 
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ), 
           'potongan'=> array(
                    'placeholder' => '',
                    'type'        => 'hidden',
                    'id'          => 'potongan',
                    'name'        => 'potongan',
                    'value'       => set_value('potongan'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'byongkos'=> array(
                    'placeholder' => '',
                    'type'        => 'hidden',
                    'id'          => 'byongkos',
                    'name'        => 'byongkos',
                    'value'       => set_value('byongkos'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
        );
    }
}
