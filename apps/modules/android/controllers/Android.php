<?php

defined('BASEPATH') OR exit('No direct script Android allowed');
require_once APPPATH . "/libraries/myjwt/JWT.php";

use \Firebase\JWT\JWT;

class Android extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('android_qry');
    }

    public function index() {
        $this->load->view('index');
    }

    public function getRugiLaba() {
        try {
            
            $url = $this->input->post('url');
            $bulan_awal = $this->input->post('bulan_awal');
            $bulan_akhir = $this->input->post('bulan_akhir');
            $tahun = $this->input->post('tahun');
            $rdjenis = $this->input->post('rdjenis');
            $kddiv = $this->input->post('kddiv');

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/android/getRugiLabaAll",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 120,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "bulan_awal: " . $bulan_awal,
                    "bulan_akhir: " . $bulan_akhir,
                    "tahun: " . $tahun,
                    "rdjenis: " . $rdjenis,
                    "kddiv: " . $kddiv,
                ),
            ));

            $response = curl_exec($curl);

        //echo "<script> console.log('PHP: ". $response ."');</script>";

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi." . $err);
                }
            } else {
                if ($response !== 'false') {
                    $dt = json_decode($response);
                    $sum_periode = array();
                    foreach ($dt->data[0] as $k => $v) {
                        if ($k !== "level" && $k !== "kdakun" & $k !== "nmakun") {
                            $sum_periode[$k] = 0;
                            $sum_periode['p_aktif'] = (int) $dt->periode_aktif;
                        }
                    }

                    foreach ($dt->data as $k => $v) {
                        foreach ($v as $vk => $d) {
                            if (strlen($v->kdakun) === 3) {
                                if (is_numeric($d) && is_numeric($vk)) {
                                    $sum_periode[$vk] = (float) $sum_periode[$vk] + (float) $d;
                                }
                            }
                        }
                    }

                    echo json_encode($sum_periode);
                }
            }
        } catch (Exception $e) {
            $res = array(
                "stat" => 0,
                "msg" => "Error : " . $e->getMessage(),
            );
            echo json_encode($res);
        }
    }

    public function getStokAllUnit() {
        try {
            $url = $this->input->post('url');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/android/getStokHargaUnit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: " . "pzu",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, " . $err . " . Silahkan Coba beberapa saat lagi.");
                }
            } else {
                echo $response;
            }
        } catch (Exception $e) {
            echo "Error : " . $e;
        }
    }

    public function getStokHargaUnit() {
        echo $this->android_qry->getStokHargaUnit();
    }

    public function gettargetbulan() {
        $dt = array();
        foreach (getallheaders() as $name => $value) {
            $dt[$name] = $value;
        }

        if (isset($dt['periode'])) {
            $res = $this->android_qry->gettargetbulan($dt['periode']);
            echo json_encode($res);
        }
    }



    public function getJualAllCab() {
        $dt = array();
        foreach (getallheaders() as $name => $value) {
            $dt[$name] = $value;
        }

        if (isset($dt['periode'])) {
            $res = $this->android_qry->getJualAllCab($dt['periode']);
            echo json_encode($res);
        }
    }




    public function getblmtrmpoleasing() {
        $dt = array();
        foreach (getallheaders() as $name => $value) {
            $dt[$name] = $value;
        }

        if (isset($dt['periode']) && isset($dt['nm_cabang'])) {
            $res = $this->android_qry->getblmtrmpoleasing($dt['nm_cabang'], $dt['periode']);
            echo json_encode($res);
        }
    }

    public function getblmtrmpoleasingdetail() {
        $dt = array();
        foreach (getallheaders() as $name => $value) {
            $dt[$name] = $value;
        }

        if (isset($dt['periode']) && isset($dt['kdleasing'])) {
            $res = $this->android_qry->getblmtrmpoleasingdetail($dt['kdleasing'], $dt['periode']);
            echo json_encode($res);
        }
    }

    public function getRugiLabaAll() {
        $this->load->model('rptrl/rptrl_qry');
        echo $this->rptrl_qry->submit();
    }

    public function getAksesMenu() {
        $token = $this->input->post('token');
        if ($this->validateToken($token)) {
            $menu_app = $this->rbac->menu_app('');
            echo json_encode($menu_app);
        } else {
            echo "";
        }
    }

    public function getJualPerBulan() {
        $token = $this->input->post('token');
        if ($this->validateToken($token)) {
            $this->load->model('dashboard/dashboard_qry');
            echo $this->dashboard_qry->getJualPerBulan();
        } else {
            echo "";
        }
    }

    public function validateToken($token) {
        try {
            $result = JWT::decode($token, $this->rbac->key, array('HS256'));
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function checklogin() {
        $token = $this->input->post('token');
        if ($token) {
            $decoded = $this->decode($token);
            echo json_encode($decoded);
        } else {
            echo "";
        }
    }

    private function decode($token) {
        $res = array();
        try {
            $result = JWT::decode($token, $this->rbac->key, array('HS256'));
            foreach ($result as $key => $value) {
                $res[$key] = $value;
            }
            $res['exp'] = time() + JWT::$leeway + 300;
            $res['token'] = JWT::encode($res, $this->rbac->key);
        } catch (Exception $e) {
            // ignore firebase's expired exception because we will throw our own later
            $res = array(
                'state' => '0',
                'logged_in' => '0',
                'title' => 'Login Gagal',
                'msg' => $e->getMessage(),
            );
            return $res;
        }
        return $res;
    }

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->_validate($username, $password) == TRUE) {
            $res = $this->android_qry->submit();
        } else {
            $res = array(
                'state' => '0',
                'logged_in' => '',
                'title' => 'Login Gagal',
                'msg' => validation_errors(),
            );
        }
        echo json_encode($res);
    }

    // Service GLR_H Start
    public function getGlrhAkunDiv() {
        $dt = $this->android_qry->getGlrhAkunDiv();
        echo json_encode($dt);
    }

    public function reqGlrhAkunDiv($url = null) {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getGlrhAkunDiv",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode:"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if ($response !== 'false') {
                    echo $response;
                    //$dt = json_decode($response);
                    //var_dump($dt);
                }
            }
        } catch (Exception $e) {
            $res = array(
                "stat" => 0,
                "msg" => "Error : " . $e->getMessage(),
            );
            echo json_encode($res);
        }
    }

    public function getGlrhNeraca() {
        $dt = $this->android_qry->getGlrhNeraca();
        echo json_encode($dt);
    }

    public function reqGlrhNeraca($url = null) {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getGlrhNeraca",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode:"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if ($response !== 'false') {
                    echo $response;
                    //$dt = json_decode($response);
                    //var_dump($dt);
                }
            }
        } catch (Exception $e) {
            $res = array(
                "stat" => 0,
                "msg" => "Error : " . $e->getMessage(),
            );
            echo json_encode($res);
        }
    }

    public function getGlrhTJurnal() {
        $dt = $this->android_qry->getGlrhTJurnal();
        echo json_encode($dt);
    }

    public function reqGlrhTJurnal($url = null) {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getGlrhTJurnal",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode:"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if ($response !== 'false') {
                    $dt = json_decode($response);
                    $this->logGlrH($dt->stat, $dt->msg);
                    if ($dt->stat === 1) {
                        $query = $this->db->get_where("imp.t_jurnal", array("periode" => $dt->data[0]->periode, "kddiv" => $dt->data[0]->kddiv));
                        if ($query->num_rows() === 0) {
                            $this->db->insert_batch('imp.t_jurnal', $dt->data);
                            if ($this->db->trans_status() === FALSE) {
                                $dt->msg .= " | " . $this->db->error();
                                $this->logGlrH($dt->stat, $this->db->error());
                            } else {
                                $dt->msg .= " | " . " imp.t_jurnal Data berhasil di import!";
                                $this->logGlrH($dt->stat, $url . " imp.t_jurnal Data berhasil di import!");
                            }
                        } else {
                            $dt->msg .= " | " . " Data sudah ada, Aksi tidak dapat dilanjutkan!";
                            $this->logGlrH($dt->stat, $query->num_rows() . " Data sudah ada, Aksi tidak dapat dilanjutkan!");
                        }
                    }
                    //var_dump($dt);
                    $res = array(
                        "stat" => 1,
                        "msg" => $url . " imp.t_jurnal " . $dt->msg,
                    );
                } else {
                    $res = array(
                        "stat" => 0,
                        "msg" => "Error : " . $response,
                    );
                }
            }
            echo json_encode($res);
        } catch (Exception $e) {
            $res = array(
                "stat" => 0,
                "msg" => "Error : " . $e->getMessage(),
            );
            echo json_encode($res);
        }
    }

    public function getGlrhTJurnalD() {
        $dt = $this->android_qry->getGlrhTJurnalD();
        echo json_encode($dt);
    }

    public function reqGlrhTJurnalD($url = null) {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getGlrhTJurnalD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode:"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if ($response !== 'false') {
                    $dt = json_decode($response);
                    $this->logGlrH($dt->stat, $dt->msg);
                    if ($dt->stat === 1) {
                        $this->db->where(array("t_jurnal.periode" => $dt->data[0]->periode
                            , "t_jurnal.kddiv" => $dt->data[0]->kddiv));
                        $this->db->join("imp.t_jurnal", "t_jurnal.nojurnal = t_jurnal_d.nojurnal");
                        $query = $this->db->get("imp.t_jurnal_d");

                        if ($query->num_rows() === 0) {
                            $arr = array();
                            foreach ($dt->data as $value) {
                                $arr[] = array(
                                    "nojurnal" => $value->nojurnal,
                                    "nourut" => $value->nourut,
                                    "kddiv" => $value->kddiv,
                                    "kdakun" => $value->kdakun,
                                    "dk" => $value->dk,
                                    "nominal" => $value->nominal,
                                    "ket" => $value->ket,
                                );
                            }
                            $this->db->insert_batch('imp.t_jurnal_d', $arr);
                            if ($this->db->trans_status() === FALSE) {
                                $dt->msg .= " | " . $this->db->error();
                                $this->logGlrH($dt->stat, $this->db->error());
                            } else {
                                $dt->msg .= " | " . " imp.t_jurnal_d Data berhasil di import!";
                                $this->logGlrH($dt->stat, $url . " imp.t_jurnal_d Data berhasil di import!");
                            }
                        } else {
                            $dt->msg .= " | " . " Data sudah ada, Aksi tidak dapat dilanjutkan!";
                            $this->logGlrH($dt->stat, $query->num_rows() . " Data sudah ada, Aksi tidak dapat dilanjutkan!");
                        }
                    }
                    //var_dump($dt);
                    $res = array(
                        "stat" => 1,
                        "msg" => $url . " imp.t_jurnal_d " . $dt->msg,
                    );
                } else {
                    $res = array(
                        "stat" => 0,
                        "msg" => "Error : " . $response,
                    );
                }
            }
            echo json_encode($res);
        } catch (Exception $e) {
            $res = array(
                "stat" => 0,
                "msg" => "Error : " . $e->getMessage(),
            );
            echo json_encode($res);
        }
    }

    private function logGlrH($stat = null, $message = null) {
        $my_file = './files/log-import-glrh.log';
        if (!file_exists($my_file)) {
            $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
        }

        $handle = fopen($my_file, 'a') or die('Cannot open file:  ' . $my_file);
        $data = date('Y-m-d H:i:s') . ", " . $stat . ", " . $message . "\n";
        fwrite($handle, $data);
        fclose($handle);
    }

    // Service GLR_H End

    private function _validate($username, $password) {
        if (!empty($username) && !empty($password)) {
            return true;
        }
        $config = array(
            array(
                'field' => 'username',
                'label' => 'User Name',
                'rules' => 'required|alpha_numeric_spaces|max_length[25]',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|alpha_numeric_spaces|max_length[25]',
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            return false;
        } else {
            return true;
        }
    }

}
