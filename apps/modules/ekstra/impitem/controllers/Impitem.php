<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Impitem
 *
 * @author
 */

class Impitem extends MY_Controller {
    public $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('impitem/submit'),
            'add' => site_url('impitem/add'),
            'edit' => site_url('impitem/edit'),
            'reload' => site_url('impitem'),
        );
        $this->load->model('impitem_qry');
        $this->load->helper(array('url','download'));
        require_once APPPATH.'libraries/PHPExcel.php';
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    } 

    public function fromExcelToLinux($excel_time){
      return ($excel_time-25569)*86400;
    }

    private function _init_add(){
        $this->data['form'] = array(
        );
    }

    public function Item() {
        //if(isset($_FILES["file"]["name"])){
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();

                for($row=2; $row<=$highestRow; $row++){
                    $kodeitem                   = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $barcode                    = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $namaitem                   = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $jenis                      = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $satuan                     = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $kategori                   = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $merek                      = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $satuandasar                = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $konversisatuandasar        = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $hargapokok                 = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $hargajual                  = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $stok                       = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $stokminimum                = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $tipeitem                   = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $rak                        = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $kodesupplier               = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $keterangan                 = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $beratsatuanterkecil        = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $satuanberatsatuanterkecil  = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                    $denganexpiredate           = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $tanggalexpired             = $worksheet->getCellByColumnAndRow(20, $row)->getValue();


                    // $xpodate = $this->fromExcelToLinux($podate);
                    // $tglbyr = $this->fromExcelToLinux($tanggalpembayaran);

                    if(empty($kodeitem)){  //jika kode tidak null -> simpan

                        $data = 'empty';
                    } else {
                        //jika deskripsi, tipe, harga = null
                        if(empty($kodeitem)){$kodeitem = '';}
                        if(empty($barcode)){$barcode = '';}
                        if(empty($namaitem)){$namaitem = '';}
                        if(empty($jenis)){$jenis = '';}
                        if(empty($satuan)){$satuan = '';}
                        if(empty($kategori)){$kategori = '';}
                        if(empty($merek)){$merek = '';}
                        if(empty($satuandasar)){$satuandasar = '';}
                        if(empty($konversisatuandasar)){$konversisatuandasar = '';}
                        if(empty($hargapokok)){$hargapokok = 0;}
                        if(empty($hargajual)){$hargajual = 0;}
                        if(empty($stok)){$stok = 0;}
                        if(empty($stokminimum)){$stokminimum = '';}
                        if(empty($tipeitem)){$tipeitem = '';}
                        if(empty($rak)){$rak = '';}
                        if(empty($kodesupplier)){$kodesupplier = 'SP-001';}
                        if(empty($keterangan)){$keterangan = '';}
                        if(empty($beratsatuanterkecil)){$beratsatuanterkecil = '';}
                        if(empty($satuanberatsatuanterkecil)){$satuanberatsatuanterkecil = '';}
                        if(empty($denganexpiredate)){$denganexpiredate = '';}
                        if(empty($tanggalexpired)){$tanggalexpired = '';} 

                        $data[] = array(
                            'kodeitem'                  => $kodeitem,
                            'barcode'                   => $barcode,
                            'namaitem'                  => $namaitem,
                            'jenis'                     => $jenis,
                            'satuan'                    => $satuan,
                            'kategori'                  => $kategori,
                            'merek'                     => $merek,
                            'satuandasar'               => $satuandasar,
                            'konversisatuandasar'       => $konversisatuandasar,
                            'hargapokok'                => $hargapokok,
                            'hargajual'                 => $hargajual,
                            'stok'                      => $stok,
                            'stokminimum'               => $stokminimum,
                            'tipeitem'                  => $tipeitem,
                            'rak'                       => $rak,
                            'kodesupplier'              => $kodesupplier,
                            'keterangan'                => $keterangan,
                            'beratsatuanterkecil'       => $beratsatuanterkecil,
                            'satuanberatsatuanterkecil' => $satuanberatsatuanterkecil,
                            'denganexpiredate'          => $denganexpiredate,
                            'tanggalexpired'            => $tanggalexpired 
                        );
                        // $res = $this->impitem_qry->insertItem($data);
                        // echo $res
                    }
                }
                return json_encode($data);
            //}
            }
            // return $data;
    }
 

    public function importItem() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->Item();

            echo $this->impitem_qry->insertItem(json_decode($data));
        }
    } 
}
