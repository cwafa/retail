<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
			<!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
			-->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="form-import main-form">

                        <div class="col-lg-8">

                        	<div class="form-group">
                                <label for="exampleInputFile">Data Item / Obat</label>

                                <div class="fileinputItem fileinput-new input-group" name="fileinputItem" id="fileinputItem" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                    	<i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    	<span class="fileinput-filename"></span>
                                	</div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Cari...</span>
                                      	<span class="fileinput-exists">Ubah</span>
                                      	<input type="file" name="fileItem" id="fileItem" data-original-value="" required accept=".xls, .xlsx">
                                	</span>

    								<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

    								<span class="input-group-btn fileinput-exists">
    									<button class="btn btn-primary" name="importItem" id="importItem"><i class="fa fa-cloud-upload"></i> Import</button>
                                	</span>

                              	</div>
    					  	</div>

                        </div> 

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
 
                    </div>
                </div>


                <span id="status" class="hidden"></span>
                <div class="form-group" id="process" style="display:none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" id="file-progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

            </div> 

        	<?php echo form_close(); ?>
      	</div>
      	<!-- /.box -->
	</div>
</div>



<script type="text/javascript">
    $(document).ready(function () {

        $(".btn-reset").click(function(){
            //disabled();
            window.location.reload();
        }); 




        //import data PemIteman
    	$('#importItem').click(function(event){
            event.preventDefault();
            var formData = new FormData();
    		formData.append('file', $('input[type=file]')[0].files[0]);
    		formData.append('kddiv',  $("#kddiv").val());

    		$.ajax({
    			url:"<?php echo base_url(); ?>impitem/importItem",
    			method:"POST",
                data:formData,
                dataType:'json',
    			contentType:false,
    			cache:false,
    			processData:false,
   				success:function(resp){

                    var obj = resp;
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputItem').fileinput('clear');
                                    $('#importItem').attr('disabled', false);
                                });
                            }
                        });
                }
            })
    	});

        function progress_bar_process(percentage, timer){

            $('.progress-bar').css('width', percentage + '%');
            if(percentage > 100){
                clearInterval(timer);
                $('#process').css('display', 'none');
                $('.progress-bar').css('width', '0%');

                $('#importItem').attr('disabled', false);
                $('#importServis').attr('disabled', false);
                $('#importJual').attr('disabled', false);
                $('#importKB').attr('disabled', false);

                setTimeout(function(){
                    }, 5000);
                $('#status').html("ok");
            }
        }

        function disabled(){
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
            //$("#fileKB").remove();
            //$("#fileItem").remove();
            //$("#fileJual").remove();
            //$("#fileServis").remove();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }


    });

 
</script>
