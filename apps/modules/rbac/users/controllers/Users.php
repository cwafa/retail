<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Users
 *
 * @author adi
 */
class Users extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,

			'submit' => site_url('users/submit'),
			'add' => site_url('users/add'),
			'edit' => site_url('users/edit'),
			'reload' => site_url('users'),
			);
		$this->load->model('users_qry');

		$kdgrup = $this->users_qry->getGroupUser();
		$this->data['group_id'][''] = '-- Grup Pengguna --';
		foreach ($kdgrup as $value) {
			$this->data['group_id'][$value['kdgroups']] = $value['nmgroups'];
		}	
	}

	//redirect if needed, otherwise display the user list

	public function index(){
		$this->template
		->title($this->data['msg_main'],$this->apps->name)
		->set_layout('main')
		->build('index',$this->data);
	}

	public function add(){
		if(!empty($this->rbac->module_access('add'))){
			redirect('debug/err_505');
		}
		$this->_init_add();
		$this->data['msg_detail'] = "Tambah Pengguna";
		$this->template
		->title($this->data['msg_main'],$this->apps->name)
		->set_layout('main')
		->build('form',$this->data);
	}

	public function edit() {
		if(!empty($this->rbac->module_access('edit'))){
			redirect('debug/err_505');
		}
		$this->_init_edit();
		$this->data['msg_detail'] = "Edit Pengguna";
		$this->template
		->title($this->data['msg_main'],$this->apps->name)
		->set_layout('main')
		->build('form',$this->data);
	}

	public function submit() {  

		$kduser = $this->input->post('kduser');
		$stat = $this->input->post('stat');

		if($this->validate($kduser,$stat) == TRUE){
//echo "<script> console.log('valid');</script>";
			$res = $this->users_qry->submit();
			if(empty($stat)){
				$data = json_decode($res);
//echo "<script> console.log('1');</script>";
				if($data->state==="0"){
//echo "<script> console.log('state=0');</script>";
					if(empty($kduser)){
						$this->_init_add();
						$this->template->build('form', $this->data);
					}else{
						$this->_init_edit($kduser);
						$this->template->build('form', $this->data);
					}
				}else{
//echo "<script> console.log('state=1');</script>";
					redirect($this->data['reload']);
				}
			}else{
//echo "<script> console.log('sukses delete');</script>";
				echo $res;
			}
		}else{
			if(empty($kduser)){
echo "<script> console.log('not valid - add');</script>";
				$this->_init_add();
				$this->template->build('form', $this->data);
			}else{
echo "<script> console.log('not valid - edit');</script>";
				$this->_init_add();
				//$this->_init_edit($kduser);
				$this->template->build('form', $this->data);
			}
		}        
	}
	
	public function get_divisi() {
		echo $this->users_qry->get_divisi();
	}
	
	public function json_dgview() {
		echo $this->users_qry->json_dgview();
	}
	
	private function _init_add(){
		$this->data['val']['kddiv'] = set_value('kddiv');
		$this->data['form'] = array(
			'id'=> array(
					'placeholder' => 'ID',
					'type'        => 'hidden',
					'id'          => 'id',
					'name'        => 'id',
					'value'       => set_value('id'),
					'class'       => 'form-control',
			),
			'kduser'=> array(
					'placeholder' => 'Kode Pengguna',
					'id'          => 'kduser',
					'name'        => 'kduser',
					'value'       => set_value('kduser'),
					'class'       => 'form-control',
					'required'    => '',
					'autofocus'   => '',
					'style'		  => 'text-transform:uppercase',
			),
			'nmuser'=> array(
					'placeholder' => 'Nama Pengguna',
					'id'          => 'nmuser',
					'name'        => 'nmuser',
					'value'       => set_value('nmuser'),
					'class'       => 'form-control',
					'required'    => '',
					'style'		  => 'text-transform:uppercase',
			),
			'pwd'=> array(
					'placeholder' => 'Password',
					//'type'        => 'password',
					'id'          => 'pwd',
					'name'        => 'pwd',
					'value'       => set_value('pwd'),
					'class'       => 'pass-control',
					'required'    => '',
			),
			'kddiv'=> array(
					'placeholder' => 'Divisi',
					'attr'        => array(
						'id'    => 'kddiv',
						'class' => 'form-control',
						'style' => 'width:100%;'
					),
					'data'        => array(),
					//'data'        => $this->data['group_id'],
					'value'       => set_value('kddiv'),
					'name'        => 'kddiv',
					'required'    => '',
			),
			'group_id'=> array(
					'placeholder' => 'Grup Pengguna',
					'attr'        => array(
						'id'    => 'group_id',
						'class' => 'form-control',
						'style' => 'width:100%;'
					),
					'data' 		  => $this->data['group_id'],
					'value'       => set_value('group_id'),
					'name'        => 'kdgroups',
					//'required'    => '',
			),
			'dashboard'=> array(
					'placeholder' => 'URL Dashboard',
					'id'          => 'dashboard',
					'name'        => 'dashboard',
					'value'       => set_value('dashboard'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'su'=> array(
					'placeholder' => 'Sebagai Administrator',
					'id'      	  => 'su',
					'name'        => 'su',
					//'value'       => TRUE,
					//'checked'     => TRUE,
					'checked'     => set_value('su'),
					'class'       => '',
			),
			'faktif'=> array(
					'placeholder' => 'Status Aktif',
					//'type'        => 'hidden',
					'id'          => 'faktif',
					'name'        => 'faktif',
					//'value'       => TRUE,
					//'checked'     => TRUE,
					'checked'     => TRUE, //set_value('faktif'),
					'class'       => '',
			),
		);
	}
	
	private function _init_edit($kduser = null){
		//echo "<script> console.log('init edit');</script>";

		if(!$kduser){
			$kduser = $this->uri->segment(3);
		}
		$this->_check_id($kduser);

		$this->data['val'] = $this->val[0]; //agar variabel bisa diakses di javascript (Select2.js)

		$this->data['form'] = array(
			'id'=> array(
					'placeholder' => 'ID',
					'type'        => 'hidden',
					'id'          => 'id',
					'name'        => 'id',
					'value'       => $this->val[0]['kduser'],
					'class'       => 'form-control',
					'required'    => '',
			),
			'kduser'=> array(
					'placeholder' => 'Kode Pengguna',
					'id'          => 'kduser',
					'name'        => 'kduser',
					'value'       => $this->val[0]['kduser'],
					'class'       => 'form-control',
					'required'    => '',
					'autofocus'   => '',
					'style'		  => 'text-transform:uppercase',
			),
			'nmuser'=> array(
					'placeholder' => 'Nama Pengguna',
					'id'          => 'nmuser',
					'name'        => 'nmuser',
					'value'       => $this->val[0]['nmuser'],
					'class'       => 'form-control',
					'required'    => '',
					'style'		  => 'text-transform:uppercase',
			),
			'pwd'=> array(
					'placeholder' => 'Password',
					//'type'        => 'password',
					'id'          => 'pwd',
					'name'        => 'pwd',
					'value'       => $this->val[0]['pwd'],
					'class'       => 'pass-control',
					'required'    => '',
			),
			'kddiv'=> array(
					'placeholder' => 'Divisi',
					'attr'        => array(
						'id'    => 'kddiv',
						'class' => 'form-control',
						'style' => 'width:100%;'
					),
					'data'        => array(),
					//'data'        => $this->data['group_id'],
					'value'       => $this->val[0]['kddiv'],
					'name'        => 'kddiv',
					'required'    => '',
			),
			'group_id'=> array(
					'placeholder' => 'Grup Pengguna',
					'attr'        => array(
						'id'    => 'group_id',
						'class' => 'form-control',
						'style' => 'width:100%;'
					),
					'data'        => $this->data['group_id'],
					'value'       => $this->val[0]['kdgroups'],
					'name'        => 'kdgroups',
					//'required'    => '',
			),
			/*
			'dashboard'=> array(
					'placeholder' => 'URL Dashboard',
					'id'          => 'dashboard',
					'name'        => 'dashboard',
					'value'       => $this->val[0]['dashboard'],
					'class'       => 'form-control',
					'required'    => '',
			),
			*/
			'su'=> array(
					'placeholder' => 'Sebagai Administrator',
					'id'      	  => 'su',
					'name'        => 'su',
					//'value'       => TRUE,
					'checked'     => ($this->val[0]['su']=="t") ? TRUE : FALSE,
					'class'       => '',
			),
			'faktif'=> array(
					'placeholder' => 'Status Aktif',
					'id'          => 'faktif',
					'name'        => 'faktif',
					//'value'       => TRUE,
					'checked'     => ($this->val[0]['faktif']=="t") ? TRUE : FALSE,
					'class'       => '',
			),
		);
	}
	
	private function _check_id($kduser){
		//echo "<script> console.log('check id - user = ". $kduser ."');</script>";
		if(empty($kduser)){
			//redirect($this->data['add']);
		}

		$this->val = $this->users_qry->select_data($kduser);

		if(empty($this->val)){
			//redirect($this->data['add']);
		}
	}
	
	private function validate($kduser,$stat) {
		//echo "<script> console.log('validate');</script>";
		//echo "<script> console.log('validate kduser = ".$kduser."'');</script>";
		//echo "<script> console.log('validate stat = ".$stat."'');</script>";
		if(!empty($kduser) && !empty($stat)){
			return true;
		}
		$config = array(
			array(
					'field' => 'kduser',
					'label' => 'Kode Pengguna',
					'rules' => 'max_length[50]|min_length[2]|required|alpha_numeric',
					'errors' => array(
							'alpha_numeric' => 'Kolom ini hanya boleh berisi huruf dan angka saja!',
							),
				),
			array(
					'field' => 'nmuser',
					'label' => 'Nama Pengguna',
					'rules' => 'required',
				),
			array(
					'field' => 'pwd',
					'label' => 'Password',
					'rules' => 'required',
				),
			array(
					'field' => 'kddiv',
					'label' => 'Divisi',
					'rules' => 'required',
				),
/*
			array(
					'field' => 'kdgroups',
					'label' => 'Grup Pengguna',
					'rules' => 'required',
				),
			array(
					'field' => 'dashboard',
					'label' => 'URL Dashboard',
					'rules' => 'required',
				),
*/
		);
		

		if (!isset($_POST['su'])) {

//echo "<script> console.log('" . json_encode($_POST) . "');</script>";


			array_push($config, 
				array(
						'field' => 'kdgroups',
						'label' => 'Grup Pengguna',
						'rules' => 'required',
					)
			);
		}

//echo "<script> console.log('" . json_encode($config) . "');</script>";

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
		{
			return false;
		}else{
			return true;
		}
	}
}
