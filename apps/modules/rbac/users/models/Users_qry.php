<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Users_qry
 *
 * @author adi
 */
class Users_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}

	public function getGroupUser() {
		$this->db->select("kdgroups,nmgroups");
		$this->db->where("faktif","true");
		$this->db->order_by("nmgroups","ASC");
		$query = $this->db->get('f_groups');
		if($query->result_array()){
			$res = $query->result_array();
		}else{
			$res = false;
		}
		return $res;
	}

	public function select_data($id) {
		$this->db->where("upper(kduser)",  strtoupper($id));
		$query = $this->db->get("user");
		if($query->result_array()){
			$res = $query->result_array();
		}else{
			$res = false;
		}

		//echo "<script> console.log('qry: ". json_encode($res) ."');</script>";

		return $res;
	}

	public function get_divisi() {
		$q = $this->input->post('q');
		$kddiv = $this->input->post('kddiv');
		if($kddiv){
			$this->db->where('kddiv', $kddiv);
		}else{
			$this->db->like('LOWER(nmdiv)', strtolower($q));
		}
		$this->db->where('faktif', 'true');
		$this->db->where('status', 'UNT');
		$this->db->order_by("case
			when LOWER(nmdiv) like '" . strtolower($q) . "' then 1
			when LOWER(nmdiv) like '" . strtolower($q) . "%' then 2
			when LOWER(nmdiv) like '%" . strtolower($q) . "' then 3
			else 4 end");
		$this->db->order_by("nmdiv");
		$query = $this->db->get('mst.divisi');
		//echo $this->db->last_query();
		//echo "<script> alert('qry: ". json_encode($this->db->last_query()) ."');</script>";

		if ($query->num_rows() > 0) {
			$res = $query->result_array();
			foreach ($res as $value) {
				$d_arr[] = array(
					'id' => $value['kddiv'],
					'text' => $value['nmdiv'],
					);
			}
			$data = array(
				'total_count' => $query->num_rows(),
				'incomplete_results' => true,
				'items' => $d_arr
				);
			return json_encode($data);
		} else {
			$d_arr[] = array(
				'id' => '',
				'text' => '',
				);

			$data = array(
				'total_count' => 0,
				'incomplete_results' => true,
				'items' => $d_arr
				);
			return json_encode($data);
		}


/*
		$query = $this->db->get("mst.divisi");
		if($query->result_array()){
			$res = $query->result_array();
		}else{
			$res = false;
		}
		return $res;
*/
	}

	public function json_dgview() {
		error_reporting(-1);
		/*
		if( isset($_GET['rowid']) ){
			$rowid = $_GET['rowid'];
		}else{
			$rowid = '';
		}
		*/

		$aColumns = array('kduser','nmuser', 'kddiv', 'nmdiv', 'sux', 'nmgroups', 'dashboard', 'faktifx');
		$sIndexColumn = "kduser";

		$sTable = ' (SELECT kduser,
							nmuser,
							kddiv,
							nmdiv,
							faktifx,
							nmgroups,
							sux,
							dashboard
						FROM trx.v_user
					) AS a';

		$sLimit = "";
		if ( !empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1' )
		{
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
					intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
				$sOrder = "";
			}
		}

		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			$sWhere = " Where (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}

		/*
		 * SQL queries
		 */
		$sQuery = "
			SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM $sTable
				$sWhere
				$sOrder
				$sLimit
				";

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
			"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$row[] = $aRow[ $aColumns[$i] ];
			}
			$row[8] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('users/edit/'.$aRow['kduser'])."\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
			$row[8] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kduser']."');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
			$output['aaData'][] = $row;
		}
		echo json_encode($output);
	}


	public function submit() {
		try
		{
			$array = $this->input->post();

			//echo "<script> console.log('post 1: ". json_encode($array) ."');</script>";
			//echo "<script> alert('post 1: ". json_encode($array) ."');</script>";
			//$array['kddiv'] = $this->apps->kd_div;
			//$array['kdlokasi'] = '1';
			//$array['kdgrupmenu'] = '1';

			if(empty($array['stat'])) {
				// kode dan nama user UPPERCASE
				$array['kduser'] = strtoupper($array['kduser']);
				$array['nmuser'] = strtoupper($array['nmuser']);

				if( isset($_POST['su']) ){
					$array['su'] = TRUE;
					$array['kdgroups'] = NULL;
				} else {
					$array['su'] = FALSE;
				}

				if( isset($_POST['faktif']) ){
					$array['faktif'] = TRUE;
				} else {
					$array['faktif'] = FALSE;
				}
			}




			//insert
			if(empty($array['id'])){
				unset($array['id']);
				$resl = $this->db->insert('user',$array);
				if( ! $resl){
					$err = $this->db->error();
					$this->res = " Error : ". $this->apps->err_code($err['message']);
					$this->state = "0";
				}else{
					$this->res = "Data Berhasil Disimpan";
					$this->state = "1";
				}
			}

			//update
			elseif(!empty($array['id']) && empty($array['stat'])){
				//echo "<script> console.log('post qry: ". json_encode($array) ."');</script>";

				$this->db->where('kduser', $array['id']);
				unset($array['id']);
				$resl = $this->db->update('user', $array);
				if( ! $resl){
					$err = $this->db->error();
					$this->res = " Error : ". $this->apps->err_code($err['message']);
					$this->state = "0";
				}else{
					$this->res = "Data Berhasil Diupdate";
					$this->state = "1";
				}

			}

			//delete
			elseif(!empty($array['id']) && !empty($array['stat'])){
				$this->db->where('kduser', $array['id']);
				$resl = $this->db->delete('user');
				if( ! $resl){
					$err = $this->db->error();
					$this->res = " Error : ". $this->apps->err_code($err['message']);
					$this->state = "0";
				}else{
					$this->res = "Data Berhasil Dihapus";
					$this->state = "1";
				}
			}

			else{
				$this->res = "Some Variable Missing";
				$this->state = "0";
			}

		}catch (Exception $e) {
			$this->res = $e->getMessage();
			$this->state = "0";
		}

		$arr = array(
			'state' => $this->state,
			'msg' => $this->res,
			);
		$this->session->set_flashdata('statsubmit', json_encode($arr));
		return json_encode($arr);
	}
}
