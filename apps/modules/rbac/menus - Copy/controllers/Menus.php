<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Menus
 *
 * @author adi
 */
class Menus extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('menus/submit'),
            'add' => site_url('menus/add'),
            'edit' => site_url('menus/edit'),
            'reload' => site_url('menus'),
        );
        $this->load->model('menus_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function get_menu() {
        $q = $this->input->post('q');
        $dbrbac = $this->load->database('rbac', TRUE);
        
        $dbrbac->where("statmenu = '1' AND link <> '#' AND (LOWER(name) LIKE '%".strtolower($q)."%' OR LOWER(description) LIKE '%".strtolower($q)."%')");
        $dbrbac->order_by("CASE "
                . " WHEN LOWER(name) LIKE '".strtolower($q)."' THEN 1"
                . " WHEN LOWER(name) LIKE '".strtolower($q)."%' THEN 2"
                . " WHEN LOWER(name) LIKE '%".strtolower($q)."' THEN 3"
                . " WHEN LOWER(name) LIKE '%".strtolower($q)."%' THEN 4"
                . " WHEN LOWER(description) LIKE '".strtolower($q)."' THEN 5"
                . " WHEN LOWER(description) LIKE '".strtolower($q)."%' THEN 6"
                . " WHEN LOWER(description) LIKE '%".strtolower($q)."' THEN 7"
                . " WHEN LOWER(description) LIKE '%".strtolower($q)."%' THEN 8"
                . " END");
        $dbrbac->order_by('name','ASC');
        $query = $dbrbac->get('menus');
        //echo $dbrbac->last_query();
        $this->load->database('rbac', FALSE);
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['link'],
                    'text' => $value['name'],
                    'deskripsi' => $value['description'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            echo json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'deskripsi' => '',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            echo json_encode($data);
        }
    }
    
    public function add(){   
        if(!empty($this->rbac->module_access('add'))){
            redirect('debug/err_505');
        }
        $this->_init_add();
        $this->data['msg_detail'] = "Tambah Menu";
        $this->data['fa_icons'] = $this->load->view('fa_icons', '', TRUE);
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('add',$this->data);
    }
    
    public function edit() {
        if(!empty($this->rbac->module_access('edit'))){
            redirect('debug/err_505');
        }
        $this->_init_edit();
        $this->data['msg_detail'] = "Edit Menu";
        $this->data['fa_icons'] = $this->load->view('fa_icons', '', TRUE);
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('edit',$this->data);
    }
    
    public function json_dgview() {
        echo $this->menus_qry->json_dgview();
    }
    
    public function submit() {  

//echo "<script> console.log('" . json_encode() . "');</script>";
//			echo "<script> console.log('masuk'); </script>";


        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        $chktoplevel = $this->input->post('chktoplevel');

/*        
        if(isset($this->input->post('chktoplevel')){
        	$chktoplevel = $this->input->post('chktoplevel');
        } else{
        	$chktoplevel = '';
        }
*/

//echo "<script> console.log('masuk'); </script>";

    	/* 
    		keterangan :
    		$this->input->post('stat') selalu bernilai null (empty) utk tambah+ubah
    		variabel $stat khusus utk delete
    	*/

    	// jika tambah atau ubah
        if(empty($stat)){
	        if($this->validate($id,$stat,$chktoplevel) == TRUE){
	        	
	        	//echo "<script> console.log('valid');</script>";
				//echo "<script> console.log('PHPx: ". json_encode($this->input->post()) ."');</script>";

	            $res = $this->menus_qry->submit();

                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->build('add', $this->data);
                    }else{
                        $this->_init_edit($id);
                        $this->template->build('edit', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }

	        }else{


	        	//echo "<script> console.log('tdk valid');</script>";
				//echo "<script> console.log('PHP: ". json_encode($this->data) ."');</script>";
	        	echo "<script> console.log('tidak valid');</script>";
				echo "<script> console.log('PHPx: ". json_encode($this->input->post()) ."');</script>";

	            //$this->data['mainmenu'] = $this->menus_qry->select_main_menu();


	            if(empty($id)){
	                $this->_init_add();
	                $this->data['msg_detail'] = "Tambah Menu";
	                $this->template->build('add', $this->data);
	            }else{
	                $this->_init_edit($id);
	                $this->data['msg_detail'] = "Edit Menu";
	                $this->template->build('edit', $this->data);
	            }
	        }
        } 

    	// jika delete
        else {
			$res = $this->menus_qry->submit();
			echo $res;
        }

/*
        if($this->validate($id,$stat,$chktoplevel) == TRUE){
        	
        	//echo "<script> console.log('valid');</script>";
			//echo "<script> console.log('PHPx: ". json_encode($this->input->post()) ."');</script>";

            $res = $this->menus_qry->submit();

            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->build('add', $this->data);
                    }else{
                        $this->_init_edit($id);
                        $this->template->build('edit', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }

        }else{


        	//echo "<script> console.log('tdk valid');</script>";
			//echo "<script> console.log('PHP: ". json_encode($this->data) ."');</script>";
        	echo "<script> console.log('tidak valid');</script>";
			echo "<script> console.log('PHPx: ". json_encode($this->input->post()) ."');</script>";

            //$this->data['mainmenu'] = $this->menus_qry->select_main_menu();


            if(empty($id)){
                $this->_init_add();
                $this->data['msg_detail'] = "Tambah Menu";
                $this->template->build('add', $this->data);
            }else{
                $this->_init_edit($id);
                $this->data['msg_detail'] = "Edit Menu";
                $this->template->build('edit', $this->data);
            }


        }
*/        
    }
    
    private function _init_add(){
    	
        if(isset($_POST['chktoplevel']) && strtoupper($_POST['chktoplevel']) == 'OK'){
			$chk_top = TRUE;
        } else{
			$chk_top = FALSE;
        }

        if(isset($_POST['chkmainmenu']) && strtoupper($_POST['chkmainmenu']) == 'OK'){
			$chk_main = TRUE;
        } else{
			$chk_main = FALSE;
        }
        

        $mainmenuid = $this->menus_qry->select_main_menu();
        $opt_mainmenu[''] = '-- Set Induk Menu --';
        foreach ($mainmenuid as $value) {
            $opt_mainmenu[$value['kdmenu']] = $value['nmmenu'];
        }
        
        $opt_statmenu = array(
            '1' => 'Ya',
            '0' => 'Tidak',
        );
        $this->data['form'] = array(
           'name'=> array(
                    'placeholder' => 'Nama Menu',
                    'id'          => 'nama',
                    'name'        => 'nmmenu',
                    'value'       => set_value('nmmenu'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'autofocus'   => ''
            ),
           'chktoplevel'=> array(
                    'id'          => 'chktoplevel',
                    'value'       => 'ok',
                    'checked'     => $chk_top,
                    'class'       => 'filled-in',
                    'name'		  => 'chktoplevel',
            ),
           'chkmainmenu'=> array(
                    'id'          => 'chkmainmenu',
                    'value'       => 'ok',
                    'checked'     => $chk_main,
                    'class'       => 'filled-in',
                    'name'		  => 'chkmainmenu',
            ),
            'mainmenuid'=> array(
                    'attr'        => array(
                        'id'      => 'mainmenuid',
                        'class'   => 'form-control',
                    ),
                    'data'        => $opt_mainmenu,
                    'value'       => set_value('kdmenu_h'),
                    'name'        => 'kdmenu_h',
                    //'required'    => '',
            ),
           'link'=> array(
                    'placeholder' => 'Link',
                    'id'          => 'link',
                    'name'        => 'link',
                    'value'       => set_value('link'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'icon'=> array(
                    'placeholder' => 'Icon',
                    'id'          => 'icon',
                    'name'        => 'icon',
                    'value'       => set_value('icon'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
            'description'=> array(
                    'placeholder' => 'Deskripsi',
                    'id'          => 'description',
                    'name'        => 'description',
                    'value'       => set_value('description'),
                    'class'       => 'form-control',
                    //'style'       => 'resize: vertical;height: 60px;',
            ),
            'statmenu'=> array(
                    'attr'        => array(
                        'id'      => 'statmenu',
                        'class'   => 'form-control',
                    ),
                    'data'        => $opt_statmenu,
                    'value'       => set_value('statmenu'),
                    'name'        => 'statmenu',
            ),
        );
    }
    
    private function _init_edit($id = null){
        if($id){
            $menuid = $id;
        }else{
            $menuid = $this->uri->segment(3);
        }
        $this->_check_id($menuid);

        if($this->val[0]['kdmenu_h']){
            $checked = FALSE;
        }else{
            $checked = TRUE;
        }
        
        $mainmenuid = $this->menus_qry->select_main_menu($menuid);
        $opt_mainmenu[''] = '-- Set Induk Menu --';
        foreach ($mainmenuid as $value) {
            $opt_mainmenu[$value['kdmenu']] = $value['nmmenu'];
        }
        
        $opt_statmenu = array(
            '1' => 'Ya',
            '0' => 'Tidak',
        );
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Menu ID',
                    'id'          => 'id',
                    'name'        => 'id',
                    'value'       => $this->val[0]['kdmenu'],
                    'class'       => 'form-control',
            ),
           'name'=> array(
                    'placeholder' => 'Nama Menu',
                    'id'          => 'name',
                    'name'        => 'nmmenu',
                    'value'       => $this->val[0]['nmmenu'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'autofocus'   => ''
            ),
           'chkmainmenu'=> array(
                    'id'          => 'chkmainmenu',
                    'value'       => 'ok',
                    'checked'     => $checked,
                    'class'       => 'filled-in',
            ),
            'mainmenuid'=> array(
                    'attr'        => array(
                        'id'      => 'mainmenuid',
                        'class'   => 'form-control',
                    ),
                    'data'        => $opt_mainmenu,
                    'value'       => $this->val[0]['kdmenu_h'],
                    'name'        => 'kdmenu_h',
            ),
           'link'=> array(
                    'placeholder' => 'Link',
                    'id'          => 'link',
                    'name'        => 'link',
                    'value'       => $this->val[0]['link'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'icon'=> array(
                    'placeholder' => 'Icon',
                    'id'          => 'icon',
                    'name'        => 'icon',
                    'value'       => $this->val[0]['icon'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
            'description'=> array(
                    'placeholder' => 'Deskripsi',
                    'id'          => 'description',
                    'name'        => 'description',
                    'value'       => $this->val[0]['description'],
                    'class'       => 'form-control',
                    //'style'       => 'resize: vertical;height: 60px;',
            ),
            'statmenu'=> array(
                    'attr'        => array(
                        'id'      => 'statmenu',
                        'class'   => 'form-control',
                    ),
                    'data'        => $opt_statmenu,
                    'value'       => $this->val[0]['statmenu'],
                    'name'        => 'statmenu',
            ),
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        
        $this->val = $this->menus_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }


    private function _ck_nama_menu($nama){
    	//if(!empty($nama)){
    		$res = $this->menus_qry->cek_nama_menu;
    		return $res;
    	//}
    }

    private function validate($id,$stat,$chktoplevel) {
        if(!empty($id) && !empty($stat)){
            return true;
        }



        $config = array(
            array(
                    'field' => 'nmmenu',
                    'label' => 'Menu Name',
                    'rules' => 'required|max_length[50]',
                ),
            array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'max_length[100]',
                    ),
            array(
                    'field' => 'link',
                    'label' => 'Link',
                    'rules' => 'required|max_length[150]',
                    ),
            array(
                    'field' => 'icon',
                    'label' => 'Icon',
                    'rules' => 'required',
                    ),
            array(
                    'field' => 'statmenu',
                    'label' => 'Status',
                    'rules' => 'required|alpha_numeric_spaces|max_length[15]',
                    ),
        );

        if (strtoupper($chktoplevel) != 'OK') {
	        array_push($config, 
	            array(
	                    'field' => 'kdmenu_h',
	                    'label' => 'Induk Menu',
	                    'rules' => 'required',
	                    )
	        );
        }
        
echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
