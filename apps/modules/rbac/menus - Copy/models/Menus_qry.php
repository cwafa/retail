<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Menus_qry
 *
 * @author adi
 */
class Menus_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function select_main_menu($param = null) {    	
        $this->db->select('kdmenu,
                        nmmenu,
                        description,
                        link,
                        icon,
                        statmenu,
                        kdmenu_h');
        $this->db->where('kdmenu_h',NULL, FALSE);
        if($param){
            $this->db->where('kdmenu !=',$param);
        }
        $this->db->where('kdmenu_h',NULL, FALSE);
        $this->db->order_by('nmmenu','ASC');
        $query = $this->db->get('f_menu');
        return $query->result_array();

/*
        $dbrbac = $this->load->database('rbac', TRUE);
        $dbrbac->select('rowid AS id,
                        name,
                        description,
                        link,
                        icon,
                        statmenu,
                        mainmenuid');
        $dbrbac->where('mainmenuid',NULL, FALSE);
        if($param){
            $dbrbac->where('rowid !=',$param);
        }
        $query = $dbrbac->get('menus');
        $this->load->database('rbac', FALSE);
        return $query->result_array();
*/        
    }
    
    public function select_data($param) {
        $this->db->select('kdmenu,
                        nmmenu,
                        description,
                        link,
                        icon,
                        statmenu,
                        kdmenu_h');
        $this->db->where('kdmenu',$param);
        $query = $this->db->get('f_menu');
        return $query->result_array();

/*    	
        $dbrbac = $this->load->database('rbac', TRUE);
        $dbrbac->select('rowid AS id,
                        name,
                        description,
                        link,
                        icon,
                        statmenu,
                        mainmenuid');
        $dbrbac->where('rowid',$param);
        $query = $dbrbac->get('menus');
        $this->load->database('rbac', FALSE);
        return $query->result_array();
*/
    }
    
    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdmenu']) ){
            $id = $_GET['kdmenu'];
        }else{
            $id = '';
        }        
        
        $aColumns = array('nmmenu', 'nmindukmenu', 'description', 'kdmenu');
		$sIndexColumn = "kdmenu";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = "(SELECT kdmenu
        				, nmmenu
						, nmindukmenu
						, description
					FROM pzu.v_f_menu
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "OFFSET ".intval( $_GET['iDisplayStart'] )." LIMIT ".
                        intval( $_GET['iDisplayStart'] + $_GET['iDisplayLength'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($dbrbac->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($dbrbac->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
        //echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);
        //$rResult = $dbrbac->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        //$rResultFilterTotal = $dbrbac->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        //$rResultTotal = $dbrbac->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[3] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('menus/edit/'.$aRow['kdmenu'])."\">Edit</a>";
                $row[3] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdmenu']."');\">Delete</button>";
		$output['aaData'][] = $row;
	}
        //$this->load->database('rbac', FALSE);
	echo  json_encode( $output );  
    }
    





    public function submit() {

    	
//echo "<script> alert('x');</script>";

        try {
            $array = $this->input->post();

            //echo "<script> alert('PHP: ". json_encode($array) ."');</script>";
            //echo "<script> console.log('qry: ". json_encode($array) ."');</script>";


            //stat khusus utk delete, tambah+ubah => stat = empty
            if(empty($array['stat'])){
                $array['nmmenu'] = $array['nmmenu'];
                $array['description'] = $array['description'];
                $array['link'] = strtolower($array['link']);
                $array['icon'] = strtolower($array['icon']);

	            //if top level checked
				if(isset($array['chktoplevel']) && strtoupper($array['chktoplevel']) == 'OK'){
					unset($array['chktoplevel']);
					$array['kdmenu_h'] = null;
				}

				unset($array['chkmainmenu']);

            //echo "<script> console.log('PHP: ". json_encode($array) ."');</script>";
            }


            //echo "<script> console.log('PHP: ". json_encode($array) ."');</script>";
            if(empty($array['id'])){    
                unset($array['id']);


                //echo "<script> console.log('qry - add: ". json_encode($array) ."');</script>";

                
                $resl = $this->db->insert('f_menu',$array);
                
                //echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }


            elseif(!empty($array['id']) && empty($array['stat'])){
                if($array['kdmenu_h']=="" || empty($array['kdmenu_h'])){
                    unset($array['kdmenu_h']);
                    $this->db->set('kdmenu_h', 'NULL', FALSE);
                }
                $this->db->where('kdmenu', $array['id']);
                unset($array['id']);
                $resl = $this->db->update('f_menu', $array);

                echo "<script> console.log('PHP: ". json_encode($this->db->last_query()) ."');</script>";

                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }


            elseif(!empty($array['id']) && !empty($array['stat'])){                
                $this->db->where('kdmenu', $array['id']);
                $resl = $this->db->delete('f_menu');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            }
            else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

/*    	
        try {
            $dbrbac = $this->load->database('rbac', TRUE);
            $array = $this->input->post();
            if(empty($array['stat'])){
                $array['name'] = $array['name'];
                $array['description'] = $array['description'];
                $array['link'] = strtolower($array['link']);
                $array['icon'] = strtolower($array['icon']);
            }
            if(empty($array['id'])){    
                unset($array['id']);
                $resl = $dbrbac->insert('menus',$array);
                echo $dbrbac->last_query();
                if( ! $resl){
                    $err = $dbrbac->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }
            elseif(!empty($array['id']) && empty($array['stat'])){
                if($array['mainmenuid']=="" || empty($array['mainmenuid'])){
                    unset($array['mainmenuid']);
                    $dbrbac->set('mainmenuid', 'NULL', FALSE);
                }
                $dbrbac->where('rowid', $array['id']);
                unset($array['id']);
                $resl = $dbrbac->update('menus', $array);
                if( ! $resl){
                    $err = $dbrbac->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }
            elseif(!empty($array['id']) && !empty($array['stat'])){                
                $dbrbac->where('rowid', $array['id']);
                $resl = $dbrbac->delete('menus');
                if( ! $resl){
                    $err = $dbrbac->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            }
            else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        $this->load->database('rbac', FALSE);
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }
*/

	function cek_nama_menu($nama)
	{
	    $this->db->where('nmmenu',$nama);
	    $query = $this->db->get('f_menu');
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}    
}
