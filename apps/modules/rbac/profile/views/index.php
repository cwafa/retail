<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
//if($this->session->userdata('username')!=="ADMINISTRATOR"){

//not super user
//if ( $this->session->userdata('su') == 'f') 
{	
?> 

<style>
	.pass-control {
		display: block;
		width: 100%;
		height: 34px;
		padding: 6px 12px;
		font-size: 14px;
		line-height: 1.42857143;
		color: #555;
		background-color: #fff;
		background-image: none;
		border: 1px solid #ccc;
		border-radius: 0px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
		-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	}
</style>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url('assets/pages/css/profile-2.min.css');?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<div class="profile">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<a href="#tab_1_1" data-toggle="tab"> Overview </a>
			</li>
			<li>
				<a href="#tab_1_3" data-toggle="tab"> Account </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1_1">
				<div class="row">
					<div class="col-md-3">
						<ul class="list-unstyled profile-nav">
							<li>
								<img id="pic" src="<?=base_url('profile/show_image');?>" class="img-responsive pic-bordered" alt="" />
							</li>






							<div id="my_photo_booth">
								<div id="my_camera"></div>
								
								<!-- First, include the Webcam.js JavaScript Library -->
								<script type="text/javascript" src="<?=base_url('assets/dist/js/webcam.min.js');?>"></script>
								
								<!-- Configure a few settings and attach camera -->
								<script language="JavaScript">
									Webcam.set({
										// karena web pzu msh http, belum https
										force_flash: false,

										// live preview size
										width: 320,
										height: 240,
										
										// device capture size
										dest_width: 640,
										dest_height: 480,
										
										// final cropped size
										crop_width: 480,
										crop_height: 480,
										
										// format and quality
										image_format: 'jpeg',
										jpeg_quality: 90,
										
										// flip horizontal (mirror mode)
										flip_horiz: true
									});
									Webcam.attach( '#my_camera' );
								</script>
								
								<!-- A button for taking snaps -->
								<form>
									<div id="camera">
										<!-- This button is shown before the user takes a snapshot -->
										<input type=button value="Foto" onClick="use_camera()">
									</div>
									<div id="pre_take_buttons">
										<!-- This button is shown before the user takes a snapshot -->
										<input type=button value="Take Snapshot" onClick="preview_snapshot()">
										<input type=button value="Cancel" onClick="init()">
									</div>
									<div id="post_take_buttons" style="display:none">
										<!-- These buttons are shown after a snapshot is taken -->
										<input type=button value="&lt; Take Another" onClick="cancel_preview()">
										<input type=button value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
										<input type=button value="Batal" onClick="init()">
									</div>

								</form>
							</div>

							<div id="results" style="display:none">
								<!-- Your captured image will appear here... -->
							</div>


							<!-- Code to handle taking the snapshot and displaying it locally -->
							<script language="JavaScript">

								function init() {
									$('#camera').show();
									$('#pic').show();

									$('#pre_take_buttons').hide();
									$('#post_take_buttons').hide();
									$('#my_camera').hide();

									Webcam.unfreeze();
								}
								
								function use_camera() {
									$('#camera').hide();
									$('#pic').hide();

									$('#pre_take_buttons').show();
									$('#my_camera').show();
								}

								// preload shutter audio clip
								var shutter = new Audio();
								shutter.autoplay = false;
								//shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
								shutter.src = "<?=base_url('assets/shutter.mp3');?>";
								function preview_snapshot() {
									// play sound effect
									try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
									shutter.play();
									
									// freeze camera so user can preview current frame
									Webcam.freeze();
									
									// swap button sets
									document.getElementById('pre_take_buttons').style.display = 'none';
									document.getElementById('post_take_buttons').style.display = '';
								}
								
								function cancel_preview() {
									// cancel preview freeze and return to live camera view
									Webcam.unfreeze();
									
									// swap buttons back to first set
									document.getElementById('pre_take_buttons').style.display = '';
									document.getElementById('post_take_buttons').style.display = 'none';
								}
								
								function save_photo() {
									// actually snap photo (from preview freeze) and display it
									Webcam.snap( function(data_uri) {
										// display results in page
										document.getElementById('results').innerHTML = 
											'<h2>Here is your large, cropped image:</h2>' + 
											'<img src="'+data_uri+'"/><br/></br>' + 
											'<a href="'+data_uri+'" target="_blank">Open image in new window...</a>';
										
										// shut down camera, stop capturing
										Webcam.reset();
										
										// show results, hide photo booth
										document.getElementById('results').style.display = '';
										document.getElementById('my_photo_booth').style.display = 'none';
									} );
								}
							</script>

						</ul>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12 profile-info">
								<h1 class="font-green sbold uppercase"><?php echo $user[0]['nmuser'];?></h1>
								<p>
									<i class="fa fa-map-marker"></i> <?php echo $user[0]['kddiv']." - ".$user[0]['nmdiv'];?>
								</p>
							</div>
							<!--end col-md-8-->
						</div>
						<!--end row-->
					</div>
				</div>
			</div>
			<!--tab_1_2-->
			<div class="tab-pane" id="tab_1_3">
				<div class="row">
					<div class="col-md-12">
						<form action="#">
							<input type="hidden" name="id" id="id" value="<?php echo $user[0]['kduser'];?>" class="form-control" />
							<div class="form-group">
								<label class="control-label">Nama Pengguna <small>(Digunakan sebagai username login)</small></label>
								<input type="text" name="kduser" id="kduser" value="<?php echo $user[0]['kduser'];?>" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">Nama Lengkap</label>
								<input type="text" name="nmuser" id="nmuser" value="<?php echo $user[0]['nmuser'];?>" class="form-control" /> 
							</div>
							<div class="form-group">
								<label class="control-label">Password Baru <small>(Isi jika hanya ingin mengubah password)</small></label>
								<input type="password" name="password1" id="password1" class="pass-control" /> </div>
							<div class="form-group">
								<label class="control-label">Ulang Password Baru <small>(Isi jika hanya ingin mengubah password)</small></label>
								<input type="password" name="password2" id="password2"class="pass-control" /> 
								<small>Harus sama dengan password baru</small></div>
							<div class="margin-top-10">
								<button type="button" class="btn btn-primary btn-save"> Simpan Perubahan </button>
								<a href="<?=site_url('profile');?>" class="btn btn-default"> Batal </a>
							</div>
						</form>
					  </div>
				</div>
			</div>
			<!--end tab-pane-->
		</div>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function () {

		init();

		$(".btn-save").click(function(){
			submit();
		});  
	});

	function submit(){
		var id = $("#id").val();
		var kduser = $("#kduser").val();
		var nmuser = $("#nmuser").val();
		var password1 = $("#password1").val();
		var password2 = $("#password2").val();
		$.ajax({
			type: "POST",
			url: "<?=site_url('profile/submit');?>",
			data: {"id":id,"kduser":kduser,"nmuser":nmuser
					,"password1":password1,"password2":password2},
			beforeSend: function(result){
				$('.form-control').attr('disabled', true);
			},
			success: function(result){
				$('.form-control').attr('disabled', false);
				var obj = jQuery.parseJSON(result);
				if(obj.state==="1"){
					swal({
						title: "Success",
						text: obj.msg,
						type: "success"
					}, function(){
						location.reload();
					});
				}else{
					swal({
						title: "Error!",
						text: obj.msg,
						type: "error"
					}, function(){
						
					});
				}
			},
			error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				$('.form-control').attr('disabled', false);
			}
		});
	};
</script>
<?php
}