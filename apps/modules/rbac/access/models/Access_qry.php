<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Access_qry
 *
 * @author adi
 */
class Access_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get_cabang() {
        $query = $this->db->get('perusahaan');
        if ($query->num_rows()>0) {
            $result = $query->result();

            //$this->apps->set_cabang($result[0]->nmcabang);
            //$this->apps->logintag = $result[0]->nmcabang;
            // $res = $result[0]->nmusaha . ' - ' . $result[0]->kota;
            $res = $result[0]->kota;
            return ucwords(strtolower($res));

            //echo "<script> console.log('PHP: qry". $this->apps->logintag ."');</script>";
        }
    }



    public function submit() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

//echo "<script> alert('x');</script>";

        $this->db->join('f_groups','a.kdgroups = f_groups.kdgroups','left outer');
        $q_login = $this->db->get_where('user as a',array('kduser' => strtoupper($username),'pwd' => $password, 'a.faktif' => TRUE));



		//echo "<script> alert(". json_encode($this->db->last_query()) .");</script>";
		//echo "<script> console.log('PHP: ". json_encode($this->db->last_query()) ."');</script>";

        if ($q_login->num_rows()>0) {
            $d_login = $q_login->result();

			echo "<script> console.log('PHP: ". json_encode($d_login) ."');</script>";

            $newdata = array(
                'username'  => strtoupper($username),
                'nmuser'    => $d_login[0]->nmuser, //strtoupper($username), //ucwords(strtolower($username)),
                'kddiv'     => $d_login[0]->kddiv,
                //'nmlokasi'  => '',//$d_login[0]->nmlokasi,
                //'jenis'     => $d_login[0]->jenis,
                'groupname' => $d_login[0]->nmgroups,
                'dashboard' => $d_login[0]->dashboard,
                //'su'        => (bool) $d_login[0]->su,
                'su'        => $d_login[0]->su,
                'logged_in' => TRUE,
                'platform'  => $this->agent->platform(),
                'browser'   => $agent,
                'strsql'	=> json_encode($this->db->last_query()),
            );

            $this->session->set_userdata($newdata);



            $query = $this->db->get('perusahaan');
            if ($query->num_rows()>0) {
                $result = $query->result();

                $newdata2 = array(
                    'data'  => array(
                                'kddiv'     => $result[0]->kddiv,
                                'cabang'    => $result[0]->nmcabang,
                                'alamat'    => $result[0]->alamat,
                                'kota'      => $result[0]->kota,
                                'kacab'     => $result[0]->nmkacab,
                                'notelp'    => $result[0]->notelp,
                                'adh'       => $result[0]->nmadh,
                            ),
                );

                $this->session->set_userdata($newdata2);
            }



            //var_dump($this->session->all_userdata());
            //echo "<script> console.log('PHP: ". json_encode($this->session->get_userdata()) ."');</script>";

            $msg = '<div class="alert alert-success">'
                            . ' <strong>Login Berhasil!</strong> <br>Selamat Datang ' . $username
                            . ' </div>';
            // $this->ion_auth->messages()
            $this->session->set_flashdata('msg', $msg);
            return true;
        }

        else {
            $msg = '<div class="alert alert-danger">'
                            . ' <strong>Login Gagal!</strong> <br>Username/Password Anda salah atau Anda telah dinon-aktifkan'
                            . ' </div>';
            // $this->ion_auth->messages()
            $this->session->set_flashdata('msg', $msg);
            return false;
        }
    }


/*
    public function submit_bowo() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        if(strtoupper($username)=="ADMINISTRATOR" && $password=="575243385"){
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $this->db->limit(1);
            $q_login = $this->db->get('user');
            if($q_login->num_rows()>0){
                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $ug = array(
                    'groupname' => $d_ug[0]->name,
                );
                $this->session->set_userdata($ug);
                $this->load->database('rbac', FALSE);

                $d_login = $q_login->result();
                $newdata = array(
                        'username'  => strtoupper($username),
                        'nmuser'  => ucwords(strtolower($username)),
                        'kddiv'  => $d_login[0]->kddiv,
                        'nmlokasi'  => $d_login[0]->nmlokasi,
                        'jenis'  => $d_login[0]->jenis,
                        'logged_in' => TRUE,
                        'platform' => $this->agent->platform(),
                        'browser' => $agent,
                );
                $this->session->set_userdata($newdata);
                $msg = '<div class="alert alert-success">'
                                . ' <strong>Login Berhasil!</strong> <br>Selamat Datang ' . $username
                                . ' </div>';
                // $this->ion_auth->messages()
                $this->session->set_flashdata('msg', $msg);
                return true;
            }
            else{
                $msg = '<div class="alert alert-danger">'
                                . ' <strong>Login Gagal!</strong> <br>Username atau Password anda salah'
                                . ' </div>';
                // $this->ion_auth->messages()
                $this->session->set_flashdata('msg', $msg);
                return false;
            }
        }else{
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $q_login = $this->db->get_where('user',array('kduser' => strtoupper($username),'pwd' => $password));
            if($q_login->num_rows()>0){

                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $ug = array(
                    'groupname' => $d_ug[0]->name,
                );
                $this->session->set_userdata($ug);
                $this->load->database('rbac', FALSE);

                $d_login = $q_login->result();
                $newdata = array(
                        'username'  => strtoupper($username),
                        'nmuser'  => ucwords(strtolower($d_login[0]->nmuser)),
                        'kddiv'  => $d_login[0]->kddiv,
                        'nmlokasi'  => $d_login[0]->nmlokasi,
                        'jenis'  => $d_login[0]->jenis,
                        'logged_in' => TRUE,
                        'platform' => $this->agent->platform(),
                        'browser' => $agent,
                );
                $this->session->set_userdata($newdata);
                $msg = '<div class="alert alert-success">'
                                . ' <strong>Login Berhasil!</strong> <br>Selamat Datang ' . $d_login[0]->nmuser
                                . ' </div>';
                // $this->ion_auth->messages()
                $this->session->set_flashdata('msg', $msg);
                return true;
            }
            else{
                $msg = '<div class="alert alert-danger">'
                                . ' <strong>Login Gagal!</strong> <br>Username atau Password anda salah'
                                . ' </div>';
                // $this->ion_auth->messages()
                $this->session->set_flashdata('msg', $msg);
                return false;
            }
        }

    }
*/
}
