<div class="login-box animated fadeInDown">
  <div class="login-logo">
        <div>
            <img src="<?=base_url('assets/dist/img/png-toko-toko-png-512.png');?>" style="height: 100px;" />
        </div>
        <a href="<?=site_url();?>">
            <p class="site__title">
                <?php echo $this->apps->logintitle;?>
            </p>
            <p style="font-size: 18px;"><?php echo $this->apps->logintag;?></p>
        </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

          <p class="login-box-msg">
              <?php echo $this->apps->logindesc;?>
          </p>

          <?php
              echo $this->session->userdata('msg');
              echo validation_errors();
              if(isset($_GET['url'])){
                  $url = $_GET['url'];
              }else{
                  $url = null;
              }
              $array_login = array('msg', 'stat');
              $this->session->unset_userdata($array_login);

              $attributes = array(
                  'class' => 'login-form'
                  , 'id' => 'access_form'
                  , 'name' => 'access_form'
                  , 'method' => 'post');
              echo form_open(site_url('access/login/?url=' . $url),$attributes);
          ?>
            <div class="form-group has-feedback">
                  <input type="text" style="text-transform: uppercase;" id="username" name="username" class="form-control" placeholder="USERNAME" autofocus="" required="">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                  <input type="password" name="password" id="password" class="form-control" placeholder="PASSWORD" required="">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                   Masuk
                </button>
              </div>
              <div class="col-xs-12" style="padding-top: 10px;">
                <a href="#" class="btn btn-default btn-block btn-flat">
                    <i class="fa fa-home"></i> Halaman Depan
                </a>
              </div>
              <!-- /.col -->
            </div>
          <?php echo form_close(); ?> 
        </div>
      </div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
