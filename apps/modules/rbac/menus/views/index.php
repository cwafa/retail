<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Keterangan : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header">
				<a href="<?php echo $add;?>" <?php echo $this->rbac->module_access('add');?> class="btn btn-success" data-toggle="tooltip" data-placement="auto" title="Tambah Data">
					<span class="fa fa-plus"></span> Tambah
				</a>
				<a href="javascript:void(0);" class="btn btn-default btn-refresh" data-toggle="tooltip" data-placement="auto" title="Refresh Data">
					<span class="fa fa-refresh"></span> Refresh
				</a>

				<!--
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-wrench"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" <?php echo $this->rbac->module_access('add');?>>Tambah Data Baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refresh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
				-->

			</div>

			<!-- /.box-body -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 150px;text-align: center;">Nama Menu</th>
								<th style="width: 150px;text-align: center;">Induk Menu</th>
								<th style="text-align: center;">Deskripsi</th>
								<th style="width: 50px;text-align: center;">Link URL</th>
								<th style="width: 50px;text-align: center;">Icon</th>
								<th style="width: 50px;text-align: center;">Status Aktif</th>
								<th style="width: 75px;text-align: center;">
									<i class="fa fa-th-large"></i>
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Menu Utama</th>
								<th>Sub Menu</th>
								<th>Keterangan</th>
								<th>Keterangan</th>
								<th>Icon</th>
								<th>Keterangan</th>
								<th>Edit</th>
								<!-- <th>Delete</th> -->
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>


<div class="modal fade" id="modal_info" tabindex="-1" role="dialog" aria-hidden="true">
	<!-- <div class="modal-dialog modal-full"> -->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h3 id="judul" class="modal-title"></h3>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="tabelModal table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th style="width: 500px;text-align: left;">Nama Grup</th>
							</tr>
						</thead>
						<!--
						<tfoot>
							<tr>
								<th>Induk Menu</th>
							</tr>
						</tfoot>
					-->
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
				
				<!-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> -->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<script type="text/javascript">
	$(document).ready(function () {
		$(".btn-refresh").click(function(){
			table.ajax.reload();
		});

		table = $('.dataTable').DataTable({
			"bProcessing": true,
			"bServerSide": true,
			//"order": [[0,"asc"]],
			"columnDefs": [
				{ "orderable": false, "targets": 4 ,"sClass": "center"},
				{ "orderable": false, "targets": 6 }
				//{ "orderable": false, "targets": 4 }
			],
			//"pagingType": "simple",
			"sAjaxSource": "<?=site_url('menus/json_dgview');?>",
			//"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}}//,
				/*
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						   $(win.document.body).addClass('white-bg');
						   $(win.document.body).css('font-size', '10px');
						   $(win.document.body).find('table')
								   .addClass('compact')
								   .css('font-size', 'inherit');
				   }
				}
				*/
			]
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" && title!=='Icon'){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
						//if (ev.keyCode == 13) { //only on enter keypress (code 13)
							that
							.search( this.value )
							.draw();
						//}
					} );
		});




		// TABEL MODAL //

		tableModal = $('.tabelModal').DataTable({
			//"bProcessing": true,
			//"bServerSide": true,
			//"autoWidth": false,
			//"order": [[0,"asc"]],
			//"pagingType": "simple",
			//"aaData" 
            "bProcessing": false,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,

            "columns": [
                {"data": "nmgroups"}
            ],
			"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}}
			]
		});
/*
		$('.tabelModal').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.tabelModal tfoot th').each( function () {
			var title = $('.tabelModal tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" && title!=='Icon'){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		tableModal.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
						//if (ev.keyCode == 13) { //only on enter keypress (code 13)
							that
							.search( this.value )
							.draw();
						//}
					} );
		});
*/
	});






	function refresh(){
		table.ajax.reload();
	}


	function deleted(id){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('menus/isparent');?>",
			data: {"id":id,},
			success: function(resp){   
				var obj = jQuery.parseJSON(resp);
				if(obj=="1"){
					swal("Data Tidak Bisa Dihapus!", 'Menu ini memiliki submenu', "error");
				}else{
					swal({
						title: "Konfirmasi Hapus",
						text: "Data yang sudah dihapus tidak dapat dikembalikan!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#c9302c",
						confirmButtonText: "Ya, Lanjutkan!",
						cancelButtonText: "Tidak, Batalkan!",
						closeOnConfirm: false
					}, function () {
						var submit = "<?php echo $submit;?>"; 
						$.ajax({
							type: "POST",
							url: submit,
							data: {"id":id,"stat":"delete"},
							success: function(resp){   
								var obj = jQuery.parseJSON(resp);
								if(obj.state==="1"){
									table.ajax.reload();
									swal({
										title: "Deleted",
										text: obj.msg,
										type: "success"
									});

									//reload whole page, so menu in navbar section will update
									location.reload();

								}else{
									swal("Delete Error!", obj.msg, "error");
								}
							},
							error:function(event, textStatus, errorThrown) {
								swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
							}
						});
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}


	function info(id,name){
        $.ajax({
            url: "<?= site_url('menus/json_dgview_modal'); ?>",
            type: "POST",
            data: {"id": id,"name": name},
            beforeSend: function () {
                tableModal.clear().draw();
                $("#modal_info").modal("show");
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                //alert(obj);
                tableModal.rows.add(obj).draw();
                $('#judul').text('Grup menu "'+name+'"');
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
	}

</script>