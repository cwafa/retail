<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Menus_qry
 *
 * @author adi
 */
class Menus_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();
	}

	public function select_main_menu($param = null) {
		$this->db->select('kdmenu,
						nmmenu,
						description,
						link,
						icon,
						kdmenu_h,
						faktif');
		//$this->db->where('kdmenu <>',$param);
		//$this->db->where('faktif is not',null);

		if ($param = null){
			$query = $this->db->get('f_get_main_menu()');
		}else{
			$str = 'select * from trx.f_get_main_menu('.$param.')';
			$query = $this->db->query($str);
		}

		return $query->result_array();
	}

	public function select_data($param) {
		$this->db->select('kdmenu,
						nmmenu,
						description,
						link,
						icon,
						kdmenu_h,
						faktif');
		$this->db->where('kdmenu',$param);
		$query = $this->db->get('f_menu');
		return $query->result_array();
	}

	public function json_dgview() {
		error_reporting(-1);

		/*
		if( isset($_GET['kdmenu']) ){
			$id = $_GET['kdmenu'];
		}else{
			$id = '';
		}
		*/

		$aColumns = array('nmmenu', 'nmindukmenu', 'description', 'link', 'icon', 'faktifx', 'kdmenu');
		$sIndexColumn = "kdmenu";

		$sTable = "(SELECT kdmenu
						, nmmenu
						, nmindukmenu
						, description
						, link
						, '<i class='''||icon||'''></i>' as icon
						, faktifx
					FROM trx.v_f_menu
					) AS a";

		$sLimit = "";
		if ( !empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1' )
		{
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
					intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
				$sOrder = "";
			}
		}

		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			$sWhere = " Where (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}

		/*
		 * SQL queries
		 */
		$sQuery = "
			SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM $sTable
				$sWhere
				$sOrder
				$sLimit
				";

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
			"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$row[] = $aRow[ $aColumns[$i] ];
			}

			//$row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('menus/edit/'.$aRow['kdmenu'])."\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";

			$row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-warning btn-xs \" onclick=\"info('".$aRow['kdmenu']."','".$aRow['nmmenu']."');\" data-toggle=\"modal\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Info Grup\"><i class='fa fa-eye'></i></a>";

			//$row[6] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-warning btn-xs \" onclick=\"deleted('".$aRow['kdmenu']."');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Info Grup\"><i class='fa fa-eye'></i></button>";


			$row[6] .= "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('menus/edit/'.$aRow['kdmenu'])."\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";

			$row[6] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdmenu']."');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
			$output['aaData'][] = $row;
		}
		echo json_encode($output);
	}





	public function submit() {


		//echo "<script> alert('x');</script>";

		try {
			$array = $this->input->post();

			if(empty($array['stat'])){

				if( isset($_POST['faktif']) ){
					$array['faktif'] = TRUE;
				} else {
					$array['faktif'] = FALSE;
				}

				$array['link'] = strtolower($array['link']);
				$array['icon'] = strtolower($array['icon']);

				//if top level checked
				if(isset($array['chktoplevel']) && strtoupper($array['chktoplevel']) == 'OK'){
					unset($array['chktoplevel']);
					$array['kdmenu_h'] = null;
				}

				//if is main menu checked
				if(isset($array['chkmainmenu']) && strtoupper($array['chkmainmenu']) == 'OK'){
					unset($array['chkmainmenu']);
					$array['link'] = '#';
				}


				//proses insert
				if(empty($array['id'])){

					unset($array['id']);

					//echo "<script> console.log('qry - add: ". json_encode($array) ."');</script>";


					$resl = $this->db->insert('f_menu',$array);

					//echo $this->db->last_query();
					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Disimpan";
						$this->state = "1";
					}

				} else{
					//proses update
					if($array['kdmenu_h']=="" || empty($array['kdmenu_h'])){
						unset($array['kdmenu_h']);
						$this->db->set('kdmenu_h', 'NULL', FALSE);
					}
					$this->db->where('kdmenu', $array['id']);
					unset($array['id']);
					$resl = $this->db->update('f_menu', $array);

					//echo "<script> console.log('PHP: ". json_encode($this->db->last_query()) ."');</script>";

					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Diupdate";
						$this->state = "1";
					}
				}
			} else{

				if(!empty($array['id'])){
					//proses delete
					$this->db->where('kdmenu', $array['id']);
					$resl = $this->db->delete('f_menu');
					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Dihapus";
						$this->state = "1";
					}
				} else{
					$this->res = "Variabel tidak sesuai!";
					$this->state = "0";
				}
			}

		}catch (Exception $e) {
			$this->res = $e->getMessage();
			$this->state = "0";
		}

		$arr = array(
			'state' => $this->state,
			'msg' => $this->res,
			);

		$this->session->set_flashdata('statsubmit', json_encode($arr));
		return json_encode($arr);
	}



	// cek nama dobel (belum terpakai)
	function cek_nama_menu($nama)
	{
		$this->db->where('nmmenu',$nama);
		$query = $this->db->get('f_menu');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}





	public function json_dgview_modal() {
		//error_reporting(-1);

/*
        try {
        	*/
		//$array = $this->input->post();
		$id = $this->input->post('id');


		//echo "<script> console.log('qry - add: ". json_encode($array) ."');</script>";

/*
            $str = "select b.nmgroups
            			from trx.f_groups_menu a inner join
            				 trx.f_groups b on a.kdgroups = b.kdgroups
            		where kdmenu= " . $array['id'];
*/


//            		where kdmenu= " . $id;


            $str = "select b.nmgroups
            			from trx.f_groups_menu a inner join
            				 trx.f_groups b on a.kdgroups = b.kdgroups
            		where kdmenu= " . $id;

            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = $query->result_array();
	            return json_encode($res);
            }else{
                return null;
            }

/*
        } catch (Exception $e) {
            return 0;
        }
*/
	}


}
