<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Groups_qry
 *
 * @author adi
 */
class Groups_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();
	}

	/*
	public function select_main_group($param = null) {
		$this->db->where('kdgroups',NULL, FALSE);
		if($param){
			$this->db->where('kdgroups !=',$param);
		}
		$query = $this->db->get('f_groups');
		return $query->result_array();
	}
	*/

	public function select_data($param) {

		$this->db->where('kdgroups',$param);
		//$this->db->select("kdgroups, nmgroups, description");
		$query = $this->db->get('f_groups');

		return $query->result_array();
	}

	public function json_dgview() {
		error_reporting(-1);
		/*
		if( isset($_GET['kdgroups']) ){
			$rowid = $_GET['kdgroups'];
		}else{
			$rowid = '';
		}
		*/
		$aColumns = array('privilege','nmgroups', 'description', 'dashboard', 'faktifx', 'kdgroups' );

		$sIndexColumn = "kdgroups";

		$sTable = " (SELECT kdgroups as privilege
						, nmgroups
						, description
						, kdgroups
						, dashboard
						, case when faktif=true then 'AKTIF' else 'TIDAK' end as faktifx
						FROM trx.f_groups
					) AS a";

		$sLimit = "";
		if ( !empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1' )
		{
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
					intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
				$sOrder = "";
			}
		}

		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			$sWhere = " Where (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}

		/*
		 * SQL queries
		 */
		$sQuery = "
			SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM $sTable
				$sWhere
				$sOrder
				$sLimit
				";

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
			"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$row[] = $aRow[ $aColumns[$i] ];
			}
			$row[0] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-info btn-xs \" href=\"".site_url('groups/akses/'.$aRow['kdgroups'])."\">Set Privilege</a>";
			$row[5] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('groups/edit/'.$aRow['kdgroups'])."\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
			$row[5] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdgroups']."');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";

			$output['aaData'][] = $row;
	   }

	   echo json_encode( $output );
	}

	public function submit() {
		try {

			$array = $this->input->post();

			if(empty($array['stat'])){

				$array['nmgroups'] = strtoupper($array['nmgroups']);
				$array['description'] = strtoupper($array['description']);

				if( isset($_POST['faktif']) ){
					$array['faktif'] = TRUE;
				} else {
					$array['faktif'] = FALSE;
				}



				//proses insert
				if(empty($array['id'])){

					unset($array['id']);

					//echo "<script> alert('qry - add: ". json_encode($array) ."');</script>";


					$resl = $this->db->insert('f_groups',$array);

					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Disimpan";
						$this->state = "1";
					}

				} else{
					//proses update
					$this->db->where('kdgroups', $array['id']);
					unset($array['id']);
					$resl = $this->db->update('f_groups', $array);

					//echo "<script> console.log('PHP: ". json_encode($this->db->last_query()) ."');</script>";

					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Diupdate";
						$this->state = "1";
					}
				}
			} else{

				if(!empty($array['id'])){
					//proses delete
					$this->db->where('kdgroups', $array['id']);
					$resl = $this->db->delete('f_groups');
					if( ! $resl){
						$err = $this->db->error();
						$this->res = " Error : ". $this->apps->err_code($err['message']);
						$this->state = "0";
					}else{
						$this->res = "Data Berhasil Dihapus";
						$this->state = "1";
					}
				} else{
					$this->res = "Variabel tidak sesuai!";
					$this->state = "0";
				}
			}

		}catch (Exception $e) {
			$this->res = $e->getMessage();
			$this->state = "0";
		}

		$arr = array(
			'state' => $this->state,
			'msg' => $this->res,
			);

		$this->session->set_flashdata('statsubmit', json_encode($arr));
		return json_encode($arr);
	}




	/* AKSES (SET PRIVILEGE) */

	public function get_nmgroups($kode) {
		$this->db->where('kdgroups',$kode);
		$query = $this->db->get('f_groups');
		if($query->result_array()){
			$res = $query->result_array();
		}else{
			$res = false;
		}
		return $res;
	}

	public function json_dgview_akses() {
		error_reporting(-1);


		if( isset($_GET['uid']) ){
			if( $_GET['uid'] ){
				$uid = $_GET['uid'];
			} else {
				$uid = "";
			}
		}else{
			$uid = "";
		}

		//echo "<script> alert('PHP: ". $uid ."');</script>";
		$aColumns = array('kdgroups', 'nmmenu', 'nmmenu_h', 'description', 'link', 'kdmenu');
		$sIndexColumn = "kdmenu";

		$sTable = " (SELECT	a.kdmenu, a.nmmenu, b.nmmenu as nmmenu_h, a.description, a.link
							, (SELECT x.kdgroups FROM trx.f_groups_menu x
								WHERE x.kdmenu=a.kdmenu AND x.kdgroups = '". $uid ."') as kdgroups
						FROM trx.f_menu a left outer join
							 trx.f_menu b on a.kdmenu_h = b.kdmenu
						WHERE a.faktif = true and a.link <> '#'
					) AS x";
//								WHERE x.kdmenu=a.kdmenu AND x.kdgroups = '". $uid ."') as kdgroups
//								WHERE x.kdmenu=a.kdmenu AND x.kdgroups = 10) as kdgroups
		//echo "<script> alert('PHP: ". $sTable ."');</script>";

		$sLimit = "";
		if ( !empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1' )
		{
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
					intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
				$sOrder = "";
			}
		}

		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			$sWhere = " Where (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}

		/*
		 * SQL queries
		 */
		$sQuery = "
			SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM $sTable
				$sWhere
				$sOrder
				$sLimit
				";

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS jml
			FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
			"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$row[] = $aRow[ $aColumns[$i] ];
			}

			$row[0] = $this->set_access($aRow['kdgroups'],$aRow['kdmenu']);

			$output['aaData'][] = $row;
	   }

	   echo json_encode( $output );

	   //echo "<script> alert('PHP: ". json_encode($output) ."');</script>";
	}

    private function set_access($param,$mnsub_id){
        if( ! empty($param)){
            $check = "<button class=\"btn btn-success btn-sm\" type=\"button\" onclick=\"set_submenu('".$mnsub_id."')\" id=\"".$mnsub_id."\">On</button>";
        }else{
            $check = "<button class=\"btn btn-default btn-sm\" type=\"button\" onclick=\"set_submenu('".$mnsub_id."')\" id=\"".$mnsub_id."\">Off</button>";
        }
        return $check;
    }

    /*
    private function add_access($param,$mnsub_id){
        if( ! empty($param)){
            $check = "checked=\"checked\"";
        }else{
            $check = "";
        }
        return "<center><input type=\"checkbox\" onclick=\"set_access('".$mnsub_id."')\" ".$check." id=\"T".$mnsub_id."\" value=\"".$mnsub_id."\" /></center>";
    }

    private function edit_access($param,$mnsub_id){
        if( ! empty($param)){
            $check = "checked=\"checked\"";
        }else{
            $check = "";
        }
        return "<center><input type=\"checkbox\" onclick=\"set_access('".$mnsub_id."')\" ".$check." id=\"E".$mnsub_id."\" value=\"".$mnsub_id."\" /></center>";
    }

    private function delete_access($param,$mnsub_id){
        if( ! empty($param)){
            $check = "checked=\"checked\"";
        }else{
            $check = "";
        }
        return "<center><input type=\"checkbox\" onclick=\"set_access('".$mnsub_id."')\" ".$check." id=\"H".$mnsub_id."\" value=\"".$mnsub_id."\" /></center>";
    }
    */

	public function submit_akses() {
		try {
			$group_id = $this->input->post('group_id');
			$menu_id = $this->input->post('menu_id');
			$privilege = $this->input->post('privilege');


			$this->db->where("kdgroups",$group_id);
			$this->db->where("kdmenu",$menu_id);
			$qrycek = $this->db->get("f_groups_menu");

			if(!empty($qrycek->result_array())){
				$str = "DELETE FROM trx.f_groups_menu WHERE kdgroups = '".$group_id."' AND kdmenu = '".$menu_id."';";
			}else{
				$str = "INSERT INTO trx.f_groups_menu
								   (kdgroups
								   ,kdmenu
								   ,privilege)
						 VALUES
								   ('".$group_id."'
								   ,'".$menu_id."'
								   ,'".$privilege."');";
			}

			$resl = $this->db->query($str);
			if( ! $resl){
				$err = $this->db->error();
				//$this->res = "<i class=\"fa fa-fw fa-warning\"></i> Error : ". $this->apps->err_code($err['message']);
				$this->res = $this->apps->err_code($err['message']);
				$this->state = "0";
			}else{
				//$this->res = "<label class=\"label label-success\">Data Berhasil Diupdate</label>";
				$this->res = "Data Berhasil Diupdate";
				$this->state = "1";
			}

		}catch (Exception $e) {
			$this->res = $e->getMessage();
			$this->state = "0";
		}

		$arr = array(
			'state' => $this->state,
			'msg' => $this->res,
			);
		$this->session->set_flashdata('statsubmit', json_encode($arr));
		return json_encode($arr);
	}
}
