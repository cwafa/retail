<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header">
				<a href="<?php echo $add;?>" <?php echo $this->rbac->module_access('add');?> class="btn btn-success" data-toggle="tooltip" data-placement="auto" title="Tambah Data">
					<span class="fa fa-plus"></span> Tambah
				</a>
				<a href="javascript:void(0);" class="btn btn-default btn-refresh" data-toggle="tooltip" data-placement="auto" title="Refresh Data">
					<span class="fa fa-refresh"></span> Refresh
				</a>

				<!--
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-wrench"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" <?php echo $this->rbac->module_access('add');?>>Tambah data baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refersh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
				-->

			</div>

			<!-- /.box-body -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="width: 80px;text-align: center;">Set Privilege</th>
								<th style="width: 200px;text-align: center;">Nama Grup</th>
								<th style="text-align: center;">Deskripsi</th>
								<th style="width: 80px;text-align: center;">URL Dashboard</th>
								<th style="width: 50px;text-align: center;">Status Aktif</th>
								<th style="width: 50px;text-align: center;">
									<i class="fa fa-th-large"></i>
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Set Privilege</th>
								<th>Nama Group</th>
								<th>Description</th>
								<th>Description</th>
								<th>Description</th>
								<th>Edit</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		$(".btn-refresh").click(function(){
			table.ajax.reload();
		});

		table = $('.dataTable').DataTable({
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 1, 'asc' ]],
			"columnDefs": [
				{ "orderable": false, "targets": 0 },
				{ "orderable": false, "targets": 5 }
				//{ "orderable": false, "targets": 4 }
			],
			//"pagingType": "simple",
			"sAjaxSource": "<?=site_url('groups/json_dgview');?>",
			"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}}/*,
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}*/
			]
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" && title!=="Set Privilege"){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
								//if (ev.keyCode == 13) { //only on enter keypress (code 13)
									that
									.search( this.value )
									.draw();
								//}
							} );
		});

	});

	function refresh(){
		table.ajax.reload();
	}

	function deleted(id){
		swal({
			title: "Konfirmasi Hapus",
			text: "Data yang dihapus tidak dapat dikembalikan!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#c9302c",
			confirmButtonText: "Ya, Lanjutkan!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false
		}, function () {
			var submit = "<?php echo $submit;?>"; 
			$.ajax({
				type: "POST",
				url: submit,
				data: {"id":id,"stat":"delete"},
				success: function(resp){   
					var obj = jQuery.parseJSON(resp);
					if(obj.state==="1"){
						table.ajax.reload();
						swal({
							title: "Terhapus",
							text: obj.msg,
							type: "success"
						}, function(){
							//location.reload();
						});
					}else{
						swal("Terhapus", obj.msg, "error");
					}
				},
				error:function(event, textStatus, errorThrown) {
					swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			});
		});
	}
</script>