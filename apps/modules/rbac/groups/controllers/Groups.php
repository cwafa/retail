<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Groups
 *
 * @author adi
 */
class Groups extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			
			'submit' => site_url('groups/submit'),
			'add' => site_url('groups/add'),
			'edit' => site_url('groups/edit'),
			'reload' => site_url('groups'),
		);
		$this->load->model('groups_qry');
		
	}

	//redirect if needed, otherwise display the user list
	
	public function index(){
		
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('index',$this->data);
	}

	public function add(){
		if(!empty($this->rbac->module_access('add'))){
			redirect('debug/err_505');
		}
		$this->_init_add();
		$this->data['msg_detail'] = "Tambah Grup Pengguna";
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('form',$this->data);
	}
	
	public function edit() {
		if(!empty($this->rbac->module_access('edit'))){
			redirect('debug/err_505');
		}
		$this->_init_edit();
		$this->data['msg_detail'] = "Edit Grup Pengguna";
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('form',$this->data);
	}
	
	public function json_dgview() {
		echo $this->groups_qry->json_dgview();
	}
	
	public function submit() {
		$id = $this->input->post('id');
		$stat = $this->input->post('stat');
		
		if($this->validate($id,$stat) == TRUE){
			$res = $this->groups_qry->submit();
			if(empty($stat)){
				$data = json_decode($res);
				if($data->state==="0"){
					if(empty($id)){
						$this->_init_add();
						$this->template->build('form', $this->data);
					}else{
						$this->_check_id($id);
						$this->template->build('form', $this->data);
					}
				}else{
					redirect($this->data['reload']);
				}
			}else{
				echo $res;
			}
		}else{
			//$this->data['maingroup'] = $this->groups_qry->select_main_group();
			echo "<script> console.log('not valid - id = " . $id . "');</script>";

			if(empty($id)){
				echo "<script> console.log('not valid - add');</script>";
				$this->_init_add();
				$this->data['msg_detail'] = "Tambah Grup Pengguna";
				$this->template->build('form', $this->data);
			}else{
				echo "<script> console.log('not valid - edit');</script>";
				$this->_init_edit($id);
				$this->data['msg_detail'] = "Edit Grup Pengguna";
				$this->template->build('form', $this->data);
			}
		}
	}
	
	private function _init_add(){
		$this->data['form'] = array(
			'id'=> array(
					'type'        => 'hidden',
					'placeholder' => 'Group ID',
					'id'          => 'id',
					'name'        => 'id',
					'value'       => set_value('kdgroups'),
					'class'       => 'form-control',
			),
			'name'=> array(
					'placeholder' => 'Nama Grup',
					'id'          => 'name',
					'name'        => 'nmgroups',
					'value'       => set_value('nmgroups'),
					'class'       => 'form-control',
					'required'    => '',
					'autofocus'   => '',
					'style'       => 'text-transform:uppercase',
			),
			'description'=> array(
					'placeholder' => 'Deskripsi',
					'id'          => 'description',
					'name'        => 'description',
					'value'       => set_value('description'),
					'class'       => 'form-control',
					'style'       => 'resize: vertical; height: 80px; text-transform:uppercase;',
			),
			'dashboard'=> array(
					'placeholder' => 'URL Dashboard',
					'id'          => 'dashboard',
					'name'        => 'dashboard',
					'value'       => set_value('dashboard'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'faktif'=> array(
					'placeholder' => 'Status Aktif',
					//'type'        => 'hidden',
					'id'          => 'faktif',
					'name'        => 'faktif',
					//'value'       => TRUE,
					//'checked'     => TRUE,
					'checked'     => TRUE, //set_value('faktif'),
					'class'       => '',
			),
		);
	}
	
	private function _init_edit($pid = null){
		if(!$pid){
			$pid = $this->uri->segment(3);
		}
		
		$this->_check_id($pid);

		$this->data['form'] = array(
			'id'=> array(
					'type'        => 'hidden',
					'placeholder' => 'Group ID',
					'id'          => 'id',
					'name'        => 'id',
					'value'       => $this->val[0]['kdgroups'],
					'class'       => 'form-control',
			),
			'name'=> array(
					'placeholder' => 'Nama Grup',
					'id'          => 'nmgroups',
					'name'        => 'nmgroups',
					'value'       => $this->val[0]['nmgroups'],
					'class'       => 'form-control',
					'required'    => '',
					'autofocus'   => '',
					'style'       => 'text-transform:uppercase',
			),
			'description'=> array(
					'placeholder' => 'Deskripsi',
					'id'          => 'description',
					'name'        => 'description',
					'value'       => $this->val[0]['description'],
					'class'       => 'form-control',
					'style'       => 'resize: vertical; height: 80px; text-transform:uppercase;',
			),
			'dashboard'=> array(
					'placeholder' => 'URL Dashboard',
					'id'          => 'dashboard',
					'name'        => 'dashboard',
					'value'       => $this->val[0]['dashboard'],
					'class'       => 'form-control',
					'required'    => '',
			),
			'faktif'=> array(
					'placeholder' => 'Status Aktif',
					'id'          => 'faktif',
					'name'        => 'faktif',
					//'value'       => TRUE,
					'checked'     => ($this->val[0]['faktif']=="t") ? TRUE : FALSE,
					'class'       => '',
			),
		);
	}
	
	private function _check_id($id){
		echo "<script> console.log('check id - id = ". $id ."');</script>";
		echo "<script> console.log('check id');</script>";
		if(empty($id)){
			echo "<script> console.log('check id - id empty');</script>";
			redirect($this->data['add']);
		}
		
		$this->val= $this->groups_qry->select_data($id);
		
		if(empty($this->val)){
			echo "<script> console.log('check id - select data empty');</script>";
			redirect($this->data['add']);
		}
	}
	
	private function validate($id,$stat) {
		if(!empty($id) && !empty($stat)){
			return true;
		}
		$config = array(
			/*
			array(
					'field' => 'nmgroups',
					'label' => 'Nama Group',
					'rules' => 'required|alpha_numeric|max_length[50]',
					'errors' => array(
							'alpha_numeric' => 'Kolom ini hanya boleh berisi huruf dan angka saja!',
							),
			),
			*/
			array(
					'field' => 'nmgroups',
					'label' => 'Nama Group',
					'rules' => 'required|max_length[50]',
			),
			array(
					'field' => 'description',
					'label' => 'Deskripsi',
					'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($config);   
		if ($this->form_validation->run() == FALSE)
		{
			return false;
		}else{
			return true;
		}
	}



	// set privilege

	public function akses() {
		/*
		if( isset($_GET['kdgroups']) ){
			$this->data['kdgroups'] = $_GET['kdgroups'];
		} else {
        	$this->data['kdgroups'] = $this->uri->segment(3);
		}
		*/
		$this->data['kdgroups'] = $this->uri->segment(3);
		$res = $this->groups_qry->get_nmgroups($this->data['kdgroups']);
		if ($res){
			$this->data['nmgroups'] = 'GRUP - '.$res[0]['nmgroups'];
		}else{
			$this->data['nmgroups'] = $this->data['kdgroups'] ;
		}

        $this->template
            ->title('Set Privilege',$this->apps->name)
            ->set_layout('main')
            ->build('akses',$this->data);

	}
	
    public function json_dgview_akses() {
        //$this->load->model('groups_qry');
        echo $this->groups_qry->json_dgview_akses();
    }
    
    public function submit_akses() {       
        echo $this->groups_qry->submit_akses();
    }
}