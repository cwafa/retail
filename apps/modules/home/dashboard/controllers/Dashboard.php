<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashboard
 *
 * @author adi
 */
class Dashboard extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
        );
        $this->load->model('dashboard_qry');
        $this->data['cabang'] = array(
            "" => "-- PILIH CABANG --",
            "Semarang" => "Semarang"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){

        //echo "<script> console.log('PHP: ". $this->apps->logintag ."');</script>";
        //var_dump($this->apps->logintag);

        //$this->dashboard_qry->set_apps_var();

        $this->_init_add();


        $dashboard_view = $this->session->userdata('dashboard');
        if ($dashboard_view == '') {
            $dashboard_view = 'index';
        }

        $this->template
            ->title('Dashboard',$this->apps->name)
            ->set_layout('main')
            ->build($dashboard_view,$this->data);

        /*
        $groupname = $this->session->userdata('groupname');
        if($groupname==="SALES"
                || $groupname==="ADMIN"
                || $groupname==="ACCOUNTING" ){
            // Dashboard untuk SALES / ADMIN / ACCOUNTING
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('adh',$this->data);
        }elseif($groupname==="SPV"){
            // Dashboard untuk SPV
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('spv',$this->data);
        }elseif($groupname==="HRD"){
            // Dashboard untuk HRD
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('hrd',$this->data);
        }elseif($groupname==="SALES ONLINE"){
            // Dashboard untuk SALES ONLINE
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('sales_online',$this->data);
        }elseif($groupname==="ADH"){
            // Dashboard untuk ADH

            //$this->load->model('dashadh/dashadh_qry');

            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                //->build('dashadh/index',$this->data);
                ->build('adh',$this->data);
        }else{
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
        */
    } 

    // dashboard ADH 

    public function get_blm_trm_po_leasing_detail() {
        $data = $this->dashboard_qry->get_blm_trm_po_leasing_detail();
        $res = array();
        $no = 0;
        foreach ($data as $dt) {
            foreach ($dt as $k => $val) {
                if(is_numeric($val)){
                    $res[$no][$k] = (float) $val;
                }else{
                    $res[$no][$k] = $val;
                }
            }
            $no++;
        }
        echo json_encode($res);
    }



    //stok 

    public function json_dgview_stok() {
        echo $this->dashboard_qry->json_dgview_stok();
    } 




    private function _init_add(){
        $this->data['form'] = array( 
           'periode_stok'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_stok',
                    'name'        => 'periode_stok',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ), 
        );
    }
}
