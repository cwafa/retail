<style type="text/css">
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
        border: none;
    }
    .box{
        border-radius: 0px;
        padding-left: 7px;
        padding-right: 7px;
        border: 1px solid #d2d6de;
        margin-bottom: 0px;
    }
    .content-header{
        display: none;
    }

    .content {
        min-height: 250px;
        padding: 0px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
    .input-sm{
        height: 20px;
    }

    .table>tbody>tr>td
    , .table>tfoot>tr>td
    , .table>thead>tr>td
    {
        padding: 3px 8px;
    }
    .table{
        font-size: 12px;
    }
</style>
<!-- Main row --> 

<!-- ChartJS -->
<script src="<?=base_url('assets/plugins/chartjs/Chart.bundle.js');?>"></script>
<script src="<?=base_url('assets/plugins/chartjs/utils.js');?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script> 
</script>
