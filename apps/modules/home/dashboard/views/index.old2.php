<style type="text/css">
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
        border: none;
    }
    .box{
        border-radius: 0px;
        padding-left: 7px;
        padding-right: 7px;
        border: 1px solid #d2d6de;
        margin-bottom: 0px;
    }
    .content-header{
        display: none;
    }
    
    .content {
        min-height: 250px;
        padding: 0px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
    .input-sm{
        height: 20px;
    }
    
    .table>tbody>tr>td
    , .table>tfoot>tr>td
    , .table>thead>tr>td
    {
        padding: 3px 8px;
    }
    .table{
        font-size: 12px;
    }
</style>
<!-- Main row -->
<div class="row">
    <div class="box nopadding" style="border-top: 0px">
        <div class="box-body">
            <div class="col-lg-3">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i> <?=$form['periode_akhir']['placeholder'];?>
                    </span>
                    <?php 
                        echo form_input($form['periode_akhir']);
                        echo form_error('periode_akhir','<div class="note">','</div>'); 
                    ?>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-preview">
                            Proses
                        </button>
                    </span>
                </div>      
            </div>
            <div class="col-lg-10">
                
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 nopadding">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="BarJualPerbulan" style="height: 250px;"></canvas>
                    </div>        
                    <!-- Split -->
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="BarJualHarian" style="height: 250px;"></canvas>                        
                    </div>      
                    <!-- Split -->
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="PieKecamatanMTD" style="height: 300px;"></canvas>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 nopadding">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-4 nopadding" style="border-bottom: solid 1px #ddd;">
                        <div class="row">
                            <div class="col-lg-7 nopadding">
                                <div style="padding-left: 15px;">
                                    <canvas id="barJualTunaiKreditYTD" style="height:205px;"></canvas>  
                                </div>
                            </div>
                            <div class="col-lg-5 nopadding">
                                <div style="padding: 0px 0px;">
                                    <canvas id="barJualTunaiKreditMTD" style="height:205px;"></canvas>  
                                </div>
                            </div>
                            <div class="col-lg-6" style="padding-left: 20px;padding-bottom: 5px;">
                                <label class="label label-info label-tunai-ytd" style="background-color: #9966ff !important;">Tunai : 0 %</label>
                                <label class="label label-default label-kredit-ytd" style="background-color: #ffcd56 !important;">Kredit : 0 %</label>
                            </div>
                            <div class="col-lg-6" style="padding-left: 20px;padding-bottom: 5px;">
                                <label class="label label-info label-tunai-mtd" style="background-color: #9966ff !important;">0 %</label>
                                <label class="label label-default label-kredit-mtd" style="background-color: #ffcd56 !important;">0 %</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="barJualTunaiKreditPerBulan" style="height:250px;"></canvas>
                    </div> 
                    <!-- Split -->
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="PieJualLeasingYTD" style="height: 125px;"></canvas>
                    </div>
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="BarJualLeasing" style="height: 125px;"></canvas>
                    </div>
                    <!-- Split -->
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <div id="" style="height: 300px; overflow-y: auto;overflow-x: hidden;padding: 10px 10px;" align="center">
                            <table class="table table-hover table-bordered table-top-sales" style="width: 98%;font-size: 12px;margin-top: 0px !important;">
                                <caption style="text-align: center; font-weight: 700;color: #ff6363;font-size: 12px;padding-top: 0px;" class="cap-sales-mtd">
                                    <div>
                                        <label class="label label-primary">Sales Productivity</label>
                                        <label class="label label-success">Counter Productivity</label>
                                    </div>
                                    Penjualan Per Sales MTD
                                </caption>
                                <thead>
                                    <tr>
                                        <th  style="width:10px;">No.</th>
                                        <th>Nama Sales</th>
                                        <th  style="width:10px;">Status.</th>
                                        <th style="width: 25px;">Jml</th>
                                    </tr>
                                </thead>
                                <tfoot class="footSalesMTD"></tfoot>
                                <tbody class="bodySalesMTD"></tbody>
                                <tfoot class="footSalesMTD"></tfoot>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 nopadding">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="barJualAllTipe" style="height:250px;"></canvas>
                    </div>  
                    <div class="col-lg-9 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="barJualPerTipe" style="height:250px;"></canvas>
                    </div>  
                    <!-- Split -->
                    <div class="col-lg-6 nopadding" style="border-bottom: solid 1px #ddd;border-right: solid 1px #ddd;">
                        <canvas id="barJualSalesHeaderYTD" style="height:250px;"></canvas>                        
                    </div>
                    <div class="col-lg-6 nopadding" style="border-bottom: solid 1px #ddd;">
                        <canvas id="barJualSalesHeaderMTD" style="height:250px;"></canvas>
                    </div>
                    <!-- Split -->
                    <div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
                        <div id="" style="height: 300px; overflow-y: auto;overflow-x: hidden;padding: 10px 10px;" align="center">
                            <table class="table table-hover table-bordered table-stok-aktual" style="width: 98%;font-size: 12px;margin-top: 0px !important;">
                                <caption style="text-align: center; font-weight: 700;font-size: 12px;color: #ff6363;padding-top: 0px;" class="cap-stok-mtd">
                                    Stok Aktual MTD
                                </caption>
                                <thead>
                                    <tr>
                                        <th style="width:10px;">No.</th>
                                        <th>Tipe</th>
                                        <th style="width: 25px;">Jml</th>
                                    </tr>
                                </thead>
                                <tfoot class="footStokMTD"></tfoot>
                                <tbody class="bodyStokMTD"></tbody>
                                <tfoot class="footStokMTD"></tfoot>
                            </table>
                        </div>             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row (main row) -->

<!-- Modal Chart -->
<div id="chart-lv-1" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog" style="width: 99%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-lv-1">Title</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="hidden" id="nm" name="nm" />
            <input type="hidden" id="tahun" name="tahun" />
            <input type="hidden" id="func" name="func" />
            <div id="divLevel1a" class="col-lg-8" style="min-height:250px;">
                <div class="row">
                    <div class="col-lg-12">
                        <canvas id="chartLevel1" style="height:250px;"></canvas>
                    </div>  
                    <div class="col-lg-12 dataLevel1"></div>
                </div>
            </div>
            <div id="divLevel1b"  class="col-lg-4" style="min-height:250px;">
                <div class="table-responsive" style="width: 100%;">
                    <div class="tableLevel1"></div>
                </div>
            </div>
            <div class="col-lg-12"><hr></div>
            <div id="divLevel2a" class="col-lg-8">
                <canvas id="chartLevel2" style="height:250px;"></canvas>
            </div>
            <div id="divLevel2b" class="col-lg-4">
                <div class="tableLevel2" style="overflow-x: auto;"></div>
                <div class="tableLevel3" style="overflow-x: auto;"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal Detail Stok-->
<div id="detail-stok" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 99%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-detail-stok">Detail Stok</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-3">
                <table class="table table-hover table-bordered table-stok-aktual-perwarna">
                    <caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-stok-perwarna">
                        Stok Aktual Per Warna MTD 
                    </caption>
                    <thead>
                        <tr>
                            <th style="width:10px;">Kode</th>
                            <th >Warna</th>
                            <th style="width:10px;">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody class="bodyStokPerWarna"></tbody>
                </table>
            </div>
            <div class="col-lg-9" style="border-left: 1px solid #f4f4f4;">
                <div class="table-responsive" align="center">
                    <table class="table table-hover table-bordered table-stok-aktual-pertipe">
                        <caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-stok-pertipe">
                            Stok Aktual MTD 
                        </caption>
                        <thead>
                            <tr>
                                <th style="width:10px;">No.</th>
                                <th style="width:10px;">Kode</th>
                                <th style="width:100px;">Warna</th>
                                <th style="width:35px;">Tahun</th>
                                <th style="width:150px;">Tgl Terima</th>
                                <th style="width:10px;">Umur/Hari</th>
                                <th style="width:100px;">Nomor Mesin</th>
                                <th style="width:100px;">Nomor Rangka</th>
                                <th style="width:100px;">Lokasi</th>
                            </tr>
                        </thead>
                        <tbody class="bodyStokPerTipe"></tbody>
                    </table>
                </div> 
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal Detail Gross Profit-->
<div id="detail-gp" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 99%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-detail-gp">Detail Stok</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-hover table-bordered table-detail-gp">
                    <caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-detail-gp">
                        GROSS PROFIT DETAIL PERIODE
                    </caption>
                    <thead>
                        <tr>
                            <th style="width:45px;">Tgl Jual</th>
                            <th >Nama Konsumen</th>
                            <th style="width:10px;">T/K</th>
                            <th >Tipe</th>
                            <th >Leasing</th>
                            <th style="width: 45px;">Prog. Leasing</th>
                            <th style="width: 10px;">DP Gross</th>
                            <th style="width: 10px;">DP Riil</th>
                            <th style="width: 10px;">Angsuran</th>
                            <th style="width: 10px;">Tenor /Bulan</th>
                            <th style="width: 10px;">JP Leasing</th>
                            <th style="width: 10px;">Gross Profit</th>
                        </tr>
                    </thead>
                    <tbody class="body-detail-gp"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

<!-- ChartJS -->
<script src="<?=base_url('assets/plugins/chartjs/Chart.bundle.js');?>"></script>
<script src="<?=base_url('assets/plugins/chartjs/utils.js');?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script>
    $(function () {
        setInterval(function(){  
            getChart();
        }, 1800000);
        $('#periode_akhir').datepicker({
            startView: "year", 
            minViewMode: "months",
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm"      
        });
        getChart();
        $('.btn-preview').click(function(){
            getChart();
        });

        function getChart(){
            var periode = $('#periode_akhir').val();
            getJualPerBulan(periode);
            getJualTunaiKreditPerBulan(periode);
            getJualTipeMTD(periode);
            getJualAllTipePie(periode);
            getJualHarian(periode);
            getJualLeasing(periode);
            getJualSalesHeader(periode);
            getJualPerPos(periode);
            getJualPerSalesAktual(periode);
            getStokPerTipeAktual(periode);
        }

        $('#periode_akhir').on("changeDate", function(e) {
            getChart();
        });

        $('.overlay').hide();

        // Define a plugin to provide data labels
        Chart.plugins.register({
            beforeDraw: function (chart, easing) {
                if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                    var helpers = Chart.helpers;
                    var ctx = chart.chart.ctx;
                    var chartArea = chart.chartArea;

                    ctx.save();
                    ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                    ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                    ctx.restore();
                }
            },
            afterDatasetsDraw: function(chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        if(meta.type!=="line"){
                            meta.data.forEach(function(element, index) {
                                // Draw the text in black, with the specified font
                                ctx.fillStyle = 'rgb(0, 0, 0)';

                                var fontSize = 11;
                                var fontStyle = 'normal';
                                var fontFamily = 'Source Sans Pro';
                                ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                                // Just naively convert to string for now
                                var dataString = dataset.data[index].toString();

                                // Make sure alignment settings are correct
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';

                                var padding = 5;
                                var position = element.tooltipPosition();
                                ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                            });    
                        }
                    }
                });
            } 
        }); 
        
        $("#chart-lv-1").on('hidden.bs.modal', function(){
            $('#divLevel1a').show();
            $('#divLevel1a').removeAttr('class');
            $('#divLevel1a').attr('class','col-lg-8');

            $('#divLevel1b').show();
            $('#divLevel1b').removeAttr('class');
            $('#divLevel1b').attr('class','col-lg-4');    
            
            $('#divLevel2a').show();
            $('#divLevel2a').removeAttr('class');
            $('#divLevel2a').attr('class','col-lg-8');

            $('#divLevel2b').show();
            $('#divLevel2b').removeAttr('class');
            $('#divLevel2b').attr('class','col-lg-4');  
            
            $(".dataLevel1").html('');
            $(".tableLevel2").html('');
            $(".tableLevel3").html('');
            if(typeof mychartLevel2 != 'undefined' ){
                mychartLevel2.destroy();
            }    
        });

        $("#chart-lv-1").on('shown.bs.modal', function(){
            var nm = $("#nm").val();
            var tahun = $("#tahun").val();
            var func = $("#func").val();            
            if(func==="getJualSalesKoorMTD"){
                getJualSalesKoorMTD(nm,tahun);
            }else if(func==="getJualHarianPerTipe"){
                var tanggal = tahun + '-' + nm;
                getJualHarianPerTipe(tanggal);              
            }else if(func==="getJualPerKecamatan"){
                getJualPerKecamatan(nm,tahun);
            }else if(func==="getJualPerLeasingOthers"){
                getJualPerLeasingOthers(nm,tahun);
            }else if(func==="getJualPerLeasingOthersYTD"){
                getJualPerLeasingOthersYTD(nm,tahun);
            }else if(func==="getJualTipeYTD"){
                getJualTipeYTD(nm,tahun);
            }else if(func==="getJualPerItemMTD"){
                getJualPerItemMTD(nm,tahun);
            }
        });

        $("#detail-stok").on('shown.bs.modal', function(){
            var periode = $('#periode_akhir').val();
            var kode = $(".title-kode-stok").html();

            if ( $.fn.DataTable.isDataTable('.table-stok-aktual-pertipe') ) {
              $('.table-stok-aktual-pertipe').DataTable().destroy();
            }    

            if ( $.fn.DataTable.isDataTable('.table-stok-aktual-perwarna') ) {
              $('.table-stok-aktual-perwarna').DataTable().destroy();
            }   
            
            if ( $.fn.DataTable.isDataTable('.table-detail-jual') ) {
              $('.table-detail-jual').DataTable().destroy();
            }
            $.ajax({
                type: "POST",
                url: "<?=site_url('dashboard/getDetailStok');?>",
                data: {"periode":periode
                        ,"kode":kode},
                success: function(resp){  
                    var obj = jQuery.parseJSON(resp);

                    $(".cap-stok-perwarna").html("Stok Aktual Per Warna MTD  " + periode);
                    $(".bodyStokPerWarna").html('');
                    $.each(obj.jmlwarna, function(key, data){
                          $(".bodyStokPerWarna").append('<tr>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.kdwarna+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.nmwarna+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.jml+
                                                      '</td>' + 
                                                  '</tr>');
                    });  

                    var no = 1;
                    $(".cap-stok-pertipe").html("Stok Aktual Per Warna Dan Lokasi MTD " + periode);
                    $(".bodyStokPerTipe").html('');
                    $.each(obj.detail, function(key, data){
                          $(".bodyStokPerTipe").append('<tr>' + 
                                                      '<td style="text-align: center;">'+no+'</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.kdwarna+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.nmwarna+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.tahun+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.tglterima+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.umur+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.nosin+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.nora+
                                                      '</td>' + 
                                                      '<td style="text-align: left;">'+
                                                          data.nmlokasi+
                                                      '</td>' + 
                                                  '</tr>');
                          no++;
                    });     
                    $(".table-stok-aktual-pertipe").DataTable({
                          "lengthMenu": [[-1], ["Semua Data"]],
                          "bProcessing": false,
                          "bServerSide": false,
                          "bDestroy": true,
                          "bAutoWidth": false,
                          "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right' >r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'>> "
                      });   
                    $(".table-stok-aktual-perwarna").DataTable({
                          "lengthMenu": [[-1], ["Semua Data"]],
                          "bProcessing": false,
                          "bServerSide": false,
                          "bDestroy": true,
                          "bAutoWidth": false,
                          "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right' >r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'>> "
                      });
                },
                error:function(event, textStatus, errorThrown) {
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    });
    
    // Detail Stok Start
    function getDetailStok(kode,nama){
        $("#detail-stok").modal({
            "show":true,
            "backdrop":"static"
        });
        $(".title-detail-stok").html(nama+' <small class="title-kode-stok">'+kode+'</small>');
    }
    // Detail Stok End
    
    // Grafik untuk penjualan per bulan start  
    function getJualPerBulan(tahun){
        $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualPerBulan');?>",
            data: {"tahun":tahun.substring(0, 4)
                  ,"tanggal":tahun},
            success: function(resp){  
                var obj = jQuery.parseJSON(resp);
                var PDataDetailUnit = [];
                var PLabelDetailUnit = [];
                var totalAll = 0;
                var jmlbulan = 0;
                $.each(obj, function(key, data){
                    PDataDetailUnit.push(data.jml);
                    PLabelDetailUnit.push(bulan_short[data.bulan]);
                    totalAll = Number(totalAll) + Number(data.jml);
                    if(data.jml!=="0"){
                        jmlbulan++;
                    }
                });
                var avgAll = Math.round(totalAll/jmlbulan);
                var PDataAVGUnit = [];
                var PLabelAVGUnit = [];
                $.each(obj, function(key, data){
                    PDataAVGUnit.push(avgAll);
                    PLabelAVGUnit.push(bulan_short[data.bulan]);                    
                });
                var pieOptions     = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  //legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: false,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                elements: {
                    point: {
                        pointStyle: 'line'
                    }
                },
                  title: {
                      display: true,
                      text: 'Penjualan Semua Unit Pada Tahun ' + tahun.substring(0, 4)
                  },
                  chartArea: {
                      backgroundColor: 'rgba(255, 255, 255, 1)'
                  }
                };

                var config = {
                    type: 'bar',
                    data: {
                            datasets: [
                              {
                                  type: 'line',
                                  label: 'Rata-rata Per Bulan',
                                  backgroundColor: window.chartColors.d,
                                  data: PDataAVGUnit,
                                  borderColor: window.chartColors.d,
                                  fill: false,
                                  borderWidth: 2
                              }, {
                                  type: 'line',
                                  label: 'Penjualan',
                                  borderColor: window.chartColors.f,
                                  borderWidth: 2,
                                  fill: false,
                                  data: PDataDetailUnit
                              }, {
                                  type: 'bar',
                                  label: 'Penjualan',
                                  backgroundColor: window.chartColors.a,
                                  data: PDataDetailUnit,
                                  borderColor: 'white',
                                  borderWidth: 2
                              }
                            ],
                            labels:PLabelDetailUnit
                        },
                    options: pieOptions
                };
                var Sales = $('#BarJualPerbulan').get(0).getContext('2d');
                if(typeof myBarJualPerbulan != 'undefined' ){
                    myBarJualPerbulan.destroy();
                }
                myBarJualPerbulan = new Chart(Sales, config);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
    // Grafik untuk penjualan per bulan end

    // Grafik untuk penjualan Tunai Kredit per bulan start  
    function getJualTunaiKreditPerBulan(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualTunaiKreditPerBulan');?>",
          data: {"tahun":tahun.substring(0, 4)
                  ,"tanggal":tahun},
          success: function(resp){   
              getJualTunaiKreditYTD(resp);
              getJualTunaiKreditMTD(resp);
              var obj = jQuery.parseJSON(resp);
              var PDataDetailTunaiUnit = [];
              var PLabelDetailTunaiUnit = [];
              $.each(obj.tunai, function(key, data){
                  PDataDetailTunaiUnit.push(data.jml);
                  PLabelDetailTunaiUnit.push(bulan_short[data.bulan]);
              });
              var PDataDetailKreditUnit = [];
              var PLabelDetailKreditUnit = [];
              $.each(obj.kredit, function(key, data){
                  PDataDetailKreditUnit.push(data.jml);
                  PLabelDetailKreditUnit.push(bulan_short[data.bulan]);
              });
              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Tunai Kredit Tahun ' + tahun.substring(0, 4)
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                },
                  scales: {
                      xAxes: [{
                          ticks: {
                              fontSize: 8
                          }
                      }]
                  }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [{
                                data: PDataDetailTunaiUnit,
                                backgroundColor: window.chartColors.h,
                                borderColor: window.chartColors.h,
                                fill: false,
                                lineTension:0.5,
                                label: 'Tunai'
                          },{
                                data: PDataDetailKreditUnit,
                                backgroundColor: window.chartColors.d,
                                borderColor: window.chartColors.d,
                                fill: false,
                                lineTension:0.5,
                                label: 'Kredit'
                          }],
                          labels:PLabelDetailTunaiUnit
                      },
                  options: pieOptions
              };
              //console.log(config);
              var Sales = $('#barJualTunaiKreditPerBulan').get(0).getContext('2d');
              if(typeof mybarJualTunaiKreditPerBulan != 'undefined' ){
                  mybarJualTunaiKreditPerBulan.destroy();
              }
              mybarJualTunaiKreditPerBulan = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function getJualTunaiKreditYTD(resp){
    //barJualTunaiKreditYTD  
          var obj = jQuery.parseJSON(resp);
          var PDataDetailTunaiYTD = [];
          var PLabelDetailTunaiYTD = [];
          var sumTunaiYTD = 0;
          var sumKreditYTD = 0;
          var sumTunaiMTD = 0;
          var sumKreditMTD = 0;
          if(obj.tunaiytd.length>0){
              $.each(obj.tunaiytd, function(key, data){
                  PDataDetailTunaiYTD.push(data.jml);
                  PLabelDetailTunaiYTD.push("YTD");
                  sumTunaiYTD = sumTunaiYTD + data.jml;
              });
          }else{
              PDataDetailTunaiYTD.push(0);
              PLabelDetailTunaiYTD.push("YTD");
          }
          if(obj.tunaimtd.length>0){
              $.each(obj.tunaimtd, function(key, data){
  //                PDataDetailTunaiYTD.push(data.jml);
  //                PLabelDetailTunaiYTD.push("MTD");
                  sumTunaiMTD = sumTunaiMTD + data.jml;
              });    
          }else{
  //            PDataDetailTunaiYTD.push(0);
  //            PLabelDetailTunaiYTD.push("MTD");
          }
          var PDataDetailKreditYTD = [];
          var PLabelDetailKreditYTD = [];

          if(obj.kreditytd.length>0){
              $.each(obj.kreditytd, function(key, data){
                  PDataDetailKreditYTD.push(data.jml);
                  PLabelDetailKreditYTD.push("YTD");
                  sumKreditYTD = sumKreditYTD + data.jml;
              });    
          }else{
              PDataDetailKreditYTD.push(0);
              PLabelDetailKreditYTD.push("YTD");            
          }
          if(obj.kreditmtd.length>0){
              $.each(obj.kreditmtd, function(key, data){
  //                PDataDetailKreditYTD.push(data.jml);
  //                PDataDetailKreditYTD.push("MTD");
                  sumKreditMTD = sumKreditMTD + data.jml;
              });    
          }else{
  //            PDataDetailKreditYTD.push(0);
  //            PDataDetailKreditYTD.push("MTD");            
          }

          var sumAllYTD = Number(sumTunaiYTD) + Number(sumKreditYTD);
          var sumAllMTD = Number(sumTunaiMTD) + Number(sumKreditMTD);

          var avgTunaiYTD = ((Number(sumTunaiYTD)/Number(sumAllYTD)) * 100).toFixed(2);
          var avgKreditYTD = ((Number(sumKreditYTD)/Number(sumAllYTD)) * 100).toFixed(2);

          var avgTunaiMTD = ((Number(sumTunaiMTD)/Number(sumAllMTD)) * 100).toFixed(2);
          var avgKreditMTD = ((Number(sumKreditMTD)/Number(sumAllMTD)) * 100).toFixed(2);


          if(sumAllYTD){
              $(".label-tunai-ytd").html("T : " + avgTunaiYTD + "%");
              $(".label-kredit-ytd").html("K : " + avgKreditYTD + "%");    
          }else{
              $(".label-tunai-ytd").html("Tunai : 0%");
              $(".label-kredit-ytd").html("Kredit : 0%" );    
          }

          if(sumAllMTD){
              $(".label-tunai-mtd").html(avgTunaiMTD + "%");
              $(".label-kredit-mtd").html(avgKreditMTD + "%");    
          }else{
              $(".label-tunai-mtd").html("0%");
              $(".label-kredit-mtd").html("0%" );    
          }
          var pieOptions     = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
            legend: {
                display: false,
                position: 'bottom',
                fontSize: 9,
                boxWidth: 20
            },
            title: {
                display: true,
                text: 'YTD' 
            },
            chartArea: {
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            scales: {
              xAxes: [{
                  stacked: true,
              }],
              yAxes: [{
                  stacked: true
              }]
            }
          };

          var config = {
              type: 'bar',
              data: {
                      datasets: [{
                            data: PDataDetailTunaiYTD,
                            backgroundColor: window.chartColors.h,
                            borderColor: window.chartColors.h,
                            fill: false,
                            lineTension:0.5,
                            label: 'Tunai'
                      },{
                            data: PDataDetailKreditYTD,
                            backgroundColor: window.chartColors.d,
                            borderColor: window.chartColors.d,
                            fill: false,
                            lineTension:0.5,
                            label: 'Kredit'
                      }],
                      labels:PLabelDetailTunaiYTD
                  },
              options: pieOptions
          };
          var Sales = $('#barJualTunaiKreditYTD').get(0).getContext('2d');
          if(typeof mybarJualTunaiKreditYTD != 'undefined' ){
              mybarJualTunaiKreditYTD.destroy();
          }
          mybarJualTunaiKreditYTD = new Chart(Sales, config);
    }

    function getJualTunaiKreditMTD(resp){
      var obj = jQuery.parseJSON(resp);
          var PDataDetailTunaiYTD = [];
          var PLabelDetailTunaiYTD = [];
          var sumTunaiMTD = 0;
          var sumKreditMTD = 0;
          if(obj.tunaimtd.length>0){
              $.each(obj.tunaimtd, function(key, data){
                  PDataDetailTunaiYTD.push(data.jml);
                  PLabelDetailTunaiYTD.push("MTD");
                  sumTunaiMTD = sumTunaiMTD + data.jml;
              });    
          }else{
              PDataDetailTunaiYTD.push(0);
              PLabelDetailTunaiYTD.push("MTD");
          }
          var PDataDetailKreditYTD = [];
          var PLabelDetailKreditYTD = [];
          if(obj.kreditmtd.length>0){
              $.each(obj.kreditmtd, function(key, data){
                  PDataDetailKreditYTD.push(data.jml);
                  PDataDetailKreditYTD.push("MTD");
                  sumKreditMTD = sumKreditMTD + data.jml;
              });    
          }else{
              PDataDetailKreditYTD.push(0);
              PDataDetailKreditYTD.push("MTD");            
          }
          var pieOptions     = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
            legend: {
                display: false,
                position: 'bottom',
                fontSize: 9,
                boxWidth: 20
            },
            title: {
                display: true,
                text: 'MTD' 
            },
            chartArea: {
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            scales: {
              xAxes: [{
                  stacked: true,
              }],
              yAxes: [{
                  stacked: true
              }]
            }
          };

          var config = {
              type: 'bar',
              data: {
                      datasets: [{
                            data: PDataDetailTunaiYTD,
                            backgroundColor: window.chartColors.h,
                            borderColor: window.chartColors.h,
                            fill: false,
                            lineTension:0.5,
                            label: 'Tunai'
                      },{
                            data: PDataDetailKreditYTD,
                            backgroundColor: window.chartColors.d,
                            borderColor: window.chartColors.d,
                            fill: false,
                            lineTension:0.5,
                            label: 'Kredit'
                      }],
                      labels:PLabelDetailTunaiYTD
                  },
              options: pieOptions
          };
          var Sales = $('#barJualTunaiKreditMTD').get(0).getContext('2d');
          if(typeof mybarJualTunaiKreditMTD != 'undefined' ){
              mybarJualTunaiKreditMTD.destroy();
          }
          mybarJualTunaiKreditMTD = new Chart(Sales, config);
    }
    // Grafik untuk penjualan Tunai Kredit per bulan end  

    // Grafik untuk penjualan tipe start  
    function getJualTipeMTD(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualTipeMTD');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              //var PDataTarget = [];
              var PLabel = [];
              $.each(obj, function(key, data){
                  PData.push(data.jml);
                  //PDataTarget.push(data.cp_target_total);
                  PLabel.push(data.nmtipegrp);
              });
              var pieOptions     = {
                  hover: { 
                      onHover: (e, a) => { 
                          //$("#barJualAllTipe").css("cursor","pointer");
                         } 
                  },
                onClick : function(e,i){
                    //console.log(e);
                    e = i[0];
                    //console.log(this.data);
                    //console.log(e._index) // Index
                    //console.log(this.data.datasets[0].backgroundColor[e._index]);
                    var x_value = this.data.labels[e._index];
                    var bg_color = this.data.datasets[0].backgroundColor[e._index];
                    var y_value = this.data.datasets[0].data[e._index];
        //            console.log(x_value); // Label
        //            console.log(y_value); // Value
                    $("#chart-lv-1").modal({
                        "show":true,
                        "backdrop":"static"
                    });     
                    $(".title-lv-1").html(x_value);
                    $("#nm").val(x_value);
                    $("#tahun").val(tahun);
                    $("#func").val('getJualPerItemMTD');
                },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: false,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Per Tipe MTD ' + tahun
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                },
                  scales: {
                      xAxes: [{
                          ticks: {
                              fontSize: 8
                          }
                      }]
                  }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [{
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.b
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          },{
                                  type: 'line',
                                  data: PData,
                                  backgroundColor: window.chartColors.a,
                                  borderColor: window.chartColors.a,
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          }
                        /*
                          ,{
                                  type: 'line',
                                  data: PDataTarget,
                                  backgroundColor: window.chartColors.d,
                                  borderColor: window.chartColors.d,
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Target Per Bulan'
                          }
                        */
                      ],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#barJualPerTipe').get(0).getContext('2d');
              if(typeof mybarJualPerTipe != 'undefined' ){
                  mybarJualPerTipe.destroy();
              }
              mybarJualPerTipe = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualPerItemMTD(nm,tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualPerItemMTD');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun
                ,"nm":nm},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered table-detail-jual">' + 
                                      '<thead>' +
                                            '<tr>' + 
                                                '<th style="width: 60px;text-align: center;">NO. DO</th>' + 
                                                '<th style="width: 60px;text-align: center;">TANGGAL</th>' + 
                                                '<th style="text-align: center;">NAMA KONSUMEN</th>' + 
                                                '<th style="width: 5px;text-align: center;">T/K</th>' + 
                                                '<th style="width: 50px;text-align: center;">KODE</th>' + 
                                                '<th style="text-align: center;">TIPE UNIT</th>' + 
                                                '<th style="text-align: center;">SERIES</th>' + 
                                                '<th style="text-align: center;">WARNA</th>' + 
                                                '<th style="width: 70px;text-align: center;">NO. MESIN</th>' + 
                                                '<th style="width: 80px;text-align: center;">NO. RANGKA</th>' + 
                                            '</tr>' + 
                                        '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
                var no = 1;
                $.each(obj, function(key, data){
                    $(".bodyLevel1").append('<tr class="tr-no-'+no+'"></tr>');
                    $.each(data, function(key, detail){
                        //console.log(data);
                       $(".tr-no-"+no).append('<td>'+detail+'</td>'); 
                    });
                    no++;
                });

            $(".tableLevel1").append('</tbody></table>');
            
            
            $('#divLevel1a').hide();
            $('#divLevel1b').removeClass('col-lg-4');
            $('#divLevel1b').addClass('col-lg-12');
            $('#divLevel2a').hide();
            $('#divLevel2b').hide();
            $(".table-detail-jual").DataTable({
                "lengthMenu": [[-1], ["Semua Data"]],
                "bProcessing": false,
                "bServerSide": false,
                "bDestroy": true,
                "bAutoWidth": false,
                "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
            });            
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualAllTipePie(tahun){
        $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualTipe');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              var total = 0;
              $.each(obj.alltipepie, function(key, data){
                  total= total + Number(data.jml);
              });
              $.each(obj.alltipepie, function(key, data){
                  PData.push(((data.jml/total)*100).toFixed(2));
                  PLabel.push(data.nmtipegrp + " ["+numeral(data.jml).format('0,0')+"] ");
              });
              var pieOptions     = {
                  hover: { 
                      onHover: (e, a) => { 
                          
                         } 
                  },
                  onClick : function(e,i){
                      e = i[0];
                      var x_value = this.data.labels[e._index];
                      var bg_color = this.data.datasets[0].backgroundColor[e._index];
                      var y_value = this.data.datasets[0].data[e._index];
                      $("#chart-lv-1").modal({
                          "show":true,
                          "backdrop":"static"
                      });     
                      $(".title-lv-1").html('Penjualan Per Tipe YTD ' + tahun.substring(0, 4));
                      $("#nm").val(x_value);
                      $("#tahun").val(tahun);
                      $("#func").val('getJualTipeYTD');
                  },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'All Tipe YTD (%)'
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                }
              };

              var config = {
                  type: 'pie',
                  data: {
                          datasets: [{
                                  type: 'pie',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.d,
                                        window.chartColors.a,
                                        window.chartColors.f
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#barJualAllTipe').get(0).getContext('2d');
              if(typeof mybarJualAllTipe != 'undefined' ){
                  mybarJualAllTipe.destroy();
              }
              mybarJualAllTipe = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
         });

    }

    function getJualTipeYTD(nm,tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualTipe');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
  //            $.each(obj.pertipebar, function(key, data){
  //                PData.push(data.jml);
  //                PLabel.push(data.nmtipegrp);
  //            });

              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                      '<thead>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;width: 5%;">No.</th>' + 
                                          '<th style="text-align: center;">Tipe</th>' + 
                                          '<th style="text-align: center;width: 25%;">Persentase %</th>' + 
                                          '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                      '</tr>' + 
                                      '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
                var no = 1;
                var totalUnit = 0;
                $.each(obj.pertipebar, function(key, data){
                    totalUnit = Number(totalUnit) + Number(data.jml);
                });
                $.each(obj.pertipebar, function(key, data){
                    PData.push(data.jml);
                    PLabel.push(data.nmtipegrp);
                  $(".bodyLevel1").append('<tr>' + 
                                              '<td style="text-align: center;">'+no+'</td>' + 
                                              '<td style="text-align: left;">'+data.nmtipegrp+'</td>' + 
                                              '<td style="text-align: right;">'+((Number(data.jml)/Number(totalUnit)) * 100).toFixed(2)+' %</td>' + 
                                              '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                          '</tr>');
                  no++;
              });

              $(".tableLevel1").append('</tbody></table>');

              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: false,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Per Tipe YTD ' + tahun.substring(0, 4)
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                },
                  scales: {
                      xAxes: [{
                          ticks: {
                              fontSize: 8
                          }
                      }]
                  }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [{
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.b
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          },{
                                  type: 'line',
                                  data: PData,
                                  backgroundColor: window.chartColors.a,
                                  borderColor: window.chartColors.a,
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#chartLevel1').get(0).getContext('2d');
              if(typeof mychartLevel1 != 'undefined' ){
                  mychartLevel1.destroy();
              }
              mychartLevel1 = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }
    // Grafik untuk penjualan tipe end

    // Grafik untuk penjualan harian start
    function getJualHarian(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualHarian');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              var PDataTarget = [];
              var PLabelTarget = [];
              $.each(obj.getharian, function(key, data){
                  PData.push(data.jml);
                  PLabel.push(data.tgl);
              });
              $.each(obj.getharian, function(key, data){
                  $.each(obj.gettarget, function(key, datatarget){
                      PDataTarget.push(datatarget.jml);
                      PLabelTarget.push(data.tgl);
                  });
              });
              var pieOptions     = {
                  onClick : function(e,i){
                      e = i[0];
                      var x_value = this.data.labels[e._index];
                      var bg_color = this.data.datasets[0].backgroundColor[e._index];
                      var y_value = this.data.datasets[0].data[e._index];
          //            console.log(x_value); // Label
          //            console.log(y_value); // Value
                      $("#chart-lv-1").modal({
                          "show":true,
                          "backdrop":"static"
                      });     
                      $(".title-lv-1").html('Penjualan Per Tipe Tanggal : ' + tahun + '-' + x_value);
                      $("#nm").val(x_value);
                      $("#tahun").val(tahun);
                      $("#func").val('getJualHarianPerTipe');
                  },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: false,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Per Hari MTD ' + tahun
                },
              elements: {
                  point: {
                      pointStyle: 'line'
                  }
              },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                },
                  scales: {
                      xAxes: [{
                          ticks: {
                              fontSize: 8
                          }
                      }]
                  }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [
                          {
                                  type: 'line',
                                  data: PDataTarget,
                                  backgroundColor: window.chartColors.a,
                                  borderColor: window.chartColors.a,
                                  fill: false,
                                  lineTension:0,
                                  label: 'Target Penjualan'
                          },{
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: window.chartColors.f,
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Hari'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#BarJualHarian').get(0).getContext('2d');
              if(typeof myBarJualHarian != 'undefined' ){
                  myBarJualHarian.destroy();
              }
              myBarJualHarian = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualHarianPerTipe(tanggal){
          $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualHarianPerTipe');?>",
            data: {"tanggal":tanggal},
            beforeSend: function() {
                $('#divLevel1a').show();
                $('#divLevel1a').removeAttr('class');
                $('#divLevel1a').attr('class','col-lg-3');

                $('#divLevel1b').show();
                $('#divLevel1b').removeAttr('class');
                $('#divLevel1b').attr('class','col-lg-9');  
            },
            success: function(resp){   
              getJualCompareHarian(resp,tanggal);
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                      '<thead>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;width: 10px;" rowspan="2">No.</th>' + 
                                          '<th style="text-align: center;"  rowspan="2">Tipe</th>' + 
                                          '<th style="text-align: center;width: 50px;" rowspan="2">Target MTD</th>' + 
                                          '<th style="text-align: center;" colspan="2">Hari Ini</th>' + 
                                          '<th style="text-align: center;" colspan="3">MTD</th>' + 
                                          '<th style="text-align: center;" colspan="3">MTD - 1</th>' + 
                                      '</tr>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;">Gross Profit</th>' + 
                                          '<th style="text-align: center;width: 65px;">Unit</th>' + 
                                          '<th style="text-align: center;">Gross Profit</th>' + 
                                          '<th style="text-align: center;width: 65px;">Unit</th>' + 
                                          '<th style="text-align: center;width: 100px;">% GP</th>' + 
                                          '<th style="text-align: center;">Gross Profit</th>' + 
                                          '<th style="text-align: center;width: 65px;">Unit</th>' + 
                                          '<th style="text-align: center;width: 100px;">% GP</th>' + 
                                      '</tr>' + 
                                      '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
                var no = 1;
                var totgp = 0;
                var totjual = 0;
                var totgp_mtd = 0;
                var totjual_mtd = 0;
                var totgp_ymd = 0;
                var totjual_ymd = 0;
                var totcp_target_harian = 0;
                var cp_target_harian = 0;
                var tot_net_sales_mtd = 0;
                var tot_net_sales_ymd = 0;
                var tot_gp_mpct = 0;
                var tot_gp_ypct = 0;
                $.each(obj.gethariantipe, function(key, data){
                    PData.push(data.jml);
                    PLabel.push(data.nmtipegrp);
                    if(data.cp_target_harian!==null){
                        cp_target_harian = data.cp_target_harian;
                    }
                    $(".bodyLevel1").append('<tr>' + 
                                              '<td style="text-align: center;">'+no+'</td>' + 
                                              '<td style="text-align: left;"><a href="javascript:getGPHarianPerTipe(\''+data.nmtipegrp+'\',\''+tanggal+'\')">'+data.nmtipegrp+'</a></td>' + 
                                              '<td style="text-align: center;">'+cp_target_harian+'</td>' + 
                                              '<td style="text-align: right;">'+numeral(data.gross_profit).format('0,0')+'</td>' + 
                                              '<td style="text-align: center;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                              '<td style="text-align: right;">'+numeral(data.gp_mtd).format('0,0')+'</td>' + 
                                              '<td style="text-align: center;">'+numeral(data.jml_mtd).format('0,0')+'</td>' + 
                                              '<td style="text-align: center;">'+numeral(data.gp_mpct).format('0,0.00')+' %</td>' + 
                                              '<td style="text-align: right;">'+numeral(data.gp_ymd).format('0,0')+'</td>' + 
                                              '<td style="text-align: center;">'+numeral(data.jml_ymd).format('0,0')+'</td>' + 
                                              '<td style="text-align: center;">'+numeral(data.gp_ypct).format('0,0.00')+' %</td>' + 
                                          '</tr>');
                    no++;
                    totgp = Number(totgp) + Number(data.gross_profit);
                    totjual = Number(totjual) + Number(data.jml);
                    
                    totgp_mtd = Number(totgp_mtd) + Number(data.gp_mtd);
                    totjual_mtd = Number(totjual_mtd) + Number(data.jml_mtd);
                    
                    totgp_ymd = Number(totgp_ymd) + Number(data.gp_ymd);
                    totjual_ymd = Number(totjual_ymd) + Number(data.jml_ymd);
                    
                    totcp_target_harian = Number(totcp_target_harian) + Number(data.cp_target_harian);
                    
                    tot_net_sales_mtd = Number(tot_net_sales_mtd) + Number(data.net_sales_mtd);
                    tot_net_sales_ymd = Number(tot_net_sales_ymd) + Number(data.net_sales_ymd);
                    
                    //tot_gp_mpct = Number(tot_gp_mpct) + Number(data.gp_mpct);
                    //tot_gp_ypct = Number(tot_gp_ypct) + Number(data.gp_ypct);
                });
                    tot_gp_mpct = (Number(totgp_mtd) / Number(tot_net_sales_mtd)) * 100;
                    tot_gp_ypct = (Number(totgp_ymd) / Number(tot_net_sales_ymd)) * 100;
                    $(".bodyLevel1").append('<tr>' + 
                                              '<td style="text-align: center;" colspan="2"><b>Total</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(totcp_target_harian).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: right;"><b>'+numeral(totgp).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(totjual).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: right;"><b>'+numeral(totgp_mtd).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(totjual_mtd).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(tot_gp_mpct).format('0,0.00')+' %</b></td>' + 
                                              '<td style="text-align: right;"><b>'+numeral(totgp_ymd).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(totjual_ymd).format('0,0')+'</b></td>' + 
                                              '<td style="text-align: center;"><b>'+numeral(tot_gp_ypct).format('0,0.00')+' %</b></td>' + 
                                          '</tr>');
                $(".dataLevel1").html('');
                $.each(obj.getsaleshariantipe, function(key, data){
                  $(".dataLevel1").append(' <span style="margin: 5px;" class="pull-left badge bg-white">'+data.nmsales+' : '+numeral(data.jml).format('0,0')+'</span> ');
              });

              $(".tableLevel1").append('</tbody></table>');

                var pieOptions     = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: false,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'Penjualan Per Tipe Tanggal ' + tanggal
                  },
                  chartArea: {
                      backgroundColor: 'rgba(255, 255, 255, 1)'
                  },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 8
                            }
                        }]
                    }
                };

                var config = {
                    type: 'bar',
                    data: {
                            datasets: [{
                                    type: 'bar',
                                    data: PData,
                                    backgroundColor: [
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.d,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.a,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.f,
                                        window.chartColors.b
                                      ],
                                    fill: false,
                                    lineTension:0.5,
                                    label: 'Penjualan Per Tipe'
                            },{
                                    type: 'line',
                                    data: PData,
                                    backgroundColor: window.chartColors.a,
                                    borderColor: window.chartColors.a,
                                    fill: false,
                                    lineTension:0.5,
                                    label: 'Penjualan Per Tipe'
                            }],
                            labels:PLabel
                        },
                    options: pieOptions
                };
                var Sales = $('#chartLevel1').get(0).getContext('2d');
                if(typeof mychartLevel1 != 'undefined' ){
                    mychartLevel1.destroy();
                }
                mychartLevel1 = new Chart(Sales, config);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    
    function getGPHarianPerTipe(nmtipegrp,tanggal){
        if ( $.fn.DataTable.isDataTable('.table-detail-gp') ) {
          $('.table-detail-gp').DataTable().destroy();
          $(".body-detail-gp").html('');
        }        
        $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getGPHarianPerTipe');?>",
            data: {"nmtipegrp":nmtipegrp
                  ,"tanggal":tanggal},
            success: function(resp){   
                var obj = jQuery.parseJSON(resp);
                var no = 1;
                var tgldo = null;
                $.each(obj, function(key, data){
                    $(".body-detail-gp").append('<tr class="row-gp-'+no+'" style="font-size: 12px;"></tr>');
                    tgldo = data.tgldo;
                    if(tgldo!==null){
                        $.each(data, function(key, val){
                            $(".row-gp-"+no).append('<td>'+val+'</tr>');
                        }); 
                    }           
                    no++;
                });
                if(tgldo===null){
                    if ( $.fn.DataTable.isDataTable('.table-detail-gp') ) {
                      $('.table-detail-gp').DataTable().destroy();
                    } 
                    $(".body-detail-gp").html('');
                    alert("Data Tidak Ada!");
                    return;
                }else{           
                    var column = [];
                    for (i = 6; i <= 11; i++) { 
                        column.push({ 
                            "aTargets": [ i ],
                            "mRender": function (data, type, full) {
                                return type === 'export' ? data : numeral(data).format('0,0');
                                },
                            "sClass": "right"
                            });
                    }   

                    column.push({ 
                        "aTargets": [ 0 ],
                        "mRender": function (data, type, full) {
                            return type === 'export' ? data : moment(data).format('L');
                            } 
                        });

                    $("#detail-gp").modal({
                        "show":true,
                        "backdrop":"static"
                    });     
                    $(".title-detail-gp").html("GROSS PROFIT (TIPE "+nmtipegrp+") PERIODE : " + tanggal);
                    $(".cap-detail-gp").html("GROSS PROFIT (TIPE "+nmtipegrp+") PERIODE : " + tanggal);      
                    $(".table-detail-gp").DataTable({
                          "aoColumnDefs": column,
                          "lengthMenu": [[-1], ["Semua Data"]],
                          "bProcessing": false,
                          "bServerSide": false,
                          "bDestroy": true,
                          "bAutoWidth": false,
                            buttons: [
                                {extend: 'copy',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'csv',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'excel',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'pdf', 
                                    orientation: 'landscape',
                                    pageSize: 'A0'
                                },
                                {extend: 'print',
                                    customize: function (win){
                                           $(win.document.body).addClass('white-bg');
                                           $(win.document.body).css('font-size', '10px');
                                           $(win.document.body).find('table')
                                                   .addClass('compact')
                                                   .css('font-size', 'inherit');
                                   }
                                }
                            ],
                          "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                      }); 
                }                    
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }
    
    function getJualCompareHarian(resp,tanggal){
        var tgl1 = tanggal.substring(0, 7);
//        var m = tanggal.substring(5, 7);
//        var mm = 0;
//        if((Number(m) - 1)<10){
//           mm = '0'+(Number(m) - 1);
//        }else{
//           mm = (Number(m) - 1);
//        }
//        var tgl2 = tanggal.substring(0, 4)+'-'+mm;
        var obj = jQuery.parseJSON(resp);
        var PData = [];
        var PLabel = [];
        var PData2 = [];
        var PLabel2 = [];
        var tgl2 = obj.getjualharianbefore[0].yyyymm;
        $(".tableLevel2").html('');
        $(".tableLevel2").html('<table class="table table-hover table-bordered">' +
                                '<caption class="text-center"><b>Tabel Perbadingan Penjualan Akumulasi Periode ' + tgl2 + ' dan ' + tgl1  + '</b></caption>' +
                                '<thead>' + 
                                '<tr class="trTanggal">' + 
                                    '<th style="text-align: center;width: 100px;">TANGGAL</th>' + 
                                '</tr>' + 
                                '</thead>' + 
                                '<tbody class="bodyLevel2">'
                            );

        $(".bodyLevel2").append('<tr class="trLMTD" style="background-color: #4bc0c0;">' + 
                                    '<td style="text-align: center;">PERIODE<br>' + tgl2 + '</td>' + 
                                '</tr>'+
                                '<tr class="trMTD" style="background-color: #ff6363;">' + 
                                    '<td style="text-align: center;">PERIODE<br>' + tgl1 + '</td>' + 
                                '</tr>');
        /*
        + '<tr class="trGPMTD" style="background-color: #f4f4f4;">' + 
            '<td style="text-align: center;">GROSS PROFIT<br>' + tgl1 + '</td>' + 
        '</tr>'
         */
        $(".tableLevel2").append('</tbody></table>');
        var jmlMTD = 0;
        var jmlLMTD = 0;
        var jmlGPMTD = 0;
        var n = tanggal.substring(8, 10);
        var colspan = 0;
        $.each(obj.getjualharian, function(key, data){
            PData.push(data.jml);
            PLabel.push(data.tgl);
            jmlMTD = Number(jmlMTD) + Number(data.jml);
            jmlGPMTD = Number(data.gross_profit) + Number(jmlGPMTD);
            //console.log((data.tgl) + ' | ' + Number(n));
            if(Number(data.tgl)>Number(n)){
                $(".trMTD").append('<td style="text-align: center;">0</td>');
            }else if(Number(data.tgl)===Number(n)){
                $(".trMTD").append('<td style="text-align: center;background-color: #da5252;"><u><b>'+jmlMTD+'</b></u></td>');
            }else{
                $(".trMTD").append('<td style="text-align: center;">'+jmlMTD+'</td>');
            }
            //
            colspan++;
        });
        //$(".trGPMTD").append('<td style="text-align: center;font-size: 20px;" colspan="'+colspan+'">Rp. '+numeral(jmlGPMTD).format('0,0')+'</td>');
        $.each(obj.getjualharianbefore, function(key, data){
            PData2.push(data.jml);
            PLabel2.push(data.tgl);
            jmlLMTD = Number(jmlLMTD) + Number(data.jml);
            $(".trTanggal").append('<th style="text-align: center;width: 30px;">'+data.tgl+'</th>');
            $(".trLMTD").append('<td style="text-align: center;">'+jmlLMTD+'</td>');
        });

        var tb_avg = '<table class="table table-hover table-bordered">'+
                '<thead>'+
                    '<tr>'+
                        '<td rowspan="2" style="text-align: center;width: 200px;">Kategori</td>'+
                        '<td colspan="2" style="text-align: center;">Tunai</td>'+
                        '<td colspan="2" style="text-align: center;">Kredit</td>'+
                        '<td colspan="2" style="text-align: center;">Total</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td style="text-align: center;width: 150px;">MTD</td>'+
                        '<td style="text-align: center;width: 150px;">MTD - 1</td>'+
                        '<td style="text-align: center;width: 150px;">MTD</td>'+
                        '<td style="text-align: center;width: 150px;">MTD - 1</td>'+
                        '<td style="text-align: center;width: 150px;">MTD</td>'+
                        '<td style="text-align: center;width: 150px;">MTD - 1</td>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>';
            $.each(obj.getavgjual, function(num, data){
                if(num===9 || num ===10){
                    tb_avg += '<tr style="background-color:#E1F5FE;">';
                }else{
                    tb_avg += '<tr>';
                }
                $.each(data, function(key, val){
                    if(key==="key"){
                        tb_avg += '<td>'+val+'</td>';
                    }else{
                        if(val===0){
                            tb_avg += '<td style="text-align: right;background-color:#ddd;">'+numeral(val).format('0,0')+'</td>';
                        }else{
                            tb_avg += '<td style="text-align: right;">'+numeral(val).format('0,0')+'</td>';
                        }
                    }
                });
                tb_avg += '</tr>';
             });        
            tb_avg += '</tbody>' +
                '</table>';
            $(".tableLevel3").html(tb_avg);
        var pieOptions     = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke    : true,
          //String - The colour of each segment stroke
          segmentStrokeColor   : '#fff',
          //Number - The width of each segment stroke
          segmentStrokeWidth   : 1,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 0, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps       : 100,
          //String - Animation easing effect
          animationEasing      : 'easeOutBounce',
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate        : true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale         : false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive           : true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio  : false,
          //String - A legend template
          legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
          legend: {
              display: true,
              position: 'bottom',
              fontSize: 9,
              boxWidth: 20
          },
          title: {
              display: true,
              text: 'Penjualan Perbandingan Antara Periode ' + tgl2 + ' Dan ' + tanggal
          },
          chartArea: {
              backgroundColor: 'rgba(255, 255, 255, 1)'
          },
            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: 10
                    }
                }]
            }
        };

        var config = {
            type: 'bar',
            data: {
                    datasets: [{
                            type: 'bar',
                            data: PData2,
                            borderColor: window.chartColors.e,
                            backgroundColor: window.chartColors.e,
                            fill: false,
                            lineTension:0.5,
                            label: 'Penjualan Periode ' + tgl2 + ' '
                    },{
                            type: 'bar',
                            data: PData,
                            borderColor: window.chartColors.a,
                            backgroundColor: window.chartColors.a,
                            fill: false,
                            lineTension:0.5,
                            label: 'Penjualan Periode ' + tgl1 + ' '
                    }],
                    labels:PLabel
                },
            options: pieOptions
        };
        $('#divLevel2a').removeClass('col-lg-8');
        $('#divLevel2a').addClass('col-lg-12');
        $('#divLevel2b').removeClass('col-lg-4');
        $('#divLevel2b').addClass('col-lg-12');
        var Sales = $('#chartLevel2').get(0).getContext('2d');
        if(typeof mychartLevel2 != 'undefined' ){
            mychartLevel2.destroy();
        }
        mychartLevel2 = new Chart(Sales, config);
    }
    // Grafik untuk penjualan harian end

    // Grafik untuk penjualan per leasing start
    function getJualLeasing(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualLeasing');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              getJualLeasingYTD(resp,tahun);
              $.each(obj.jualleasingmtd, function(key, data){
                  PData.push(data.jml);
                  PLabel.push(data.kdleasing);
              });
              var pieOptions     = {
                  onClick : function(e,i){
                      e = i[0];
                      var x_value = this.data.labels[e._index];
                      var bg_color = this.data.datasets[0].backgroundColor[e._index];
                      var y_value = this.data.datasets[0].data[e._index];
                      if(x_value==="OTHERS"){
                          $("#chart-lv-1").modal({
                              "show":true,
                              "backdrop":"static"
                          });     
                          $(".title-lv-1").html(x_value);
                          $("#nm").val(x_value);
                          $("#tahun").val(tahun);
                          $("#func").val('getJualPerLeasingOthers');    
                      }
                  },
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: false,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'Penjualan Per Leasing MTD ' + tahun
                  },
                    elements: {
                        point: {
                            pointStyle: 'line'
                        }
                    },
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 8
                            }
                        }]
                    }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [
                          {
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.a,
                                        window.chartColors.b,
                                        window.chartColors.c,
                                        window.chartColors.d,
                                        window.chartColors.e,
                                        window.chartColors.f,
                                        window.chartColors.g,
                                        window.chartColors.h,
                                        window.chartColors.i,
                                        window.chartColors.j
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Leasing'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#BarJualLeasing').get(0).getContext('2d');
              if(typeof myBarJualLeasing != 'undefined' ){
                  myBarJualLeasing.destroy();
              }
              myBarJualLeasing = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualLeasingYTD(resp,tahun){
          var obj = jQuery.parseJSON(resp);
          var PData = [];
          var PLabel = [];
          $.each(obj.jualleasingytd, function(key, data){
              PData.push(data.jml);
              PLabel.push(data.kdleasing);
          });
          var pieOptions     = {
              onClick : function(e,i){
                  e = i[0];
                  var x_value = this.data.labels[e._index];
                  var bg_color = this.data.datasets[0].backgroundColor[e._index];
                  var y_value = this.data.datasets[0].data[e._index];
                  if(x_value==="OTHERS"){
                      $("#chart-lv-1").modal({
                          "show":true,
                          "backdrop":"static"
                      });     
                      $(".title-lv-1").html(x_value);
                      $("#nm").val(x_value);
                      $("#tahun").val(tahun);
                      $("#func").val('getJualPerLeasingOthersYTD');
                  }
              },
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
            legend: {
                display: false,
                position: 'bottom',
                fontSize: 9,
                boxWidth: 20
            },
            title: {
                display: true,
                text: 'Penjualan Per Leasing YTD '
            },
          elements: {
              point: {
                  pointStyle: 'line'
              }
          },
            chartArea: {
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
              scales: {
                  xAxes: [{
                      ticks: {
                          fontSize: 8
                      }
                  }]
              }
          };

          var config = {
              type: 'bar',
              data: {
                      datasets: [
                      {
                              type: 'bar',
                              data: PData,
                              backgroundColor: [
                                    window.chartColors.a,
                                    window.chartColors.b,
                                    window.chartColors.c,
                                    window.chartColors.d,
                                    window.chartColors.e,
                                    window.chartColors.f,
                                    window.chartColors.g,
                                    window.chartColors.h,
                                    window.chartColors.i,
                                    window.chartColors.j
                                ],
                              fill: false,
                              lineTension:0.5,
                              label: 'Penjualan Per Leasing YTD'
                      }],
                      labels:PLabel
                  },
              options: pieOptions
          };
          var Sales = $('#PieJualLeasingYTD').get(0).getContext('2d');
          if(typeof myPieJualLeasingYTD != 'undefined' ){
              myPieJualLeasingYTD.destroy();
          }
          myPieJualLeasingYTD = new Chart(Sales, config);
    }

    function getJualPerLeasingOthers(nm,tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualPerLeasingOthers');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun
                ,"nm":nm},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                      '<thead>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;width: 5%;">No.</th>' + 
                                          '<th style="text-align: center;">Nama Leasing</th>' + 
                                          '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                      '</tr>' + 
                                      '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
              var no = 1;
              $.each(obj, function(key, data){
                  PData.push(data.jml);
                  PLabel.push(data.kdleasing);
                $(".bodyLevel1").append('<tr>' + 
                                            '<td style="text-align: center;">'+no+'</td>' + 
                                            '<td style="text-align: left;">'+data.kdleasing+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                        '</tr>');
                no++;
            });

            $(".tableLevel1").append('</tbody></table>');
              var pieOptions     = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: false,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'Penjualan Per Leasing MTD ' + tahun
                  },
                    elements: {
                        point: {
                            pointStyle: 'line'
                        }
                    },
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 8
                            }
                        }]
                    }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [
                          {
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.a,
                                        window.chartColors.b,
                                        window.chartColors.c,
                                        window.chartColors.d,
                                        window.chartColors.e,
                                        window.chartColors.f,
                                        window.chartColors.g,
                                        window.chartColors.h,
                                        window.chartColors.i,
                                        window.chartColors.j
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Leasing'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
                var Sales = $('#chartLevel1').get(0).getContext('2d');
                if(typeof mychartLevel1 != 'undefined' ){
                    mychartLevel1.destroy();
                }
                mychartLevel1 = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualPerLeasingOthersYTD(nm,tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualPerLeasingOthersYTD');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun
                ,"nm":nm},
          success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                      '<thead>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;width: 5%;">No.</th>' + 
                                          '<th style="text-align: center;">Nama Leasing</th>' + 
                                          '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                      '</tr>' + 
                                      '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
              var no = 1;
              $.each(obj, function(key, data){
                  PData.push(data.jml);
                  PLabel.push(data.kdleasing);
                $(".bodyLevel1").append('<tr>' + 
                                            '<td style="text-align: center;">'+no+'</td>' + 
                                            '<td style="text-align: left;">'+data.kdleasing+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                        '</tr>');
                no++;
            });

            $(".tableLevel1").append('</tbody></table>');
              var pieOptions     = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: false,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'Penjualan Per Leasing YTD ' + tahun.substring(0, 4)
                  },
                    elements: {
                        point: {
                            pointStyle: 'line'
                        }
                    },
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 8
                            }
                        }]
                    }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [
                          {
                                  type: 'bar',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.a,
                                        window.chartColors.b,
                                        window.chartColors.c,
                                        window.chartColors.d,
                                        window.chartColors.e,
                                        window.chartColors.f,
                                        window.chartColors.g,
                                        window.chartColors.h,
                                        window.chartColors.i,
                                        window.chartColors.j
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Leasing'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
                var Sales = $('#chartLevel1').get(0).getContext('2d');
                if(typeof mychartLevel1 != 'undefined' ){
                    mychartLevel1.destroy();
                }
                mychartLevel1 = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }
    // Grafik untuk penjualan per leasing end

    // Grafik untuk penjualan sales header start  
    function getJualSalesHeader(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualSalesHeader');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun},
          success: function(resp){   
              getJualSalesHeaderMTD(resp,tahun);
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              var total = 0;
              $.each(obj.salesheaderytd, function(key, data){
                  total = total + Number(data.jml);
              });
              $.each(obj.salesheaderytd, function(key, data){
                  PData.push(((data.jml/total)*100).toFixed(2));
                  PLabel.push(data.kc + " ["+numeral(data.jml).format('0,0')+"]");
              });
              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Team Leader YTD ' + tahun.substring(0, 4)
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                }
              };

              var config = {
                  type: 'pie',
                  data: {
                          datasets: [{
                                  type: 'pie',
                                  data: PData,
                                  backgroundColor: [
                                        window.chartColors.a,
                                        window.chartColors.b,
                                        window.chartColors.c,
                                        window.chartColors.d,
                                        window.chartColors.e,
                                        window.chartColors.f,
                                        window.chartColors.g,
                                        window.chartColors.h,
                                        window.chartColors.i,
                                        window.chartColors.j
                                    ],
                                  fill: false,
                                  lineTension:0.5,
                                  label: 'Penjualan Per Tipe'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var Sales = $('#barJualSalesHeaderYTD').get(0).getContext('2d');
              if(typeof mybarJualSalesHeaderYTD != 'undefined' ){
                  mybarJualSalesHeaderYTD.destroy();
              }
              mybarJualSalesHeaderYTD = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function getJualSalesHeaderMTD(resp,tahun){
      var obj = jQuery.parseJSON(resp);
      var PData = [];
      var PLabel = [];
      var total = 0;
      $.each(obj.salesheadermtd, function(key, data){
          total = total + Number(data.jml);
      });
      $.each(obj.salesheadermtd, function(key, data){
          PData.push(((data.jml/total)*100).toFixed(2));
          PLabel.push(data.kc + " ["+numeral(data.jml).format('0,0')+"]");
      });
      var pieOptions     = {
          onClick : function(e,i){
              //console.log(e);
              e = i[0];
              //console.log(this.data);
              //console.log(e._index) // Index
              //console.log(this.data.datasets[0].backgroundColor[e._index]);
              var x_value = this.data.labels[e._index];
              var bg_color = this.data.datasets[0].backgroundColor[e._index];
              var y_value = this.data.datasets[0].data[e._index];
  //            console.log(x_value); // Label
  //            console.log(y_value); // Value
              $("#chart-lv-1").modal({
                  "show":true,
                  "backdrop":"static"
              });     
              $(".title-lv-1").html(x_value);
              $("#nm").val(x_value);
              $("#tahun").val(tahun);
              $("#func").val('getJualSalesKoorMTD');
          },
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke    : true,
          //String - The colour of each segment stroke
          segmentStrokeColor   : '#fff',
          //Number - The width of each segment stroke
          segmentStrokeWidth   : 1,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 0, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps       : 100,
          //String - Animation easing effect
          animationEasing      : 'easeOutBounce',
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate        : true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale         : false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive           : true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio  : false,
          //String - A legend template
          legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
          legend: {
              display: true,
              position: 'bottom',
              fontSize: 9,
              boxWidth: 20
          },
          title: {
              display: true,
              text: 'Team Leader MTD ' + tahun
          },
          chartArea: {
              backgroundColor: 'rgba(255, 255, 255, 1)'
          }
      };

      var config = {
          type: 'pie',
          data: {
                  datasets: [{
                          type: 'pie',
                          data: PData,
                          backgroundColor: [
                              window.chartColors.a,
                              window.chartColors.b,
                              window.chartColors.c,
                              window.chartColors.d,
                              window.chartColors.e,
                              window.chartColors.f,
                              window.chartColors.g,
                              window.chartColors.h,
                              window.chartColors.i,
                              window.chartColors.j
                            ],
                          fill: false,
                          lineTension:0.5,
                          label: 'Penjualan Per Tipe'
                  }],
                  labels:PLabel
              },
          options: pieOptions
      };
      var Sales = $('#barJualSalesHeaderMTD').get(0).getContext('2d');
      if(typeof mybarJualSalesHeaderMTD != 'undefined' ){
          mybarJualSalesHeaderMTD.destroy();
      }
      mybarJualSalesHeaderMTD = new Chart(Sales, config);
    }
    // Grafik untuk penjualan sales header end  

    // Grafik untuk penjualan sales koordinator start  
    function getJualSalesKoorMTD(nm,tahun){
      var nama = nm.substring(0, nm.indexOf("[") - 1);
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualSalesKoorMTD');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun
                ,"nama":nama},
          success: function(resp){  
                  var obj = jQuery.parseJSON(resp);
                  var PData = [];
                  var PLabel = [];
                  var total = 0;
                  $.each(obj.grafik, function(key, data){
                      total = total + Number(data.jml);
                  });

                  $(".tableLevel1").html('');
                  $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                          '<thead>' + 
                                          '<tr>' + 
                                              '<th style="text-align: center;width: 5%;">No.</th>' + 
                                              '<th style="text-align: center;">Nama Sales</th>' + 
                                              '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                          '</tr>' + 
                                          '</thead>' + 
                                          '<tbody class="bodyLevel1">'
                                      );
                  var no = 1;
                  $.each(obj.grafik, function(key, data){
                      $(".bodyLevel1").append('<tr>' + 
                                                  '<td style="text-align: center;">'+no+'</td>' + 
                                                  '<td style="text-align: left;">'+data.kc+'</td>' + 
                                                  '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                              '</tr>');
                      PData.push(((data.jml/total)*100).toFixed(2));
                      PLabel.push(data.kc + " ["+numeral(data.jml).format('0,0')+"]");
                      no++;
                  });

                  $(".tableLevel1").append('</tbody></table>');
                  var pieOptions     = {  
                      onClick : function(e,i){
                          //console.log(e);
                          e = i[0];
                          //console.log(this.data);
                          //console.log(e._index) // Index
                          //console.log(this.data.datasets[0].backgroundColor[e._index]);
                          var x_value = this.data.labels[e._index];
                          var bg_color = this.data.datasets[0].backgroundColor[e._index];
                          var y_value = this.data.datasets[0].data[e._index];
              //            console.log(x_value); // Label
              //            console.log(y_value); // Value
                          getJualSalesMTD(x_value,tahun);
                      },                
                      //Boolean - Whether we should show a stroke on each segment
                      segmentShowStroke    : true,
                      //String - The colour of each segment stroke
                      segmentStrokeColor   : '#fff',
                      //Number - The width of each segment stroke
                      segmentStrokeWidth   : 1,
                      //Number - The percentage of the chart that we cut out of the middle
                      percentageInnerCutout: 0, // This is 0 for Pie charts
                      //Number - Amount of animation steps
                      animationSteps       : 100,
                      //String - Animation easing effect
                      animationEasing      : 'easeOutBounce',
                      //Boolean - Whether we animate the rotation of the Doughnut
                      animateRotate        : true,
                      //Boolean - Whether we animate scaling the Doughnut from the centre
                      animateScale         : false,
                      //Boolean - whether to make the chart responsive to window resizing
                      responsive           : true,
                      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                      maintainAspectRatio  : false,
                      //String - A legend template
                      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                      legend: {
                          display: true,
                          position: 'right',
                          fontSize: 9,
                          boxWidth: 20
                      },
                      title: {
                          display: true,
                          text: 'SALES ' + nm + ' ' + tahun.substring(0, 4)
                      },
                      chartArea: {
                          backgroundColor: 'rgba(255, 255, 255, 1)'
                      }
                  };

                  var config = {
                      type: 'pie',
                      data: {
                              datasets: [{
                                      type: 'pie',
                                      data: PData,
                                      backgroundColor: [
                                            window.chartColors.a,
                                            window.chartColors.c,
                                            window.chartColors.d,
                                            window.chartColors.e,
                                            window.chartColors.f,
                                            window.chartColors.g,
                                            window.chartColors.h,
                                            window.chartColors.i,
                                            window.chartColors.j,
                                            window.chartColors.b
                                        ],
                                      fill: false,
                                      lineTension:0.5,
                                      label: 'Penjualan Per Tipe'
                              }],
                              labels:PLabel
                          },
                      options: pieOptions
                  };
                  var Sales = $('#chartLevel1').get(0).getContext('2d');
                  if(typeof mychartLevel1 != 'undefined' ){
                      mychartLevel1.destroy();
                  }
                  mychartLevel1 = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }
    // Grafik untuk penjualan sales koordinator end

    // Grafik untuk penjualan sales start  
    function getJualSalesMTD(nm,tahun){
      var nama = nm.substring(0, nm.indexOf("[") - 1);
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualSalesMTD');?>",
          data: {"tahun":tahun.substring(0, 4)
                ,"tanggal":tahun
                ,"nama":nama},
          success: function(resp){  
                  var obj = jQuery.parseJSON(resp);
                  var PData = [];
                  var PLabel = [];
                  var total = 0;
                  $.each(obj.grafik, function(key, data){
                      total = total + Number(data.jml);
                  });

                  $(".tableLevel2").html('');
                  $(".tableLevel2").html('<table class="table table-hover table-bordered">' + 
                                          '<thead>' + 
                                          '<tr>' + 
                                              '<th style="text-align: center;width: 5%;">No.</th>' + 
                                              '<th style="text-align: center;">Nama Sales</th>' + 
                                              '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                          '</tr>' + 
                                          '</thead>' + 
                                          '<tbody class="bodyLevel2">'
                                      );
                  var no = 1;
                  $.each(obj.grafik, function(key, data){
                      $(".bodyLevel2").append('<tr>' + 
                                                  '<td style="text-align: center;">'+no+'</td>' + 
                                                  '<td style="text-align: left;">'+data.kc+'</td>' + 
                                                  '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                              '</tr>');
                      PData.push(((data.jml/total)*100).toFixed(2));
                      PLabel.push(data.kc + " ["+numeral(data.jml).format('0,0')+"]");
                      no++;
                  });

                  $(".tableLevel2").append('</tbody></table>');
                  var pieOptions     = {              
                      //Boolean - Whether we should show a stroke on each segment
                      segmentShowStroke    : true,
                      //String - The colour of each segment stroke
                      segmentStrokeColor   : '#fff',
                      //Number - The width of each segment stroke
                      segmentStrokeWidth   : 1,
                      //Number - The percentage of the chart that we cut out of the middle
                      percentageInnerCutout: 0, // This is 0 for Pie charts
                      //Number - Amount of animation steps
                      animationSteps       : 100,
                      //String - Animation easing effect
                      animationEasing      : 'easeOutBounce',
                      //Boolean - Whether we animate the rotation of the Doughnut
                      animateRotate        : true,
                      //Boolean - Whether we animate scaling the Doughnut from the centre
                      animateScale         : false,
                      //Boolean - whether to make the chart responsive to window resizing
                      responsive           : true,
                      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                      maintainAspectRatio  : false,
                      //String - A legend template
                      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                      legend: {
                          display: true,
                          position: 'right',
                          fontSize: 9,
                          boxWidth: 20
                      },
                      title: {
                          display: true,
                          text: 'SALES ' + nm + ' ' + tahun.substring(0, 4)
                      },
                      chartArea: {
                          backgroundColor: 'rgba(255, 255, 255, 1)'
                      }
                  };

                  var config = {
                      type: 'pie',
                      data: {
                              datasets: [{
                                      type: 'pie',
                                      data: PData,
                                      backgroundColor: [
                                            window.chartColors.a,
                                            window.chartColors.c,
                                            window.chartColors.d,
                                            window.chartColors.e,
                                            window.chartColors.f,
                                            window.chartColors.g,
                                            window.chartColors.h,
                                            window.chartColors.i,
                                            window.chartColors.j,
                                            window.chartColors.b
                                        ],
                                      fill: false,
                                      lineTension:0.5,
                                      label: 'Penjualan Per Tipe'
                              }],
                              labels:PLabel
                          },
                      options: pieOptions
                  };
                  var Sales = $('#chartLevel2').get(0).getContext('2d');
                  if(typeof mychartLevel2 != 'undefined' ){
                      mychartLevel2.destroy();
                  }
                  mychartLevel2 = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }
    // Grafik untuk penjualan sales end

    // Grafik untuk penjualan pos kecamatan start  
    function getJualPerPos(tahun){
        $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualPerPos');?>",
            data: {"tanggal":tahun},
            success: function(resp){  
                var obj = jQuery.parseJSON(resp);
                var PData = [];
                var PLabel = [];
                var RandColor = [];
                $.each(obj, function(key, data){
                    PData.push(data.jml);
                    PLabel.push(data.kec);
                    RandColor.push(getRandomColorRGB());
                });
                var pieOptions     = {
                      onClick : function(e,i){
                          e = i[0];
                          var x_value = this.data.labels[e._index];
                          var bg_color = this.data.datasets[0].backgroundColor[e._index];
                          var y_value = this.data.datasets[0].data[e._index];
                          $("#chart-lv-1").modal({
                              "show":true,
                              "backdrop":"static"
                          });     
                          $(".title-lv-1").html(x_value);
                          $("#nm").val(x_value);
                          $("#tahun").val(tahun);
                          $("#func").val('getJualPerKecamatan');
                      },
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: true,
                      position: 'bottom',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'Penjualan Per Area MTD ' + tahun
                  },
                  chartArea: {
                      backgroundColor: 'rgba(255, 255, 255, 1)'
                  }
                };

                var config = {
                    type: 'pie',
                    data: {
                            datasets: [ {
                                  type: 'pie',
                                  label: 'Penjualan',
                                  data: PData,
                                  backgroundColor: [
                                            window.chartColors.a,
                                            window.chartColors.c,
                                            window.chartColors.d,
                                            window.chartColors.e,
                                            window.chartColors.f,
                                            window.chartColors.g,
                                            window.chartColors.h,
                                            window.chartColors.i,
                                            window.chartColors.j,
                                            window.chartColors.b
                                        ],
                                  borderColor: 'white',
                                  borderWidth: 2
                              }
                            ],
                            labels:PLabel
                        },
                    options: pieOptions
                };
                var Sales = $('#PieKecamatanMTD').get(0).getContext('2d');
                if(typeof myPieKecamatanMTD != 'undefined' ){
                    myPieKecamatanMTD.destroy();
                }
                myPieKecamatanMTD = new Chart(Sales, config);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }

    function getJualPerKecamatan(pos,tanggal){
          $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualPerKecamatan');?>",
            data: {"tanggal":tanggal
                   ,"pos":pos},
            success: function(resp){   
              var obj = jQuery.parseJSON(resp);
              var PData = [];
              var PLabel = [];
              var RandColor = [];
              $(".tableLevel1").html('');
              $(".tableLevel1").html('<table class="table table-hover table-bordered">' + 
                                      '<thead>' + 
                                      '<tr>' + 
                                          '<th style="text-align: center;width: 5%;">No.</th>' + 
                                          '<th style="text-align: center;">Tipe</th>' + 
                                          '<th style="text-align: center;width: 25%;">Total Penjualan</th>' + 
                                      '</tr>' + 
                                      '</thead>' + 
                                      '<tbody class="bodyLevel1">'
                                  );
              var no = 1;
                $.each(obj.kec, function(key, data){
                    PData.push(data.jml);
                    PLabel.push(data.kec);
                    RandColor.push(getRandomColorRGB());
                    $(".bodyLevel1").append('<tr>' + 
                                                '<td style="text-align: center;">'+no+'</td>' + 
                                                '<td style="text-align: left;" class="ring-area-'+data.kec+'">'+data.kec+'</td>' + 
                                                '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                            '</tr>');
                    $.each(obj.ringarea, function(key, val){
                        var ring = val.nama;
                        var jmlring = ring.substring(10, 100);
                        var area = ' [' + val.area + '] ';
                        if(area.indexOf(data.kec)>0){
                            for (i = 1; i <= Number(jmlring); i++) { 
                                $(".ring-area-"+data.kec).append(' <a href="javascript:void(0);" style="color: #000;font-size: 12px;" title="'+val.nama+'"><sup><i class="fa fa-star"></i></sup></a>');
                            }
                        }
                    });
                  no++;
              });

              $(".tableLevel1").append('</tbody></table>');

                var pieOptions     = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke    : true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor   : '#fff',
                  //Number - The width of each segment stroke
                  segmentStrokeWidth   : 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 0, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps       : 100,
                  //String - Animation easing effect
                  animationEasing      : 'easeOutBounce',
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate        : true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale         : false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive           : true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio  : false,
                  //String - A legend template
                  legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                  legend: {
                      display: true,
                      position: 'right',
                      fontSize: 9,
                      boxWidth: 20
                  },
                  title: {
                      display: true,
                      text: 'PENJUALAN TOP 10 PER KEC. DI POS ' + pos + ' MTD ' + tanggal
                  },
                  chartArea: {
                      backgroundColor: 'rgba(255, 255, 255, 1)'
                  }
                };

                var config = {
                    type: 'pie',
                    data: {
                            datasets: [{
                                    type: 'pie',
                                    data: PData,
                                    backgroundColor: [
                                            window.chartColors.a,
                                            window.chartColors.b,
                                            window.chartColors.c,
                                            window.chartColors.d,
                                            window.chartColors.e,
                                            window.chartColors.f,
                                            window.chartColors.g,
                                            window.chartColors.h,
                                            window.chartColors.i,
                                            window.chartColors.j
                                        ],
                                    fill: false,
                                    lineTension:0.5,
                                    label: 'Penjualan Per Kecamatan'
                            }],
                            labels:PLabel
                        },
                    options: pieOptions
                };
                var Sales = $('#chartLevel1').get(0).getContext('2d');
                if(typeof mychartLevel1 != 'undefined' ){
                    mychartLevel1.destroy();
                }
                mychartLevel1 = new Chart(Sales, config);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    // Grafik untuk penjualan pos kecamatan end

    // Tabel untuk penjualan per sales aktual start
    function getJualPerSalesAktual(tahun){
          if ( $.fn.DataTable.isDataTable('.table-top-sales') ) {
            $('.table-top-sales').DataTable().destroy();
          } 
        $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualPerSalesAktual');?>",
            data: {"tanggal":tahun},
            success: function(resp){  
                var obj = jQuery.parseJSON(resp);
                var no = 1;
                var totalJualSL = 0;
                var totalJualCS = 0;
                
                var totalSL = 0;
                var totalCS = 0;
                var totalAll = 0;
                $(".bodySalesMTD").html('');
                $.each(obj.jual, function(key, data){
                      $(".bodySalesMTD").append('<tr>' + 
                                                  '<td style="text-align: center;">'+no+'</td>' + 
                                                  '<td style="text-align: left;">'+data.nmsales+'</td>' + 
                                                  '<td style="text-align: center;">'+data.status+'</td>' + 
                                                  '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                              '</tr>');
                        if(data.status==="CS"){
                            totalJualCS = Number(totalJualCS) + Number(data.jml);                            
                        }else{
                            totalJualSL = Number(totalJualSL) + Number(data.jml);
                        }
                        totalAll = Number(totalAll) + Number(data.jml);     
                      no++;
                });     
                $(".footSalesMTD").html('<tr>' + 
                                                '<td style="text-align: center;"><b>TOTAL</b></td>'+
                                                '<td style="text-align: center;"></td>' + 
                                                '<td style="text-align: center;"></td>' + 
                                                '<td style="text-align: right;"><b>'+numeral(totalAll).format('0,0')+'</b></td>' + 
                                            '</tr>');
                $(".table-top-sales").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                });
                
                $.each(obj.jmlsales, function(key, data){
                    if(data.status==="CS"){
                        totalCS = Number(totalCS) + Number(data.jml);                            
                    }else{
                        totalSL = Number(totalSL) + Number(data.jml);
                    }
                });
//                console.log("TOTAL JUAL CS : " + totalJualCS);
//                console.log("TOTAL CS : " + totalCS);
//                console.log("TOTAL JUAL SL : " + totalJualSL);
//                console.log("TOTAL SL : " + totalSL);
                var avgCS = (Number(totalJualCS)/Number(totalCS));
                var avgSL = (Number(totalJualSL)/Number(totalSL));
                
                $(".cap-sales-mtd").html('<div style="font-size:13px;">' +
                                                ' <label class="label label-primary">Sales Productivity '+avgSL.toFixed(1)+'</label> ' +
                                                ' | <label class="label label-success">Counter Productivity '+avgCS.toFixed(1)+'</label> ' +
                                            '</div>' +
                                            'Penjualan Per Sales MTD : ' + tahun);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
    // Tabel untuk penjualan per sales aktual end

    // Tabel untuk stok per tipe aktual start
    function getStokPerTipeAktual(tahun){
      if ( $.fn.DataTable.isDataTable('.table-stok-aktual') ) {
        $('.table-stok-aktual').DataTable().destroy();
      }    
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getStokPerTipeAktual');?>",
          data: {"tanggal":tahun},
          success: function(resp){  
              var obj = jQuery.parseJSON(resp);
              var no = 1;
              $(".cap-stok-mtd").html("Stok Aktual MTD " + tahun);
              $(".bodyStokMTD").html('');
              var totalAll = 0;
              $.each(obj, function(key, data){
                    $(".bodyStokMTD").append('<tr>' + 
                                                '<td style="text-align: center;">'+no+'</td>' + 
                                                '<td style="text-align: left;">'+
                                                    data.nmtipegrp+
                                                    ' <small style="font-size: 12px;float: right;"><label onclick="getDetailStok(\''+data.nmtipegrp+'\',\''+data.nmtipe+'\')" class="label label-info">Detail</label></small>'+
                                                    '<br><small style="font-size: 10px;">'+data.nmtipe+'</small>'+
                                                '</td>' + 
                                                '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                            '</tr>');
                    totalAll = Number(totalAll) + Number(data.jml);                 
                    no++;
              });            
                $(".footStokMTD").html('<tr>' + 
                                                '<td style="text-align: center;"></td>' + 
                                                '<td style="text-align: center;"><b>TOTAL</b></td>'+
                                                '<td style="text-align: right;"><b>'+numeral(totalAll).format('0,0')+'</b></td>' + 
                                            '</tr>');
              $(".table-stok-aktual").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                });
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
    });
    }
    // Tabel untuk stok per tipe aktual end
</script>