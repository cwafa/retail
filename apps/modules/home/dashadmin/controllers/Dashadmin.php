<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashadmin
 *
 * @author adi
 */
class Dashadmin extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('dashadmin/submit'),
            'add' => site_url('dashadmin/add'),
            'edit' => site_url('dashadmin/edit'),
            'reload' => site_url('dashadmin'),
        );
        $this->load->model('dashadmin_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->dashadmin_qry->json_dgview();
    } 
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    } 
}
