<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php echo $template['title'];?> </title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url('assets/dist/img/logo-honda-prima-small.png');?>" type="image/png">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('assets/font-awesome-4.7.0/css/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url('assets/ionicons-2.0.1/css/ionicons.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/flat/blue.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/daterangepicker/daterangepicker.css');?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    
    <!-- jQuery 2.2.3 -->
    <script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?=base_url('assets/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
    
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/dataTables.min.css');?>">
    <script src="<?=base_url('assets/plugins/dtables/dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/fnReloadAjax.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/currency.js');?>"></script>
    
<!--    <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.buttons.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>-->
    
    <!-- Sweetalert -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/sweetalert/sweetalert.css');?>">
    <script src="<?=base_url('assets/plugins/sweetalert/sweetalert.min.js');?>"></script>
    
    <!-- Jansy -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css');?>">
    <script src="<?=base_url('assets/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js');?>"></script>
    
    <!-- Ajax Form -->
    <script src="<?=base_url('assets/plugins/jquery-form/jquery.form.min.js');?>"></script>
    
    <!-- AdminLTE App -->
    <script src="<?=base_url('assets/dist/js/app.min.js');?>"></script>
    
    <!-- Chosen -->
    <link href="<?=base_url('assets/plugins/chosen/bootstrap-chosen.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/chosen/chosen.jquery.js');?>"></script>
    
    <!-- Select2 -->
    <link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
    
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/maskedinput/jquery.maskedinput.min.js');?>"></script>

    <!-- Numeral JS -->
    <script src="<?=base_url('assets/plugins/numeral/numeral.min.js');?>"></script>    
    
    <!-- Animate -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/animate.css');?>">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
    
    <!-- Custom Js CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/my.css');?>">
    <script src="<?=base_url('assets/custom/my.js');?>"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js');?>"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js');?>"></script>
    <![endif]-->
    <style type="text/css">
        .main-header .logo {
            font-size: 15px;
            width: 180px;
        }
        
        .main-header .navbar {
            margin-left: 180px;
        }
        .main-sidebar, .left-side {
            font-size: 11px;
            width: 180px;
        }
        .user-panel>.info {
            left: 0px;
        }
        
        .sidebar-menu .treeview-menu>li>a {
            font-size: 11px;
        }
        .content-wrapper, .right-side, .main-footer {
            margin-left: 180px;
        }
        .skin-red-light .sidebar-menu>li.active>a>.fa
            , .skin-red-light .sidebar-menu>li.active>a>.glyphicon
            , .skin-red-light .sidebar-menu>li.active>a>.ion{
            color: #dd4b39;
        }
        .select2-selection{
            min-height: 35px !important;
/*            width: 210px !important;*/
        }
        .skin-red .sidebar-menu>li:hover>a{
            color: #fff;
            border-left-color: #d73925;           
        }
        
        .skin-red .sidebar-menu>li.active>a {
            color: #fff;
            background: #d73925;
            border-left-color: #d73925;
        }     
        
        .skin-red .main-header .navbar .sidebar-toggle:hover {
            background-color: rgba(0, 0, 0, 0.1);
        }
        
        @media (min-width: 768px){
            .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>a>span:not(.pull-right), .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu {
/*                width: 225px;*/
            }    
            .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu {
/*                width: 225px;*/
            }    
            .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu {
                top: 35px;
                margin-left: 0;
            }  
        }
        @media (max-width: 768px){
            .content-wrapper, .right-side, .main-footer {
                margin-left: 0px;
            }
            
            .main-header .logo, .main-header .navbar {
                width: 100%;
                float: none;
            }            
            .main-header .navbar {
                 margin-left: 0px !important; 
            }
            .main-sidebar, .left-side {
                font-size: 14px;
                width: 230px;
            }
            .sidebar-menu .treeview-menu>li>a {
                font-size: 14px;
            }
        }
    </style>
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=site_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
          <img src="<?=base_url('assets/dist/img/honda-one-heart-white-xs.png');?>" style="height: 40px;" />
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
          <?php echo $this->apps->title;?>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a href="javascript:void(0);" class="date-time"></a>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url('profile/show_image');?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('nmuser');?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?=base_url('profile/show_image');?>" class="img-circle" alt="User Image">

                  <p>
                      <?php echo $this->session->userdata('nmuser');?>
                      <small><?php echo $this->apps->logintag;?></small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                        <a href="#">&nbsp;</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
  <!--                  <a href="<?=site_url('profile');?>" class="btn btn-default btn-flat">Profile</a>-->
                  </div>
                  <div class="pull-right">
                      <?php
                          $attributes = array(
                              'id' => 'logout_form'
                              , 'name' => 'logout_form'
                              , 'method' => 'post');
                          echo form_open(site_url('access/logout'),$attributes);

                          $btn_logout = array(
                              'name'          => 'button',
                              'id'            => 'button',
                              'value'         => 'true',
                              'type'          => 'submit',
                              'content'       => '<i class="fa fa-sign-out"></i> Keluar',
                              'class'         => 'btn btn-danger btn-flat'
                          );
                          echo form_button($btn_logout);
                          echo form_close();
                      ?>
                  </div>
                </li>
              </ul>
            </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="padding-bottom: 40px;">
        <div class="pull-left info" style="width: 115px !important;">
          <p>
              <?php echo $this->session->userdata('nmuser');?>
          </p>
          <a href="#" style="font-size: 10px;"><i class="fa fa-map-marker"></i> <?php echo $this->apps->logintag;?></a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form" style="border: none;">
        <div class="input-group">
            <select class="form-control" name="q-menu" id="q-menu">
            </select>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php
            $dashboard_active = "";
            $profile_active = "";
            $class_name = $this->uri->segment(1);
            if($class_name=="dashboard" || $class_name == ""){
                $dashboard_active= "active";
            }elseif($class_name=="profile"){
                $profile_active= "active";
            }
        ?>   
        <li class="<?php echo $dashboard_active;?>">
            <a href="<?=site_url('dashboard');?>">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <?php
            $menu_app = $this->rbac->menu_app($class_name);
            if(is_array($menu_app)){
                foreach ($menu_app['menus'] as $mn)
                {  
                    if( !empty($mn['name']) ){
                        if($mn['sub']['submenu']){
                    ?>
                            <li class="<?php echo $mn['active'];?> treeview">
                              <a href="javascript:void(0);" title="<?php echo $mn['description'];?>">
                                <i class="<?php echo $mn['icon'];?>"></i>
                                <span ><?php echo $mn['name'];?></span> 
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                              </a>
                              <ul class="treeview-menu">
                    <?php      
                                foreach ($mn['sub']['submenu'] as $submn)
                                {
                                    if($submn){
                                        echo "<li class=\"".$submn['sub_active']."\">"
                                                . "<a title=\"".$submn['description']."\" href=\"".site_url($submn['link'])."\"> "
                                                . "<i class=\"".$submn['icon']."\"></i>"
                                                . $submn['name']
                                                . "</a>"
                                            . "</li>";
                                    }
                                }                        
                    ?>
                              </ul>
                            </li>
                    <?php
                            }
                        }
                }     
            } 
        ?> 
<?php

        $groupname = $this->session->userdata('groupname');
        if($groupname!=="SALES" 
                && $groupname!=="SPV" 
                && $groupname!=="ACCOUNTING"  ){
?>
        <li class="<?php echo $profile_active;?>">
            <a href="<?=site_url('profile');?>">
                <i class="fa fa-user-circle-o"></i> 
                <span>Profil Pengguna</span>
            </a>
        </li>                                                        
<?php
        }
?>
        <li>
            <a href="javascript:void(0);" onclick="logout();" title="Keluar dari aplikasi menuju halaman login">
                <i class="fa fa-sign-out"></i> 
                <span>Keluar</span>
            </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          {msg_main}
          <small>{msg_detail}</small>
        </h1>         
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="alert-submit" style="display: none;">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <span class="alert-msg"></span>
            </div>                    
        </div>
        <?php echo $template['body'];?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">      
    <div class="pull-right hidden-xs">
        <?php 
            echo  (ENVIRONMENT === 'development') ? 
                ''
//                . 'Memory usage :' 
//                . $this->benchmark->memory_usage() 
//                . ', ' 
//                . $this->benchmark->elapsed_time() 
//                . ' seconds '
                . 'CodeIgniter Version '
                . CI_VERSION 
                . ' | Engine Ver : ' . phpversion()
                : '' ?>
    </div>
    <div>
        <?php echo $this->apps->copyright;?> &copy; 2016 - <?php echo (date('Y'));?>
    </div>
  </footer>
  <?php
    $statsubmit = $this->session->userdata('statsubmit');
    $this->session->unset_userdata('statsubmit');
  ?> 
<script>
    $(document).ready(function() {
    
//        $(".form-control").keyup(function(){
//            var val = $(this).val();
//            $(this).val(val.toUpperCase());
//        });
        
        var statsubmit = '<?php echo $statsubmit?>';
        if(statsubmit!==""){
            var obj = jQuery.parseJSON(statsubmit);
            if(obj){
                $(".alert-submit").show()
                                  .fadeOut(60000);
                $(".alert-msg").html(obj.msg);               
            }      
        }          
        setInterval(function(){  
            var tanggal = moment().format('DD MMMM YYYY, H:mm:ss');
            $(".date-time").html(tanggal);
        }, 1000);
        
        $('#q-menu').select2({
            placeholder: 'Pencarian Menu ...',
            dropdownAutoWidth : false,
            width: '210px',
            ajax: {
                  url: "<?=site_url('menus/get_menu');?>",
                  type: 'post',
                  dataType: 'json',
                  delay: 250,
                  data: function (params) {
                    return {
                      q: params.term, // search term
                      page: params.page
                    };
                  },
                  processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                      results: data.items,
                      pagination: {
                        more: (params.page * 30) < data.total_count
                      }
                    };
                  },
                  cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatMenus, // omitted for brevity, see the source of this page
                templateSelection: formatMenusSelection // omitted for brevity, see the source of this page
        });        
        
        $('#q-menu').change(function(){
            var data = $(this).val();
            window.location.replace("<?=site_url();?>"+data);
        });
        
        function formatMenus (repo) {
            if (repo.loading) return "Mencari data ... ";


            var markup = "<div class='select2-result-repository clearfix'>" +
              "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

            markup += "<div class='select2-result-repository__statistics'>" +
              "<div class='select2-result-repository__stargazers' style='font-size: 12px;'> " + repo.deskripsi + " </div>" +
            "</div>" +
            "</div></div>";
            return markup;
          }

        function formatMenusSelection (repo) {
            return repo.full_name || repo.text;
        }  
          
        $(".chosen-select").chosen({
            no_results_text: "Maaf, data tidak ditemukan!"
        }); 

        $('.calendar').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('.month').datepicker({
            startView: "year", 
            minViewMode: "months",
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "mm-yyyy"      
        });
    });
    
    function logout(){
        swal({
            title: "Konfirmasi Keluar Aplikasi !",
            text: "Pilih Ya, jika sudah selesai menggunakan aplikasi",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Keluar!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            location.replace("<?=site_url('access/logout');?>");
        });
    }
</script>
</div>
<!-- ./wrapper -->
</body>
</html>
