<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

/**
 * Description of Access
 *
 * @author pudyasto
 */
class Rbac {
//class Rbac extends CI_Model {
	var $key = "575243385";
	//put your code here
	public function __construct(){
		$this->ci =& get_instance();
	}



	public function get_main_menu($kduser) {

		//user = administrator / super user

		//echo "<script> console.log('PHP: ". $this->ci->session->userdata('su') ."');</script>";


		if ( $this->ci->session->userdata('su')== 't') {
			$query = $this->ci->db->query("select * from trx.f_menu where faktif=true and kdmenu_h is null order by menuorder", $kduser);
		}
		else {
			$query = $this->ci->db->query("select * from trx.f_get_main_menu(?)", $kduser);
		}

		$result = $query->result();

		$output['menus']=array();

		foreach ($result as $res)
		{
			$output['menus'][] = array(
				'id'            => $res->kdmenu,
				'name'          => $res->nmmenu,
				'icon'          => $res->icon,
				'link'          => $res->link,
				'description'   => $res->description,
				//'active'      => $this->active_menu($class,$res->rowid),
				'sub'           => $res->kdmenu_h,
			);
		}

		//echo "<script> console.log('PHP: ". json_encode($output) ."');</script>";

		return $output;
	}


	public function get_sub_menu($kduser, $kdmenu) {

		//user = administrator / super user
		if ( $this->ci->session->userdata('su')== 't') {
			$query = $this->ci->db->query("select * from trx.f_get_sub_menu(?)", $kdmenu);
		}
		else {
			$query = $this->ci->db->query("select * from trx.f_get_sub_menu(?,?)", array($kduser, $kdmenu));
		}

		$result = $query->result();

		$output['menus']=array();

		foreach ($result as $res)
		{
			$output['menus'][] = array(
				'id'            => $res->kdmenu,
				'name'          => $res->nmmenu,
				'icon'          => $res->icon,
				'link'          => $res->link,
				'description'   => $res->description,
				//'active'      => $this->active_menu($class,$res->rowid),
				'sub'           => $res->sub,

			);
		}

		//echo "<script> console.log('PHP: ". json_encode($output) ."');</script>";

		return $output;
	}












	/*
	public function menu_app($class) {
		$dbrbac = $this->ci->load->database('rbac', TRUE);
		$dbrbac->select('rowid,
						name,
						description,
						link,
						icon,
						statmenu,
						mainmenuid');
		$dbrbac->where('mainmenuid',NULL, FALSE);
		//$dbrbac->order_by("name","asc");
		$dbrbac->order_by("menuorder,name","asc");
		$query = $dbrbac->get('menus');
		$output['menus']=array();
		$result = $query->result();
		$this->ci->load->database('rbac', FALSE);
		foreach ($result as $res)
		{
			$output['menus'][] = array(
				'id' => $res->rowid,
				'name' => $res->name,
				'icon' => $res->icon,
				'description' => $res->description,
				'active' => $this->active_menu($class,$res->rowid),
				'sub' => $this->submenu_app($class,$res->rowid),
			);
		}

		echo "<script> console.log('PHP: ". json_encode($output) ."');</script>";

		return $output;
	}
	*/

	/*
	private function active_menu($class,$id) {
		$dbrbac = $this->ci->load->database('rbac', TRUE);
		$dbrbac->where('link',skipclass($class));
		$dbrbac->order_by('menuorder,name');

		$query = $dbrbac->get('menus');


		//$query = $dbrbac->get('menus')->get_compiled_select();
		//echo "<script> console.log('PHP: ',",$dbrbac->get_compiled_select(),");</script>";

		//print_r($dbrbac->get_compiled_select());

		$this->ci->load->database('rbac', FALSE);
		$result = $query->result_array();
		foreach ($result as $val){
			if($val['mainmenuid']==$id){
				return 'active';
			}else{
				return false;
			}
		}
	}
	*/
	/*
	private function submenu_app($class,$id){
		$username = $this->ci->session->userdata('username');
		$dbrbac = $this->ci->load->database('rbac', TRUE);
		if($username=="ADMINISTRATOR"){
			$dbrbac->select('rowid,
							name,
							description,
							link,
							icon,
							statmenu,
							mainmenuid');
			$dbrbac->where('mainmenuid',$id);
			$dbrbac->where('statmenu','1');
			$dbrbac->order_by('menuorder,name');
			$query = $dbrbac->get('menus');
		}else{
//            $dbrbac->select('rowid,
//                            name,
//                            description,
//                            link,
//                            icon,
//                            statmenu,
//                            mainmenuid');
//            $dbrbac->where('mainmenuid',$id);
//            $dbrbac->where('statmenu','1');
//            $dbrbac->order_by('menuorder,name');
//            $query = $dbrbac->get('menus');
			$str = "SELECT
						menus.rowid,
						menus.menuorder,
						menus.name,
						menus.link,
						menus.icon,
						menus.description,
						menus.statmenu
					FROM
						menus
					INNER JOIN trx.f_groups_menu
						ON menus.rowid = trx.f_groups_menu.kdmenu
					INNER JOIN trx.user
						ON trx.user.kdgroups = trx.f_groups_menu.kdgroups
					WHERE (menus.mainmenuid='{$id}')
						AND (trx.user.user_id = '{$username}')
						AND menus.statmenu = '1'
					ORDER BY menus.name ASC";
			$query = $dbrbac->query($str);
		}
		$output['submenu']=array();
		$this->ci->load->database('rbac', FALSE);
		$result = $query->result();
		foreach ($result as $row)
		{
			if(skipclass($class) == $row->link){
				$mnsub_active = 'active';
			}else{
				$mnsub_active = '';
			}

			if($this->group_app($row->rowid)){
				$output['submenu'][] = array(
					'id' => $row->rowid,
					'name' => $row->name,
					'link' => $row->link,
					'icon' => $row->icon,
					'description' => $row->description,
					'sub_active' => $mnsub_active,
				);
			}
		}
		return $output;
	}

	private function group_app($id){
		$username = $this->ci->session->userdata('username');
		if($username=="ADMINISTRATOR"){
			return true;
		}else{
//            return true;
			$dbrbac = $this->ci->load->database('rbac', TRUE);
			$str = " SELECT ga.rowid
						FROM trx.f_groups_menu AS ga
							INNER JOIN trx.user AS ug
								ON ga.kdgroups = ug.kdgroups
						WHERE (ug.user_id = '".$username."')
						AND (ga.kdmenu = '".$id."') ";
			$query = $dbrbac->query($str);
			$this->ci->load->database('rbac', FALSE);
			$num_row = $query->num_rows();
			if ($num_row > 0){
				return true;
			}else{
				return false;
			}
		}
	}

	*/


	public function ceksubmenu_app($class) {

		$cls = skipclass($class);
		//echo "<script> console.log('PHP: ". $cls ."');</script>";

		$query = $this->ci->db->get_where('f_menu', array('link'=>$cls));

		if ($query)
		{
			$result = $query->result();
			return $result;
		}else{
			return false;
		}




/*
		$cls = skipclass($class);

		//echo "<script> console.log('PHP: ". json_encode($class) ."');</script>";
		//echo "<script> console.log('PHP: ". json_encode($cls) ."');</script>";

		$dbrbac = $this->ci->load->database('rbac', TRUE);
		$dbrbac->where('link',$cls);
		$query = $dbrbac->get('menus');
		$this->ci->load->database('rbac', FALSE);
		if ($query)
		{
			$result = $query->result();
			return $result;
		}else{
			return false;
		}
*/

	}





	public function module_access($action){
		$class = $this->ci->uri->segment(1);
		$method = $this->ci->uri->segment(2);
		$username = $this->ci->session->userdata('username');
		$res = null;
		if((skipclass($class)=="dashboard" || ($class=="menus" && $method=="get_menu") || skipclass($class)=="") && $action=="show"){
			$res = "show";
		}else{
			$res = $this->_privilege($class,$username);
			if($action=="show" && !empty($res[0]["moduleid"])){
				$res = "show";
			}elseif($action=="add" && empty($res[0]["act_add"])){
				$res = "disabled";
			}elseif($action=="edit" && empty($res[0]["act_edit"])){
				$res = "disabled";
			}elseif($action=="del" && empty($res[0]["act_delete"])){
				$res = "disabled";
			}elseif($action=="print" && empty($res[0]["act_print"])){
				$res = "disabled";
			}else{
				$res = "";
			}
		}
//        $dbrbac->close(); // Kalo pake postgre fitur ini dimatikan saja
		return $res;
	}





	private function _privilege($class,$username) {
		if ( $this->ci->session->userdata('su')== 't') {
			$str = "SELECT 1 moduleid ,1 act_add ,1 act_edit ,1 act_delete ,1 act_print";
		}else{
			$str = "SELECT m.kdmenu
						,(SELECT ga.kdgroups
								FROM trx.f_groups_menu AS ga
										INNER JOIN trx.user AS ug ON ga.kdgroups = ug.kdgroups
								WHERE (ug.kduser = '".$username."')
										AND (ga.kdmenu = m.kdmenu)) moduleid
						,(SELECT substr(ga.privilege,1,1)
								FROM trx.f_groups_menu AS ga
										INNER JOIN trx.user AS ug ON ga.kdgroups = ug.kdgroups
								WHERE (ug.kduser = '".$username."')
										AND (ga.kdmenu = m.kdmenu)) act_add
						,(SELECT substr(ga.privilege,3,1)
								FROM trx.f_groups_menu AS ga
										INNER JOIN trx.user AS ug ON ga.kdgroups = ug.kdgroups
								WHERE (ug.kduser = '".$username."')
										AND (ga.kdmenu = m.kdmenu)) act_edit
						,(SELECT substr(ga.privilege,5,1)
								FROM trx.f_groups_menu AS ga
										INNER JOIN trx.user AS ug ON ga.kdgroups = ug.kdgroups
								WHERE (ug.kduser = '".$username."')
										AND (ga.kdmenu = m.kdmenu)) act_delete
						,(SELECT substr(ga.privilege,7,1)
								FROM trx.f_groups_menu AS ga
										INNER JOIN trx.user AS ug ON ga.kdgroups = ug.kdgroups
								WHERE (ug.kduser = '".$username."')
										AND (ga.kdmenu = m.kdmenu)) act_print
						FROM trx.f_menu m
						WHERE (link = '".$class."') AND m.faktif = 'true'";
		}

		$query = $this->ci->db->query($str);
		return $query->result_array();
	}


/*
	private function _privilege($class,$username) {
		$dbrbac = $this->ci->load->database('rbac', TRUE);
		if($username=="ADMINISTRATOR"){
			$str = "SELECT 1 moduleid ,1 act_add ,1 act_edit ,1 act_delete ,1 act_print";
		}else{
//            $str = "SELECT 1 moduleid ,1 act_add ,1 act_edit ,1 act_delete ,1 act_print";
			$str = "SELECT m.rowid
						,(SELECT ga.rowid
								FROM groups_access AS ga
										INNER JOIN users_groups AS ug ON ga.group_id = ug.group_id
								WHERE (ug.user_id = '".$username."')
										AND (ga.menu_id = m.rowid)) moduleid
						,(SELECT substr(ga.privilege,1,1)
								FROM groups_access AS ga
										INNER JOIN users_groups AS ug ON ga.group_id = ug.group_id
								WHERE (ug.user_id = '".$username."')
										AND (ga.menu_id = m.rowid)) act_add
						,(SELECT substr(ga.privilege,3,1)
								FROM groups_access AS ga
										INNER JOIN users_groups AS ug ON ga.group_id = ug.group_id
								WHERE (ug.user_id = '".$username."')
										AND (ga.menu_id = m.rowid)) act_edit
						,(SELECT substr(ga.privilege,5,1)
								FROM groups_access AS ga
										INNER JOIN users_groups AS ug ON ga.group_id = ug.group_id
								WHERE (ug.user_id = '".$username."')
										AND (ga.menu_id = m.rowid)) act_delete
						,(SELECT substr(ga.privilege,7,1)
								FROM groups_access AS ga
										INNER JOIN users_groups AS ug ON ga.group_id = ug.group_id
								WHERE (ug.user_id = '".$username."')
										AND (ga.menu_id = m.rowid)) act_print
						FROM menus m
						WHERE (link = '".$class."') AND m.statmenu = '1'";
		}

		$this->ci->load->database('rbac', FALSE);
		$query = $dbrbac->query($str);
		return $query->result_array();
	}
*/

}
