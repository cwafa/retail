<?php

/*
 * ***************************************************************
 * Script : M_pdf
 * Version : 
 * Date : Jun 11, 2017 | 7:58:52 PM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

/**
 * Description of M_pdf
 *
 * @author pudyasto
 */
include_once APPPATH.'/third_party/mpdf60/mpdf.php';
class M_pdf {
    //put your code here
    
    function load($param=NULL)
    {    
        if ($params == NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';          		
        }
         
        //return new mPDF($param);
        return new mPDF();
    }
}
